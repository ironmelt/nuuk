/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include "testbench.h"

#include "nuuk/collection/concurrent/dsnmap.h"
#include "nuuk/util/math.h"

#include "helper/test_map.h"


/******************************************************************************
 * FEST CONFIG
 */

#define DEFAULT_CAPACITY    16
#define DEFAULT_LOAD_FACTOR 0.75


/******************************************************************************
 * HELPER MACROS
 */

#define dsnmap(__map)               ((nuuk_dsnmap_t *) __map)
#define ACTUAL_CAPACITY(__capacity) nuuk_next_pow2((uint32_t) ((__capacity) / DEFAULT_LOAD_FACTOR))


/******************************************************************************
 * SPECIALIZED TESTS
 */

TEST(nuuk_dsnmap_init, {

  DESCRIBE(".nuuk_dsnmap_init()", {

      IT("should work with null load factor", {
          nuuk_dsnmap_t map;
          ASSERT(NUUK_MAP_OK == nuuk_dsnmap_init(&map, &nuuk_map_test_element_type, DEFAULT_CAPACITY, 0));
          ASSERT(NUUK_MAP_OK == NUUK_MAP_TEST_PUT(&map, 0, 0, NULL).status);
          nuuk_dsnmap_destroy(&map);
      })

      IT("should work with null capacity", {
        nuuk_dsnmap_t map;
        ASSERT(NUUK_MAP_OK == nuuk_dsnmap_init(&map, &nuuk_map_test_element_type, 0, DEFAULT_LOAD_FACTOR));
        ASSERT(((void *) atomic_load(&map.node)) == NULL);
        //ASSERT(NUUK_MAP_OK == NUUK_MAP_TEST_PUT(&map, 0, 0, NULL));
        nuuk_dsnmap_destroy(&map);
      })

  })

})


/******************************************************************************
 * HOOKS / FIXTURES
 */

static
nuuk_map_t * _nuuk_dsnmap_test_hook_new() {
  nuuk_map_t * map = nuuk_alloc(sizeof(nuuk_dsnmap_t));
  if (!map ||
      nuuk_dsnmap_init((nuuk_dsnmap_t *) map, &nuuk_map_test_element_type, DEFAULT_CAPACITY, DEFAULT_LOAD_FACTOR) !=
          NUUK_MAP_OK) {
    return NULL;
  }
  return map;
}

static
void _nuuk_dsnmap_test_hook_free(nuuk_map_t * map) {
  nuuk_dsnmap_destroy((nuuk_dsnmap_t *) map);
  nuuk_free(map);
}

/*
static
void * _nuuk_dsnmap_test_setup(void * fixtures) {
  return (void *) _nuuk_dsnmap_test_hook_new();
}

static
void _nuuk_dsnmap_test_teardown(void * fixtures, void * context) {
  nuuk_dsnmap_t * map = (nuuk_dsnmap_t *) context;
  _nuuk_dsnmap_test_hook_free((nuuk_map_t *) map);
}
*/

static
nuuk_map_test_hooks_t nuuk_dsnmap_test_hooks = {
    .new  = _nuuk_dsnmap_test_hook_new,
    .free = _nuuk_dsnmap_test_hook_free
};


/******************************************************************************
 * FEST RUNNER
 */

int main(void) {

  // Run the test cases here.
  DESCRIBE("nuuk_dsnmap_t", {

    // Should initialize correctly.
    RUN(nuuk_dsnmap_init, NULL);

    // Execute standard map tests.
    DESCRIBE("should pass nuuk_map tests", {
      RUN(nuuk_map, &nuuk_dsnmap_test_hooks);
    })

  })

  // Done. Print the results, and exit.
  return RESULTS();

}
