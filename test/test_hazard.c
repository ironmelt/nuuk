/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

#define NUUK_TEST


/*****************************************************************************/

#define TESTBENCH_NO_PIPE_OUTPUT
#include "testbench.h"

#include "nuuk/concurrent/hazard.h"
#include "nuuk/util/alloc.h"


/******************************************************************************
 * TEST CONFIG
 */

#define DEFAULT_STATIC_PTRS (2)
#define DEFAULT_LAZY        false


/******************************************************************************
 * MACROS
 */

#define HAZ                 ((nuuk_hazard_t *) fixtures)


/******************************************************************************
 * FIXTURES
 */

static
void * _nuuk_hazard_test_setup(void * udata) {
  nuuk_hazard_t * haz = malloc(sizeof(nuuk_hazard_t));
  nuuk_hazard_init(haz, DEFAULT_STATIC_PTRS, DEFAULT_LAZY);
  return (void *) haz;
}

static
void _nuuk_hazard_test_teardown(void * udata, void * fixtures) {
  nuuk_hazard_t * haz = (nuuk_hazard_t *) fixtures;
  nuuk_hazard_destroy(haz);
  free(haz);
}


/******************************************************************************
 * TESTS
 */

/**
 * Test hazard initialization.
 */
TEST(nuuk_test_hazard_init, {

  DESCRIBE("nuuk_hazard_init()", {

    IT("should initialize the hazard", {
      nuuk_hazard_t haz;
      ASSERT(nuuk_hazard_init(&haz, 2, false) == NUUK_HAZARD_OK);
      nuuk_hazard_destroy(&haz);
    })

    IT("should be initializable lazily", {
      nuuk_hazard_t haz;
      ASSERT(nuuk_hazard_init(&haz, 2, true) == NUUK_HAZARD_OK);
      nuuk_hazard_destroy(&haz);
    })

  })

})

/**
 * Test hazard free
 */
TEST(nuuk_test_hazard_free, {

  DESCRIBE("hazard_free()", {

    IT("should register the pointer for deferred free", {
      void * c = nuuk_alloc(sizeof(uint32_t));
      ASSERT(nuuk_hazard_free(HAZ, c) == NUUK_HAZARD_OK);
    })

    IT("should register multiple pointers for deferred free", {
      for (uint32_t i = 0; i < 100; ++i) {
        void * c = nuuk_alloc(sizeof(uint32_t));
        ASSERT(nuuk_hazard_free(HAZ, c) == NUUK_HAZARD_OK);
      }
    })

  })

})


/******************************************************************************
 * RUNNER
 */

int main() {

  DESCRIBE("nuuk/concurrent/hazard", {

    RUN(nuuk_test_hazard_init, NULL);

    SETUP(_nuuk_hazard_test_setup, NULL);
    TEARDOWN(_nuuk_hazard_test_teardown, NULL);

    RUN(nuuk_test_hazard_free, NULL);

  })

  return RESULTS();
}
