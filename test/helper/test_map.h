/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

#pragma once


/*****************************************************************************/


#include <string.h>

#include "nuuk/collection/map.h"
#include "nuuk/util/hash.h"

#include "testbench.h"


/******************************************************************************
 * GLOBAL VARIABLES
 */

static
nuuk_map_element_type_t nuuk_map_test_element_type = {
  .key_size = sizeof(uint32_t),
  .value_size = sizeof(uint64_t),
  .hash = XXH32,
  .compare = memcmp
};



/******************************************************************************
 * TYPES
 */

typedef struct nuuk_map_test_hooks_s {

  /**
   * Create a new map.
   */
  nuuk_map_t * (* new)();

  /**
   * Create a new map.
   */
  void (* free)(nuuk_map_t * map);

} nuuk_map_test_hooks_t;


/******************************************************************************
 * FIXTURES
 */

static
void * _nuuk_map_test_setup(void * udata) {
  nuuk_map_test_hooks_t * config = (nuuk_map_test_hooks_t *) udata;
  return (void *) config->new();
}

static
void _nuuk_map_test_teardown(void * udata, void * fixtures) {
  nuuk_map_test_hooks_t * config = (nuuk_map_test_hooks_t *) udata;
  nuuk_map_t * map = (nuuk_map_t *) fixtures;
  config->free(map);
}


/******************************************************************************
 * HELPER MACROS
 */

#define MAP(__map) ((nuuk_map_t *) __map)

#define NUUK_MAP_TEST_PUT(__map, __key, __value, __prev_value) \
  ({ \
    uint32_t key   = (__key); \
    uint64_t value = (__value); \
    nuuk_map_put(MAP(__map), &key, &value, (__prev_value)); \
  })

#define NUUK_MAP_TEST_REMOVE(__map, __key, __prev_value) \
  ({ \
    uint32_t key   = (__key); \
    nuuk_map_remove(MAP(__map), &key, (__prev_value)); \
  })

#define NUUK_MAP_TEST_GET(__map, __key) \
  ({ \
    uint32_t key   = (__key); \
    uint64_t value; \
    nuuk_map_get(MAP(__map), &key, &value); \
  })

#define NUUK_MAP_TEST_GET_VALUE(__map, __key) \
  ({ \
    uint32_t key   = (__key); \
    uint64_t value; \
    nuuk_map_get(MAP(__map), &key, &value); \
    value; \
  })

#define NUUK_MAP_TEST_HAS_KEY(__map, __key) \
  ({ \
    uint32_t key   = (__key); \
    nuuk_map_has_key(MAP(__map), &key); \
  })


/******************************************************************************
 * PUT TESTS
 */

TEST(nuuk_map_put, {

  DESCRIBE(".nuuk_map_put()", {

    IT("should add individual elements successfully", {
      NUUK_MAP_TEST_PUT(fixtures, 1, 42, NULL);
      ASSERT(42 == NUUK_MAP_TEST_GET_VALUE(fixtures, 1));
      NUUK_MAP_TEST_PUT(fixtures, 2, 43, NULL);
      ASSERT(42 == NUUK_MAP_TEST_GET_VALUE(fixtures, 1));
      ASSERT(43 == NUUK_MAP_TEST_GET_VALUE(fixtures, 2));
    })

    IT("should add duplicate elements successfully", {
      NUUK_MAP_TEST_PUT(fixtures, 1, 42, NULL);
      ASSERT(42 == NUUK_MAP_TEST_GET_VALUE(fixtures, 1));
      NUUK_MAP_TEST_PUT(fixtures, 1, 42, NULL);
      ASSERT(42 == NUUK_MAP_TEST_GET_VALUE(fixtures, 1));
    })

    IT("should add duplicate key elements successfully", {
      NUUK_MAP_TEST_PUT(fixtures, 1, 42, NULL);
      ASSERT(42 == NUUK_MAP_TEST_GET_VALUE(fixtures, 1));
      NUUK_MAP_TEST_PUT(fixtures, 1, 55, NULL);
      ASSERT(55 == NUUK_MAP_TEST_GET_VALUE(fixtures, 1));
    })

    IT("should return NUUK_MAP_OK when elements are added", {
      ASSERT(NUUK_MAP_OK == NUUK_MAP_TEST_PUT(fixtures, 1, 1, NULL).status);
    })

    IT("should provide the previous value if requested", {
      NUUK_MAP_TEST_PUT(fixtures, 1, 42, NULL);
      uint64_t prev_value = 0;
      NUUK_MAP_TEST_PUT(fixtures, 1, 55, &prev_value);
      ASSERT(42 == prev_value);
    })

  })

})


/******************************************************************************
 * GET TESTS
 */

TEST(nuuk_map_get, {

  DESCRIBE(".nuuk_map_get()", {

    IT("should retreive individual elements successfully", {
      NUUK_MAP_TEST_PUT(fixtures, 1, 42, NULL);
      ASSERT(42 == NUUK_MAP_TEST_GET_VALUE(fixtures, 1));
    })

    IT("should return true on existing elements", {
      NUUK_MAP_TEST_PUT(fixtures, 1, 42, NULL);
      ASSERT(true == NUUK_MAP_TEST_GET(fixtures, 1));
    })

    IT("should return false on non-existing elements", {
      ASSERT(false == NUUK_MAP_TEST_GET(fixtures, 1));
    })

  })

})


/******************************************************************************
 * HAS_KEY TESTS
 */

TEST(nuuk_map_has_key, {

  DESCRIBE(".nuuk_map_has_key()", {

    IT("should return true on existing elements", {
      NUUK_MAP_TEST_PUT(fixtures, 1, 42, NULL);
      ASSERT(true == NUUK_MAP_TEST_HAS_KEY(fixtures, 1));
    })

    IT("should return false on non-existing elements", {
      ASSERT(false == NUUK_MAP_TEST_HAS_KEY(fixtures, 1));
    })

  })

})


/******************************************************************************
 * REMOVE TESTS
 */

TEST(nuuk_map_remove, {

  DESCRIBE(".nuuk_map_remove()", {

    IT("should remove existing elements", {
      NUUK_MAP_TEST_PUT(fixtures, 1, 42, NULL);
      NUUK_MAP_TEST_REMOVE(fixtures, 1, NULL);
      ASSERT(false == NUUK_MAP_TEST_HAS_KEY(fixtures, 1));
    })

    IT("should handle non-existing elements", {
      NUUK_MAP_TEST_REMOVE(fixtures, 1, NULL);
      ASSERT(false == NUUK_MAP_TEST_HAS_KEY(fixtures, 1));
    })

    IT("should return true on existing elements", {
      NUUK_MAP_TEST_PUT(fixtures, 1, 42, NULL);
      ASSERT(true == NUUK_MAP_TEST_REMOVE(fixtures, 1, NULL).had_mapping);
    })

    IT("should return false on non-existing elements", {
      ASSERT(false == NUUK_MAP_TEST_REMOVE(fixtures, 1, NULL).had_mapping);
    })

    IT("should not remove the same element multiple times", {
      NUUK_MAP_TEST_PUT(fixtures, 1, 42, NULL);
      ASSERT(true == NUUK_MAP_TEST_REMOVE(fixtures, 1, NULL).had_mapping);
      ASSERT(false == NUUK_MAP_TEST_REMOVE(fixtures, 1, NULL).had_mapping);
    })

    IT("should provide the previous value if requested", {
      uint64_t prev_value = 0;
      NUUK_MAP_TEST_PUT(fixtures, 1, 42, NULL);
      NUUK_MAP_TEST_REMOVE(fixtures, 1, &prev_value);
      ASSERT(42 == prev_value);
    })

  })

})


/******************************************************************************
 * SIZE TESTS
 */

TEST(nuuk_map_size, {

  DESCRIBE(".nuuk_map_size()", {

    IT("should return 0 when the map is empty", {
      ASSERT(0 == nuuk_map_size(MAP(fixtures)));
    })

    IT("should increase when elements are added", {
      NUUK_MAP_TEST_PUT(fixtures, 1, 1, NULL);
      ASSERT(1 == nuuk_map_size(MAP(fixtures)));
      NUUK_MAP_TEST_PUT(fixtures, 2, 1, NULL);
      ASSERT(2 == nuuk_map_size(MAP(fixtures)));
      NUUK_MAP_TEST_PUT(fixtures, 3, 1, NULL);
      ASSERT(3 == nuuk_map_size(MAP(fixtures)));
    })

    IT("should decrease when elements are deleted", {
      NUUK_MAP_TEST_PUT(fixtures, 1, 1, NULL);
      NUUK_MAP_TEST_PUT(fixtures, 2, 1, NULL);
      NUUK_MAP_TEST_PUT(fixtures, 3, 1, NULL);
      NUUK_MAP_TEST_REMOVE(fixtures, 3, NULL);
      ASSERT(2 == nuuk_map_size(MAP(fixtures)));
      NUUK_MAP_TEST_REMOVE(fixtures, 2, NULL);
      ASSERT(1 == nuuk_map_size(MAP(fixtures)));
      NUUK_MAP_TEST_REMOVE(fixtures, 1, NULL);
      ASSERT(0 == nuuk_map_size(MAP(fixtures)));
    })

    IT("should not increase in case of duplicate elements", {
      NUUK_MAP_TEST_PUT(fixtures, 1, 1, NULL);
      NUUK_MAP_TEST_PUT(fixtures, 1, 2, NULL);
      ASSERT(1 == nuuk_map_size(MAP(fixtures)));
      NUUK_MAP_TEST_PUT(fixtures, 1, 3, NULL);
      ASSERT(1 == nuuk_map_size(MAP(fixtures)));
    })

  })

})


/******************************************************************************
 * TEST SUITE
 */

TEST(nuuk_map, {

  DESCRIBE("nuuk_map", {

    // Setup fixtures.
    SETUP(_nuuk_map_test_setup, udata);
    TEARDOWN(_nuuk_map_test_teardown, udata);

    // Test different functions.
    RUN(nuuk_map_put, udata);
    RUN(nuuk_map_get, udata);
    RUN(nuuk_map_has_key, udata);
    RUN(nuuk_map_remove, udata);
    RUN(nuuk_map_size, udata);

  })

})
