/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

#pragma once


/*****************************************************************************/


#include "nuuk/collection/list.h"

#include "testbench.h"


/******************************************************************************
 * MACROS
 */

#define LIST ((nuuk_list_t *) fixtures)


/******************************************************************************
 * TYPES
 */

typedef struct nuuk_list_test_hooks_s {

  /**
   * Create a new map.
   */
  nuuk_list_t * (* new)();

  /**
   * Create a new map.
   */
  void (* free)(nuuk_list_t * list);

} nuuk_list_test_hooks_t;


/******************************************************************************
 * FIXTURES
 */

static
void * _nuuk_list_test_setup(void * udata) {
  nuuk_list_test_hooks_t * config = (nuuk_list_test_hooks_t *) udata;
  return (void *) config->new();
}

static
void _nuuk_list_test_teardown(void * udata, void * fixtures) {
  nuuk_list_test_hooks_t * config = (nuuk_list_test_hooks_t *) udata;
  nuuk_list_t * list = (nuuk_list_t *) fixtures;
  config->free(list);
}


/******************************************************************************
 * HELPERS
 */

static
bool _nuuk_list_test_foreach_fn_add(void * element, void * udata) {
  uint32_t * total = (uint32_t *) udata;
  *total += *((uint32_t *) element);
  return true;
}

static
bool _nuuk_list_test_foreach_fn_stop(void * element, void * udata) {
  uint32_t * total = (uint32_t *) udata;
  *total += *((uint32_t *) element);
  return false;
}


/******************************************************************************
 * TESTS
 */

/*
 * Test put.
 */
TEST(nuuk_list_put, {

  uint32_t input[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  uint32_t output = 0;

  /*
   * Single put.
   */
  DESCRIBE("nuuk_list_put()", {

    IT("should put data in the list", {
      ASSERT(nuuk_list_put(LIST, 0, &input[1]) == NUUK_LIST_OK);
      output = 0;
      nuuk_list_get(LIST, 0, &output);
      ASSERT(output == input[1]);
      ASSERT(nuuk_list_put(LIST, 10, &input[1]) == NUUK_LIST_OK);
      output = 0;
      nuuk_list_get(LIST, 10, &output);
      ASSERT(output == input[1]);
    })

    IT("should increase the size of the list", {
      ASSERT(nuuk_list_put(LIST, 0, &input[1]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 1);
      ASSERT(nuuk_list_put(LIST, 10, &input[1]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 11);
    })

    IT("should fill the gap with 0 when putting beyond the end of the list", {
      ASSERT(nuuk_list_put(LIST, 10, &input[1]) == NUUK_LIST_OK);
      for (uint32_t i = 1; i < 10; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == 0);
      }
    })

    IT("should replace element previously in place", {
      ASSERT(nuuk_list_put(LIST, 0, &input[1]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_put(LIST, 0, &input[2]) == NUUK_LIST_OK);
      output = 0;
      nuuk_list_get(LIST, 0, &output);
      ASSERT(output == input[2]);
    })

    IT("should not increase the size of the list when replacing elements", {
      ASSERT(nuuk_list_put(LIST, 0, &input[1]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_put(LIST, 0, &input[2]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 1);
    })

  })

  /*
   * Range put.
   */
  DESCRIBE("nuuk_list_put_range()", {

    IT("should put data in the list", {
      ASSERT(nuuk_list_put_range(LIST, 0, 10, &input) == NUUK_LIST_OK);
      for (uint32_t i = 1; i < 10; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == i);
      }
    })

    IT("should increase the size of the list", {
      ASSERT(nuuk_list_put_range(LIST, 0, 2, &input[0]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 2);
      ASSERT(nuuk_list_put_range(LIST, 8, 2, &input[8]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 10);
    })

    IT("should fill the gap with 0 when putting beyond the end of the list", {
      ASSERT(nuuk_list_put_range(LIST, 8, 2, &input[8]) == NUUK_LIST_OK);
      for (uint32_t i = 1; i < 8; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == 0);
      }
    })

    IT("should replace element previously in place", {
      ASSERT(nuuk_list_put_range(LIST, 0, 5, &input[0]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_put_range(LIST, 3, 5, &input[5]) == NUUK_LIST_OK);
      for (uint32_t i = 0; i < 3; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == input[i]);
      }
      for (uint32_t i = 3; i < 8; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == input[i + 2]);
      }
    })

    IT("should not increase the size of the list when replacing elements", {
      ASSERT(nuuk_list_put_range(LIST, 0, 5, &input[0]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_put_range(LIST, 3, 5, &input[5]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 8);
    })

  })

})


/*
 * Test insert.
 */
TEST(nuuk_list_insert, {

  uint32_t input[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  uint32_t output = 0;

  /*
   * Single put.
   */
  DESCRIBE("nuuk_list_insert()", {

    IT("should insert data in the list", {
      ASSERT(nuuk_list_insert(LIST, 0, &input[1]) == NUUK_LIST_OK);
      output = 0;
      nuuk_list_get(LIST, 0, &output);
      ASSERT(output == input[1]);
      ASSERT(nuuk_list_insert(LIST, 10, &input[1]) == NUUK_LIST_OK);
      output = 0;
      nuuk_list_get(LIST, 10, &output);
      ASSERT(output == input[1]);
    })

    IT("should increase the size of the list", {
      ASSERT(nuuk_list_insert(LIST, 0, &input[1]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 1);
      ASSERT(nuuk_list_insert(LIST, 10, &input[1]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 11);
    })

    IT("should fill the gap with 0 when putting beyond the end of the list", {
      ASSERT(nuuk_list_insert(LIST, 10, &input[1]) == NUUK_LIST_OK);
      for (uint32_t i = 1; i < 10; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == 0);
      }
    })

    IT("should shift the position of elements on and after `index`", {
      ASSERT(nuuk_list_insert(LIST, 0, &input[1]) == NUUK_LIST_OK);
      output = 0;
      nuuk_list_get(LIST, 0, &output);
      ASSERT(output == input[1]);
      ASSERT(nuuk_list_insert(LIST, 0, &input[2]) == NUUK_LIST_OK);
      output = 0;
      nuuk_list_get(LIST, 0, &output);
      ASSERT(output == input[2]);
      nuuk_list_get(LIST, 1, &output);
      ASSERT(output == input[1]);
    })

  })

  /*
   * Range put.
   */
  DESCRIBE("nuuk_list_insert_range()", {

    IT("should put data in the list", {
      ASSERT(nuuk_list_put_range(LIST, 0, 10, &input) == NUUK_LIST_OK);
      for (uint32_t i = 1; i < 10; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == i);
      }
    })

    IT("should increase the size of the list", {
      ASSERT(nuuk_list_put_range(LIST, 0, 2, &input[0]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 2);
      ASSERT(nuuk_list_put_range(LIST, 8, 2, &input[8]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 10);
    })

    IT("should fill the gap with 0 when putting beyond the end of the list", {
      ASSERT(nuuk_list_put_range(LIST, 8, 2, &input[8]) == NUUK_LIST_OK);
      for (uint32_t i = 1; i < 8; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == 0);
      }
    })

  })

})


/*
 * Test append.
 */
TEST(nuuk_list_append, {

  uint32_t input[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  uint32_t output = 0;

  /*
   * Single append.
   */
  DESCRIBE("nuuk_list_append()", {

    IT("should append data to the list", {
      ASSERT(nuuk_list_append(LIST, &input[1]) == NUUK_LIST_OK);
      output = 0;
      nuuk_list_get(LIST, 0, &output);
      ASSERT(output == input[1]);
      ASSERT(nuuk_list_append(LIST, &input[2]) == NUUK_LIST_OK);
      output = 0;
      nuuk_list_get(LIST, 0, &output);
      ASSERT(output == input[1]);
      output = 0;
      nuuk_list_get(LIST, 1, &output);
      ASSERT(output == input[2]);
    })

    IT("should increase the size of the list", {
      ASSERT(nuuk_list_append(LIST, &input[1]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 1);
      ASSERT(nuuk_list_append(LIST, &input[1]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 2);
    })

  })

  /*
   * Range append.
   */
  DESCRIBE("nuuk_list_append_range()", {

    IT("should append data to the list", {
      ASSERT(nuuk_list_append_range(LIST, 10, &input) == NUUK_LIST_OK);
      for (uint32_t i = 1; i < 10; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == i);
      }
      ASSERT(nuuk_list_append_range(LIST, 10, &input) == NUUK_LIST_OK);
      for (uint32_t i = 10; i < 20; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == i - 10);
      }
    })

    IT("should increase the size of the list", {
      ASSERT(nuuk_list_append_range(LIST, 2, &input[0]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 2);
      ASSERT(nuuk_list_append_range(LIST, 2, &input[0]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 4);
    })

  })

})


/*
 * Test prepend.
 */
TEST(nuuk_list_prepend, {

  uint32_t input[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  uint32_t output = 0;

  /*
   * Single prepend.
   */
  DESCRIBE("nuuk_list_prepend()", {

    IT("should prepend data to the list", {
      ASSERT(nuuk_list_prepend(LIST, &input[1]) == NUUK_LIST_OK);
      output = 0;
      nuuk_list_get(LIST, 0, &output);
      ASSERT(output == input[1]);
      ASSERT(nuuk_list_prepend(LIST, &input[2]) == NUUK_LIST_OK);
      output = 0;
      nuuk_list_get(LIST, 0, &output);
      ASSERT(output == input[2]);
      output = 0;
      nuuk_list_get(LIST, 1, &output);
      ASSERT(output == input[1]);
    })

    IT("should increase the size of the list", {
      ASSERT(nuuk_list_prepend(LIST, &input[1]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 1);
      ASSERT(nuuk_list_prepend(LIST, &input[1]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 2);
    })

  })

  /*
   * Range prepend.
   */
  DESCRIBE("nuuk_list_prepend_range()", {

    IT("should prepend data to the list", {
      ASSERT(nuuk_list_prepend_range(LIST, 10, &input) == NUUK_LIST_OK);
      for (uint32_t i = 1; i < 10; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == i);
      }
      ASSERT(nuuk_list_prepend_range(LIST, 5, &input[5]) == NUUK_LIST_OK);
      for (uint32_t i = 0; i < 5; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == i + 5);
      }
      for (uint32_t i = 0; i < 10; ++i) {
        output = 0;
        nuuk_list_get(LIST, i + 5, &output);
        ASSERT(output == i);
      }
    })

    IT("should increase the size of the list", {
      ASSERT(nuuk_list_prepend_range(LIST, 2, &input[0]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 2);
      ASSERT(nuuk_list_prepend_range(LIST, 2, &input[0]) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 4);
    })

  })

})


/*
 * Test remove.
 */
TEST(nuuk_list_remove, {

  uint32_t input[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  uint32_t output = 0;

  /*
   * Single remove.
   */
  DESCRIBE("nuuk_list_remove()", {

    IT("should remove data from the list", {
      ASSERT(nuuk_list_prepend_range(LIST, 10, &input) == NUUK_LIST_OK);
      ASSERT(nuuk_list_remove(LIST, 0) == NUUK_LIST_OK);
      output = 0;
      nuuk_list_get(LIST, 0, &output);
      ASSERT(output == 1);
      ASSERT(nuuk_list_remove(LIST, 5) == NUUK_LIST_OK);
      output = 0;
      nuuk_list_get(LIST, 5, &output);
      ASSERT(output == 7);
    })

    IT("should decrease the size of the list", {
      ASSERT(nuuk_list_prepend_range(LIST, 10, &input) == NUUK_LIST_OK);
      ASSERT(nuuk_list_remove(LIST, 0) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 9);
      ASSERT(nuuk_list_remove(LIST, 7) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 8);
    })

  })

  /*
   * Range prepend.
   */
  DESCRIBE("nuuk_list_remove_range()", {

    IT("should remove data from the list", {
      ASSERT(nuuk_list_prepend_range(LIST, 10, &input) == NUUK_LIST_OK);
      ASSERT(nuuk_list_remove_range(LIST, 0, 5) == NUUK_LIST_OK);
      for (uint32_t i = 0; i < 5; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == i + 5);
      }
    })

    IT("should not do anything if `start_index` = `end_index`", {
      ASSERT(nuuk_list_prepend_range(LIST, 10, &input) == NUUK_LIST_OK);
      ASSERT(nuuk_list_remove_range(LIST, 4, 4) == NUUK_LIST_OK);
      for (uint32_t i = 0; i < 10; ++i) {
        output = 0;
        nuuk_list_get(LIST, i, &output);
        ASSERT(output == i);
      }
      ASSERT(nuuk_list_size(LIST) == 10);
    })

    IT("should decrease the size of the list", {
      ASSERT(nuuk_list_prepend_range(LIST, 10, &input) == NUUK_LIST_OK);
      ASSERT(nuuk_list_remove_range(LIST, 1, 4) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 7);
      ASSERT(nuuk_list_remove_range(LIST, 2, 4) == NUUK_LIST_OK);
      ASSERT(nuuk_list_size(LIST) == 5);
    })

    IT("should return `NUUK_LIST_ERR_INDEX` if the range is invalid", {
      ASSERT(nuuk_list_remove_range(LIST, 0, 1) == NUUK_LIST_ERR_INDEX);
      ASSERT(nuuk_list_prepend_range(LIST, 10, &input) == NUUK_LIST_OK);
      ASSERT(nuuk_list_remove_range(LIST, 5, 11) == NUUK_LIST_ERR_INDEX);
      ASSERT(nuuk_list_remove_range(LIST, 4, 3) == NUUK_LIST_ERR_INDEX);
    })

  })

})


/*
 * Test foreach.
 */
TEST(nuuk_list_foreach, {

  uint32_t input[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

  /*
   * Single remove.
   */
  DESCRIBE("nuuk_list_foreach()", {

    IT("should iterate through the list", {
      ASSERT(nuuk_list_prepend_range(LIST, 10, &input) == NUUK_LIST_OK);
      uint32_t total = 0;
      ASSERT(nuuk_list_foreach(LIST, _nuuk_list_test_foreach_fn_add, &total));
      ASSERT(total == 45);
    })

    IT("should stop iterating when `fn` returns false", {
      ASSERT(nuuk_list_prepend_range(LIST, 10, &input) == NUUK_LIST_OK);
      uint32_t total = 0;
      ASSERT(!nuuk_list_foreach(LIST, _nuuk_list_test_foreach_fn_stop, &total));
      ASSERT(total == 0);
    })

  })

})


/******************************************************************************
 * ENTRY POINT
 */

TEST(nuuk_list, {

  // Setup fixtures.
  SETUP(_nuuk_list_test_setup, udata);
  TEARDOWN(_nuuk_list_test_teardown, udata);

  // Run tests.
  RUN(nuuk_list_put, udata);
  RUN(nuuk_list_insert, udata);
  RUN(nuuk_list_append, udata);
  RUN(nuuk_list_prepend, udata);
  RUN(nuuk_list_remove, udata);
  RUN(nuuk_list_foreach, udata);

})
