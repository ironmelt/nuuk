/*
 * This file is part of Týr Server.
 *
 * Copyright 2014-2015 Tyr, Inc.
 *
 * Portions may be licensed to Tyr, Inc. under one or more contributor
 * license agreements.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */


/*****************************************************************************/


#include "testbench.h"

#include "nuuk/collection/ringmap.h"
#include "nuuk/util/alloc.h"
#include "nuuk/util/math.h"

#include "helper/test_map.h"


/******************************************************************************
 * TEST CONFIG
 */

#define DEFAULT_CAPACITY    16
#define DEFAULT_LOAD_FACTOR 0.75


/******************************************************************************
 * HELPER MACROS
 */

#define RINGMAP(__map)              ((nuuk_ringmap_t *) __map)
#define ACTUAL_CAPACITY(__capacity) nuuk_next_pow2((uint32_t) ((__capacity) / DEFAULT_LOAD_FACTOR))


/******************************************************************************
 * SPECIALIZED TESTS
 */

TEST(nuuk_ringmap_init, {

  DESCRIBE(".nuuk_ringmap_init()", {

    IT("should work with null load factor", {
      nuuk_ringmap_t map;
      ASSERT(NUUK_MAP_OK == nuuk_ringmap_init(&map, &nuuk_map_test_element_type, DEFAULT_CAPACITY, 0));
      nuuk_ringmap_destroy(&map);
    })

    IT("should work with null capacity", {
      nuuk_ringmap_t map;
      ASSERT(NUUK_MAP_OK == nuuk_ringmap_init(&map, &nuuk_map_test_element_type, 0, DEFAULT_LOAD_FACTOR));
      ASSERT(NULL == map.elements);
      ASSERT(NUUK_MAP_OK == NUUK_MAP_TEST_PUT(&map, 0, 0, NULL).status);
      nuuk_ringmap_destroy(&map);
    })

  })

})

TEST(nuuk_ringmap_put, {

  DESCRIBE(".nuuk_ringmap_put()", {

    IT("should grow capacity as needed", {
      for (uint32_t i = 0; i < DEFAULT_CAPACITY * 100; ++i) {
        ASSERT(NUUK_MAP_OK == NUUK_MAP_TEST_PUT(fixtures, i, i, NULL).status);
      }
      uint64_t value;
      for (uint32_t i = 0; i < DEFAULT_CAPACITY * 100; ++i) {
        value = NUUK_MAP_TEST_GET_VALUE(fixtures, i);
        ASSERT(i == value);
      }
    })

  })

})

TEST(nuuk_ringmap_capacity, {

  DESCRIBE(".nuuk_ringmap_capacity()", {

    IT("should return the capacity of the map", {
      ASSERT(ACTUAL_CAPACITY(DEFAULT_CAPACITY) == nuuk_ringmap_capacity(RINGMAP(fixtures)));
    })

  })

})

TEST(nuuk_ringmap_ensure_capacity, {

  DESCRIBE(".nuuk_ringmap_ensure_capacity()", {

    IT("should not increase the capacity when not needed", {
      nuuk_ringmap_ensure_capacity(RINGMAP(fixtures), DEFAULT_CAPACITY);
      ASSERT(ACTUAL_CAPACITY(DEFAULT_CAPACITY) == nuuk_ringmap_capacity(RINGMAP(fixtures)));
      nuuk_ringmap_ensure_capacity(RINGMAP(fixtures), DEFAULT_CAPACITY - 1);
      ASSERT(ACTUAL_CAPACITY(DEFAULT_CAPACITY) == nuuk_ringmap_capacity(RINGMAP(fixtures)));
    })

    IT("should increase the capacity when needed", {
      nuuk_ringmap_ensure_capacity(RINGMAP(fixtures), DEFAULT_CAPACITY + 1);
      ASSERT(ACTUAL_CAPACITY(DEFAULT_CAPACITY + 1) == nuuk_ringmap_capacity(RINGMAP(fixtures)));
      nuuk_ringmap_ensure_capacity(RINGMAP(fixtures), DEFAULT_CAPACITY * 10);
      ASSERT(ACTUAL_CAPACITY(DEFAULT_CAPACITY * 10) == nuuk_ringmap_capacity(RINGMAP(fixtures)));
    })

  })

})

TEST(nuuk_ringmap_heap, {

  DESCRIBE("with storage on stack", {

    IT("should support allocation", {
      nuuk_ringmap_t map;
      nuuk_ringmap_inita(&map, &nuuk_map_test_element_type, DEFAULT_CAPACITY);
      nuuk_ringmap_destroy(&map);
    })

    IT("should allocate exact capacity", {
      nuuk_ringmap_t map;
      nuuk_ringmap_inita(&map, &nuuk_map_test_element_type, DEFAULT_CAPACITY);
      ASSERT(DEFAULT_CAPACITY == nuuk_ringmap_capacity(RINGMAP(&map)));
      nuuk_ringmap_destroy(&map);
    })

    IT("should work as a dynamically-sized map up to the allocated size", {
      nuuk_ringmap_t map;
      nuuk_ringmap_inita(&map, &nuuk_map_test_element_type, DEFAULT_CAPACITY);
      for (uint32_t i = 0; i < DEFAULT_CAPACITY; ++i) {
        ASSERT(NUUK_MAP_OK == NUUK_MAP_TEST_PUT(&map, i, i, NULL).status);
      }
      for (uint32_t i = 0; i < DEFAULT_CAPACITY; ++i) {
        ASSERT(i == NUUK_MAP_TEST_GET_VALUE(&map, i));
      }
      nuuk_ringmap_destroy(&map);
    })

    IT("should return NUUK_MAP_ERR_MAX on insert beyond the allocated size", {
      nuuk_ringmap_t map;
      nuuk_ringmap_inita(&map, &nuuk_map_test_element_type, DEFAULT_CAPACITY);
      for (uint32_t i = 0; i < DEFAULT_CAPACITY * 10; ++i) {
        ASSERT(i >= DEFAULT_CAPACITY ?
            NUUK_MAP_ERR_MAX : NUUK_MAP_OK == NUUK_MAP_TEST_PUT(&map, i, i, NULL).status);
      }
      for (uint32_t i = 0; i < DEFAULT_CAPACITY; ++i) {
        ASSERT(i == NUUK_MAP_TEST_GET_VALUE(&map, i));
      }
      nuuk_ringmap_destroy(&map);
    })

    IT("should support updates anyway", {
      nuuk_ringmap_t map;
      nuuk_ringmap_inita(&map, &nuuk_map_test_element_type, DEFAULT_CAPACITY);
      for (uint32_t i = 0; i < DEFAULT_CAPACITY; ++i) {
        ASSERT(NUUK_MAP_OK == NUUK_MAP_TEST_PUT(&map, i, i, NULL).status);
      }
      for (uint32_t i = 0; i < DEFAULT_CAPACITY; ++i) {
        ASSERT(NUUK_MAP_OK == NUUK_MAP_TEST_PUT(&map, i, i + 1, NULL).status);
      }
      for (uint32_t i = 0; i < DEFAULT_CAPACITY; ++i) {
        ASSERT(i + 1 == NUUK_MAP_TEST_GET_VALUE(&map, i));
      }
      nuuk_ringmap_destroy(&map);
    })

  })

})


/******************************************************************************
 * HOOKS / FIXTURES
 */

static
nuuk_map_t * _nuuk_ringmap_test_hook_new() {
  nuuk_map_t * map = nuuk_alloc(sizeof(nuuk_ringmap_t));
  if (!map || nuuk_ringmap_init((nuuk_ringmap_t *) map, &nuuk_map_test_element_type, DEFAULT_CAPACITY,
      DEFAULT_LOAD_FACTOR) != NUUK_MAP_OK) {
    return NULL;
  }
  return map;
}

static
void _nuuk_ringmap_test_hook_free(nuuk_map_t * map) {
  nuuk_ringmap_destroy((nuuk_ringmap_t *) map);
  nuuk_free(map);
}

static
void * _nuuk_ringmap_test_setup(void * fixtures) {
  return (void *) _nuuk_ringmap_test_hook_new();
}

static
void _nuuk_ringmap_test_teardown(void * fixtures, void * context) {
  nuuk_ringmap_t * map = (nuuk_ringmap_t *) context;
  _nuuk_ringmap_test_hook_free((nuuk_map_t *) map);
}

static
nuuk_map_test_hooks_t nuuk_ringmap_test_hooks = {
  .new  = _nuuk_ringmap_test_hook_new,
  .free = _nuuk_ringmap_test_hook_free
};


/******************************************************************************
 * TEST RUNNER
 */

int main(void) {

  // Run the test cases here.
  DESCRIBE("nuuk_ringmap_t", {

    // Should initialize correctly.
    RUN(nuuk_ringmap_init, NULL);

    // Execute standard map tests.
    DESCRIBE("should pass nuuk_map tests", {
      RUN(nuuk_map, &nuuk_ringmap_test_hooks);
    })

    // Setup for specialized tests.
    SETUP(_nuuk_ringmap_test_setup, NULL);
    TEARDOWN(_nuuk_ringmap_test_teardown, NULL);

    // Run them.
    RUN(nuuk_ringmap_put, NULL);
    RUN(nuuk_ringmap_capacity, NULL);
    RUN(nuuk_ringmap_ensure_capacity, NULL);

    // Prepare for heap-allocated map (we want to allocate inline).
    SETUP(NULL, NULL);
    TEARDOWN(NULL, NULL);

    // Run them.
    RUN(nuuk_ringmap_heap, NULL);

  })

  // Done. Print the results, and exit.
  return RESULTS();

}
