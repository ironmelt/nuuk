/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

#define NUUK_TEST


/*****************************************************************************/


#include "testbench.h"

#include "nuuk/collection/arraylist.h"
#include "helper/test_list.h"
#include "nuuk/util/alloc.h"


/******************************************************************************
 * TEST CONFIG
 */

#define DEFAULT_CAPACITY   (8)
#define DEFAULT_BLOCK_SIZE (16)


/******************************************************************************
 * MACROS
 */

#define ARRAYLIST ((nuuk_arraylist_t *) fixtures)


/******************************************************************************
 * FIXTURES
 */

static
nuuk_list_t * _nuuk_arraylist_test_hook_new() {
  nuuk_list_t * list = nuuk_alloc(sizeof(nuuk_arraylist_t));
  if (!list ||
      nuuk_arraylist_init(
          (nuuk_arraylist_t *) list, sizeof(uint32_t), DEFAULT_CAPACITY, DEFAULT_BLOCK_SIZE) != NUUK_ARRAYLIST_OK) {
    return NULL;
  }
  return list;
}

static
void _nuuk_arraylist_test_hook_free(nuuk_list_t * list) {
  nuuk_arraylist_destroy((nuuk_arraylist_t *) list);
  nuuk_free(list);
}

static
void * _nuuk_arraylist_test_setup(void * udata) {
  return (void *) _nuuk_arraylist_test_hook_new();
}

static
void _nuuk_arraylist_test_teardown(void * udata, void * fixtures) {
  nuuk_arraylist_t * list = (nuuk_arraylist_t *) fixtures;
  _nuuk_arraylist_test_hook_free((nuuk_list_t *) list);
}

static
nuuk_list_test_hooks_t _nuuk_arraylist_test_hooks = {
  .new  = _nuuk_arraylist_test_hook_new,
  .free = _nuuk_arraylist_test_hook_free
};


/******************************************************************************
 * TESTS
 */

/**
 * Test arraylist initialization.
 */
TEST(nuuk_test_arraylist_init, {

  DESCRIBE("nuuk_arraylist_init()", {

    IT("should initialize the arraylist", {
      nuuk_arraylist_t list;
      ASSERT(nuuk_arraylist_init(&list, sizeof(uint32_t), 10, 20) == NUUK_ARRAYLIST_OK);
      nuuk_arraylist_destroy(&list);
    })

    IT("should allocate a multiple of `block_size` for capacity", {
      nuuk_arraylist_t list;
      nuuk_arraylist_init(&list, sizeof(uint32_t), 10, 20);
      ASSERT(nuuk_arraylist_capacity(&list) == 20);
      nuuk_arraylist_destroy(&list);
      nuuk_arraylist_init(&list, sizeof(uint32_t), 30, 20);
      ASSERT(nuuk_arraylist_capacity(&list) == 40);
      nuuk_arraylist_destroy(&list);
    })

    IT("should not allocate anything when `capacity` equals 0", {
      nuuk_arraylist_t list;
      nuuk_arraylist_init(&list, sizeof(uint32_t), 0, 20);
      ASSERT(nuuk_arraylist_capacity(&list) == 0);
      ASSERT(!list.elements);
      nuuk_arraylist_destroy(&list);
    })

    IT("should allocate exactly `capacity` when `block_size` equals 0", {
      nuuk_arraylist_t list;
      nuuk_arraylist_init(&list, sizeof(uint32_t), 10, 0);
      ASSERT(nuuk_arraylist_capacity(&list) == 10);
      nuuk_arraylist_destroy(&list);
    })

  })

})


/**
 * Test arraylist compliance to list interface.
 */
TEST(nuuk_test_arraylist_list_interface, {

  DESCRIBE("should comply to `nuuk_list_t` interface", {
    RUN(nuuk_list, &_nuuk_arraylist_test_hooks);
  })

})


/**
 * Test arraylist compliance to list interface.
 */
TEST(nuuk_test_arraylist_ensure_capacity, {

  DESCRIBE("nuuk_arraylist_ensure_capacity()", {

    SETUP(_nuuk_arraylist_test_setup, NULL);
    TEARDOWN(_nuuk_arraylist_test_teardown, NULL);

    IT("should actually increase capacity", {
      ASSERT(nuuk_arraylist_ensure_capacity(ARRAYLIST, 2 * DEFAULT_BLOCK_SIZE) == NUUK_ARRAYLIST_OK);
      ASSERT(nuuk_arraylist_capacity(ARRAYLIST) >= 2 * DEFAULT_BLOCK_SIZE);
    })

    IT("should increase capacity by a multiple of `block_size`", {
      ASSERT(nuuk_arraylist_ensure_capacity(ARRAYLIST, 2 * DEFAULT_BLOCK_SIZE + 1) == NUUK_ARRAYLIST_OK);
      ASSERT(nuuk_arraylist_capacity(ARRAYLIST) == 3 * DEFAULT_BLOCK_SIZE);
    })

    IT("should not do anything if desired capacity is lower or equal to current capacity", {
      uint32_t c = nuuk_arraylist_capacity(ARRAYLIST);
      ASSERT(nuuk_arraylist_ensure_capacity(ARRAYLIST, c - 1) == NUUK_ARRAYLIST_OK);
      ASSERT(nuuk_arraylist_capacity(ARRAYLIST) == c);
      ASSERT(nuuk_arraylist_ensure_capacity(ARRAYLIST, c) == NUUK_ARRAYLIST_OK);
      ASSERT(nuuk_arraylist_capacity(ARRAYLIST) == c);
      ASSERT(nuuk_arraylist_ensure_capacity(ARRAYLIST, 0) == NUUK_ARRAYLIST_OK);
      ASSERT(nuuk_arraylist_capacity(ARRAYLIST) == c);
    })

  })

})


/**
 * Test stack-allocated arraylist.
 */
TEST(nuuk_test_arraylist_stack, {

  DESCRIBE("stack-allocated arraylist", {

    uint32_t input[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    uint32_t output = 0;

    IT("should be allocated correctly", {
      nuuk_arraylist_t list;
      nuuk_arraylist_inita(&list, sizeof(uint32_t), 10);
      nuuk_arraylist_destroy(&list);
    })

    IT("should be able to be filled entirely", {
      nuuk_arraylist_t list;
      nuuk_arraylist_inita(&list, sizeof(uint32_t), 10);
      ASSERT(nuuk_arraylist_prepend_range(&list, 10, &input) == NUUK_ARRAYLIST_OK);
      for (uint32_t i = 0; i < 10; ++i) {
        output = 0;
        nuuk_arraylist_get(&list, i, &output);
        ASSERT(output == i);
      }
      nuuk_arraylist_destroy(&list);
    })

    IT("should return `NUUK_ARRAYLIST_ERR_MAX` on operations increasing the size when full", {
      nuuk_arraylist_t list;
      nuuk_arraylist_inita(&list, sizeof(uint32_t), 10);
      ASSERT(nuuk_arraylist_prepend_range(&list, 10, &input) == NUUK_ARRAYLIST_OK);
      ASSERT(nuuk_arraylist_prepend_range(&list, 10, &input) == NUUK_ARRAYLIST_ERR_MAX);
      ASSERT(nuuk_arraylist_append_range(&list, 10, &input) == NUUK_ARRAYLIST_ERR_MAX);
      ASSERT(nuuk_arraylist_insert_range(&list, 1, 10, &input) == NUUK_ARRAYLIST_ERR_MAX);
      nuuk_arraylist_destroy(&list);
    })

  })

})


/******************************************************************************
 * RUNNER
 */

int main() {

  DESCRIBE("nuuk/collection/arraylist", {
    RUN(nuuk_test_arraylist_init, NULL);
    RUN(nuuk_test_arraylist_list_interface, NULL);
    RUN(nuuk_test_arraylist_ensure_capacity, NULL);
    RUN(nuuk_test_arraylist_stack, NULL);
  })

  return RESULTS();
}
