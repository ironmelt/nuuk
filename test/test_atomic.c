/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

#define NUUK_TEST


/*****************************************************************************/


#include "testbench.h"

#include "nuuk/concurrent/atomic.h"


/******************************************************************************
 * TESTS
 */

/**
 * Test atomic initialization.
 */
TEST(nuuk_test_atomic_init, {

  DESCRIBE("ATOMIC_VAR_INIT()", {
    IT("should initialize the variable", {
      atomic_uint8_t foo = ATOMIC_VAR_INIT(42);
      ASSERT(atomic_load(&foo) == 42);
    })
  })

  DESCRIBE("atomic_init()", {
    IT("should initialize the variable", {
      atomic_uint8_t foo;
      atomic_init(&foo, 42);
      ASSERT(atomic_load(&foo) == 42);
    })
  })

})

/**
 * Test atomic load and store.
 */
TEST(nuuk_test_atomic_load_store, {

  DESCRIBE("atomic_store()", {
    IT("should store a new value", {
      atomic_uint8_t foo = ATOMIC_VAR_INIT(42);
      atomic_store(&foo, 43);
      ASSERT(atomic_load(&foo) == 43);
    })
  })

  DESCRIBE("atomic_load()", {
    IT("should load the value", {
      atomic_uint8_t foo = ATOMIC_VAR_INIT(42);
      atomic_store(&foo, 43);
      ASSERT(atomic_load(&foo) == 43);
      atomic_store(&foo, 44);
      ASSERT(atomic_load(&foo) == 44);
    })
  })

})

/**
 * Test atomic CAS.
 */
TEST(nuuk_test_atomic_cas, {

  DESCRIBE("atomic_compare_exchange_strong()", {

    IT("should swap the value when expected == current", {
      atomic_uint8_t foo = ATOMIC_VAR_INIT(42);
      uint8_t expected = 42;
      ASSERT(atomic_compare_exchange_strong(&foo, &expected, 43));
      ASSERT(atomic_load(&foo) == 43);
    })

    IT("should not swap the value and update expected when expected != current", {
      atomic_uint8_t foo = ATOMIC_VAR_INIT(42);
      uint8_t expected = 43;
      ASSERT(!atomic_compare_exchange_strong(&foo, &expected, 44));
      ASSERT(atomic_load(&foo) == 42);
      ASSERT(expected == 42);
    })

  })

  DESCRIBE("atomic_compare_exchange_weak()", {

    IT("should swap the value when expected == current", {
      atomic_uint8_t foo = ATOMIC_VAR_INIT(42);
      uint8_t expected = 42;
      ASSERT(atomic_compare_exchange_weak(&foo, &expected, 43));
      ASSERT(atomic_load(&foo) == 43);
    })

    IT("should not swap the value and update expected when expected != current", {
      atomic_uint8_t foo = ATOMIC_VAR_INIT(42);
      uint8_t expected = 43;
      ASSERT(!atomic_compare_exchange_weak(&foo, &expected, 44));
      ASSERT(atomic_load(&foo) == 42);
      ASSERT(expected == 42);
    })

  })

})


/******************************************************************************
 * RUNNER
 */

int main() {

  DESCRIBE("nuuk/concurrent/atomic", {
    RUN(nuuk_test_atomic_init, NULL);
    RUN(nuuk_test_atomic_load_store, NULL);
    RUN(nuuk_test_atomic_cas, NULL);
  })

  return RESULTS();
}
