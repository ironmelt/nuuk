#
# General definitions.
#

cmake_minimum_required (VERSION 2.8)
project (nuuk)


#
# Set CMake policies.
#

if(POLICY CMP0042)
  cmake_policy(SET CMP0042 NEW)
endif()


#
# Set directories and paths.
#

set(CMAKE_BINARY_DIR ${CMAKE_BINARY_DIR}/bin)
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})
set(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR})

set(PROJECT_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)
set(PROJECT_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)
set(PROJECT_DEPS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/deps)

set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)


#
# Set C11 compile flags.
#

include(CheckCCompilerFlag)
CHECK_C_COMPILER_FLAG("-std=c11" COMPILER_SUPPORTS_C11)
CHECK_C_COMPILER_FLAG("-std=c0x" COMPILER_SUPPORTS_C0X)
if(COMPILER_SUPPORTS_C11)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c11")
elseif(COMPILER_SUPPORTS_C0X)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c0x")
else()
  message(STATUS "The compiler `${CMAKE_C_COMPILER}` has no C11 support. Please use a different C compiler.")
endif()


#
# Set compiler flags.
#

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O3 -Wall")


#
# Set linker flags.
#

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -flto")


#
# Find libraries.
#

# Threads
find_package(Threads REQUIRED)


#
# Set include directories.
#

include_directories("${PROJECT_INCLUDE_DIR}")
include_directories("${PROJECT_SOURCE_DIR}")

# Don't forget bundled dependencies.
include_directories("${PROJECT_DEPS_DIR}/testbench/include")


#
# Enable testing.
#

enable_testing()


#
# Include subdirectories.
#

add_subdirectory(src)
add_subdirectory(test)
