/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include <string.h>

#include "nuuk/collection/arraylist.h"
#include "nuuk/util/alloc.h"
#include "nuuk/util/math.h"

#include "arraylist_private.h"


/******************************************************************************
 * STATIC FUNCTIONS
 */

inline
void _nuuk_arraylist_write(nuuk_arraylist_t * list, uint32_t index, uint32_t n_elements, bool shift, void * src) {

  // Shift previous elements, if necessary.
  if (shift && index < list->size) {
    char * move_src_start = &NUUK_ARRAYLIST_ELEMENT(list, index);
    char * move_dest_start = &NUUK_ARRAYLIST_ELEMENT(list, index + n_elements);
    uint32_t move_bytes = (list->size - index) * list->element_size;
    memmove(move_dest_start, move_src_start, move_bytes);
  }

  // Copy new elements.
  char * copy_start = &NUUK_ARRAYLIST_ELEMENT(list, index);
  uint32_t copy_bytes = n_elements * list->element_size;
  memcpy(copy_start, src, copy_bytes);
}


/******************************************************************************
 * INSTANCE FUNCTIONS
 */

nuuk_arraylist_status_t nuuk_arraylist_init(
    nuuk_arraylist_t * list, uint32_t element_size, uint32_t capacity, uint32_t block_size) {

  // Initialize the `nuuk_list_t`.
  nuuk_list_interface_init((nuuk_list_t *) list, &nuuk_arraylist_hooks);

  // Initialize struct fields.
  list->element_size = element_size;
  list->block_size = block_size;
  list->size = 0;
  list->free_elements = true;

  // Allocate the element array if necessary.
  list->capacity = 0;
  list->elements = NULL;
  nuuk_arraylist_status_t ensure_status = nuuk_arraylist_ensure_capacity(list, capacity);
  if (ensure_status != NUUK_ARRAYLIST_OK) {
    return ensure_status;
  }

  return NUUK_ARRAYLIST_OK;
}

void nuuk_arraylist_destroy(nuuk_arraylist_t * list) {

  // Destroy the element array.
  if (list->free_elements) {
    nuuk_free(list->elements);
  }

  // Discard the list.
  nuuk_list_interface_destroy((nuuk_list_t *) list);
}

nuuk_arraylist_status_t nuuk_arraylist_ensure_capacity(nuuk_arraylist_t * list, uint32_t capacity) {

  // Maybe we don't need to do anything.
  if (capacity <= list->capacity) {
    return NUUK_ARRAYLIST_OK;
  }

  // Maybe the list has a fixed size.
  if (list->block_size == 0 && list->elements) {
    return NUUK_ARRAYLIST_ERR_MAX;
  }

  // Else, calculate how much space we need to allocate.
  uint32_t needed_capacity;
  if (list->block_size) {
    uint32_t needed_blocks = (capacity / list->block_size) + 1;
    needed_capacity = needed_blocks * list->block_size;
  } else {
    needed_capacity = capacity;
    if (capacity == 0) {
      return NUUK_ARRAYLIST_OK;
    }
  }
  uint32_t needed_bytes = needed_capacity * list->element_size;

  // Reallocate memory.
  char * new_elements;
  if (list->capacity) {
    new_elements = nuuk_realloc(list->elements, needed_bytes);
  } else {
    new_elements = nuuk_alloc(needed_bytes);
  }
  if (new_elements == NULL) {
    return NUUK_ARRAYLIST_ERR_ALLOC;
  }
  list->elements = new_elements;

  // Zero-out every element beyond original pointers.
  char * zero_start = &NUUK_ARRAYLIST_ELEMENT(list, list->capacity);
  uint32_t zero_bytes = needed_bytes - (list->capacity * list->element_size);
  memset(zero_start, 0, zero_bytes);

  // Done.
  list->capacity = needed_capacity;
  return NUUK_ARRAYLIST_OK;
}


/******************************************************************************
 * READ FUNCTIONS
 */

nuuk_arraylist_status_t nuuk_arraylist_get_range(
    nuuk_arraylist_t * list, uint32_t start_index, uint32_t end_index, void * dest) {

  // Ensure we are in bounds.
  if (end_index > list->size || start_index > end_index) {
    return NUUK_ARRAYLIST_ERR_INDEX;
  }

  // Position at the first element, and copy data.
  char * copy_start = &NUUK_ARRAYLIST_ELEMENT(list, start_index);
  uint32_t copy_bytes = (end_index - start_index) * list->element_size;
  memcpy(dest, copy_start, copy_bytes);

  return NUUK_ARRAYLIST_OK;
}


/******************************************************************************
 * WRITE FUNCTIONS
 */

nuuk_arraylist_status_t nuuk_arraylist_put_range(
    nuuk_arraylist_t * list, uint32_t index, uint32_t n_elements, void * src) {

  // Ensure we have sufficient capacity.
  uint32_t future_size = nuuk_max(index + n_elements, list->size);
  nuuk_arraylist_status_t ensure_status = nuuk_arraylist_ensure_capacity(list, future_size);
  if (ensure_status != NUUK_ARRAYLIST_OK) {
    return ensure_status;
  }

  // Copy new elements.
  _nuuk_arraylist_write(list, index, n_elements, false, src);

  list->size = future_size;
  return NUUK_ARRAYLIST_OK;
}

nuuk_arraylist_status_t nuuk_arraylist_insert_range(
    nuuk_arraylist_t * list, uint32_t index, uint32_t n_elements, void * src) {

  // Ensure we have sufficient capacity.
  uint32_t end_index = index + n_elements;
  uint32_t future_size = nuuk_max(list->size + n_elements, end_index);
  nuuk_arraylist_status_t ensure_status = nuuk_arraylist_ensure_capacity(list, future_size);
  if (ensure_status != NUUK_ARRAYLIST_OK) {
    return ensure_status;
  }

  // Copy new elements.
  _nuuk_arraylist_write(list, index, n_elements, true, src);

  list->size = future_size;
  return NUUK_ARRAYLIST_OK;
}


/******************************************************************************
 * DELETE FUNCTIONS
 */

nuuk_arraylist_status_t nuuk_arraylist_remove_range(nuuk_arraylist_t * list, uint32_t start_index, uint32_t end_index) {

  // Ensure we are in bounds.
  if (end_index > list->size || start_index > end_index) {
    return NUUK_ARRAYLIST_ERR_INDEX;
  }

  // Copy the remaining elements of the list.
  char * move_start = &NUUK_ARRAYLIST_ELEMENT(list, end_index);
  char * move_dest = &NUUK_ARRAYLIST_ELEMENT(list, start_index);
  uint32_t move_bytes = (list->size - end_index) * list->element_size;
  memmove(move_dest, move_start, move_bytes);

  // Zero-out the freed space.
  uint32_t zero_bytes = (end_index - start_index) * list->element_size;
  memset(move_dest + move_bytes, 0, zero_bytes);

  list->size -= end_index - start_index;
  return NUUK_ARRAYLIST_OK;
}


/******************************************************************************
 * MODIFICATION FUNCTIONS
 */

nuuk_arraylist_status_t nuuk_arraylist_trim(nuuk_arraylist_t * list, uint32_t index) {

  // Ensure we are in bounds.
  if (index > list->size) {
    return NUUK_ARRAYLIST_ERR_INDEX;
  }

  // Crop the list.
  list->size = index;

  return NUUK_ARRAYLIST_OK;
}


/******************************************************************************
 * ITERATION FUNCTIONS
 */

bool nuuk_arraylist_foreach(nuuk_arraylist_t * list, nuuk_list_foreach_fn_t fn, void * udata) {
  uint32_t size = nuuk_arraylist_size(list);
  for (uint32_t index = 0; index < size; ++index) {
    void * element = &NUUK_ARRAYLIST_ELEMENT(list, index);
    if (!fn(element, udata)) {
      return false;
    }
  }
  return true;
}
