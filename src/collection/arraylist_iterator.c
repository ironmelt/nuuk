/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include "nuuk/collection/arraylist_iterator.h"

#include "arraylist_private.h"


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

void nuuk_arraylist_iterator_init(nuuk_arraylist_iterator_t * iter, nuuk_arraylist_t * list) {
  nuuk_iterator_interface_init((nuuk_iterator_t *) iter, &nuuk_arraylist_iterator_hooks);
  iter->list = list;
  iter->index = 0;
}

void nuuk_arraylist_iterator_destroy(nuuk_arraylist_iterator_t * iter) {
  nuuk_iterator_interface_destroy((nuuk_iterator_t *) iter);
}

/******************************************************************************
 * ITERATOR FUNCTIONS
 */

void * nuuk_arraylist_iterator_next(nuuk_arraylist_iterator_t * iter) {
  return nuuk_arraylist_iterator_has_next(iter) ? &NUUK_ARRAYLIST_ELEMENT(iter->list, iter->index++) : NULL;
}
