/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include "nuuk/collection/arraylist_iterator.h"


/******************************************************************************
 * HOOKS
 */

static
void _nuuk_arraylist_iterator_hook_destroy(nuuk_iterator_t * iter) {
  nuuk_arraylist_iterator_destroy((nuuk_arraylist_iterator_t *) iter);
}

static
bool _nuuk_arraylist_iterator_hook_has_next(nuuk_iterator_t * iter) {
  return nuuk_arraylist_iterator_has_next((nuuk_arraylist_iterator_t *) iter);
}

static
void * _nuuk_arraylist_iterator_hook_next(nuuk_iterator_t * iter) {
  return nuuk_arraylist_iterator_next((nuuk_arraylist_iterator_t *) iter);
}


/******************************************************************************
 * HOOKS DEFINITION
 */

const nuuk_iterator_hooks_t nuuk_arraylist_iterator_hooks = {
  .destroy = _nuuk_arraylist_iterator_hook_destroy,
  .has_next = _nuuk_arraylist_iterator_hook_has_next,
  .next = _nuuk_arraylist_iterator_hook_next
};
