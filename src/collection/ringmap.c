/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include "nuuk/collection/ringmap.h"
#include "nuuk/util/math.h"


/******************************************************************************
 * MACROS
 */

#define ELEMENT_TYPE(__map)           (nuuk_map_element_type((nuuk_map_t *) __map))
#define MAP_RECORD(__record)          ((nuuk_map_record_t *) &__record->map_record)
#define RECORD_KEY(__map, __record)   (nuuk_map_record_key(ELEMENT_TYPE(__map), MAP_RECORD(__record)))
#define RECORD_VALUE(__map, __record) (nuuk_map_record_value(ELEMENT_TYPE(__map), MAP_RECORD(__record)))
#define KEY_SIZE(__map)               (nuuk_map_element_key_size(ELEMENT_TYPE(map)))
#define VALUE_SIZE(__map)             (nuuk_map_element_value_size(ELEMENT_TYPE(map)))

#define STATE_UNDEFINED               (0)
#define STATE_DELETED                 (1)
#define STATE_DEFINED                 (2)


/******************************************************************************
 * STATIC FUNCTIONS
 */

/**
 * Checks whether a key matches with a record.
 */
static inline
bool _nuuk_ringmap_record_key_match(
    const nuuk_ringmap_t * map, nuuk_ringmap_record_t * record, const void * key, uint32_t key_hash) {
  return
      key_hash == nuuk_map_record_hash(MAP_RECORD(record)) &&
      !nuuk_map_element_key_compare(ELEMENT_TYPE(map), RECORD_KEY(map, record), key);
}

/**
 * Get the record associated with the key `key` (hashed to `hash`).
 * In order to do some housekeeping, if another available open position can be found before the actual position of
 * the record, it will be copied to that position, and the previous position will be made available.
 */
static
nuuk_ringmap_record_t * _nuuk_ringmap_locate(
    const nuuk_ringmap_t * map, const char * elements, uint32_t capacity,
    const void * key, uint32_t hash, bool active, bool relocate, nuuk_ringmap_record_t ** prev_record) {

  // Compute start index.
  uint32_t start_index = nuuk_mod_pow2(hash, capacity);

  // Loop to find a defined bucket, and possible relocation candidates.
  nuuk_ringmap_record_t * first_available_record = NULL;
  nuuk_ringmap_record_t * first_matching_record = NULL;
  nuuk_ringmap_record_t * record = NULL;
  uint32_t record_size = NUUK_RINGMAP_RECORD_SIZE(ELEMENT_TYPE(map));
  uint32_t index = start_index;
  do {

    // Get the record.
    record = (nuuk_ringmap_record_t *) &elements[index * record_size];

    // Does the record match what we're trying to insert?
    if (_nuuk_ringmap_record_key_match(map, record, key, hash)) {
      first_matching_record = record;
      if (!first_available_record) {
        first_available_record = first_matching_record;
      }
      break;
    }

    // Maybe we found one available spot.
    if (record->state != STATE_DEFINED) {

      //If it is free, just stop, and maybe set as first available record. Nothing good is after.
      if (record->state == STATE_UNDEFINED) {
        if (!first_available_record) {
          first_available_record = record;
        }
        break;
      }

      // If just deleted, register it as candidate for relocation.
      if (record->state == STATE_DELETED && !first_available_record) {
        first_available_record = record;
      }

    }

    // Increment index, with looping.
    index = (index + 1) & capacity - 1;

  } while (index != start_index);

  // Set the previous record.
  if (prev_record) {
    *prev_record = first_matching_record;
  }

  // Copy records if required to, and return the new spot.
  bool do_relocate =
    relocate &&
    first_matching_record &&
    first_matching_record->state == STATE_DEFINED &&
    first_available_record &&
    first_matching_record != first_available_record;
  if (do_relocate) {
    memcpy(first_available_record, first_matching_record, record_size);
    first_matching_record->state = STATE_DELETED;
    return first_available_record;
  }

  // Get and return the record.
  record = active ? first_matching_record : first_available_record;
  if (active && record && record->state != STATE_DEFINED) {
    record = NULL;
  }

  return record;
}


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

nuuk_map_status_t nuuk_ringmap_init(
    nuuk_ringmap_t * map, const nuuk_map_element_type_t * element_type, uint32_t capacity, float load_factor) {

  // Initialize base interface.
  nuuk_map_interface_init((nuuk_map_t *) map, &nuuk_ringmap_hooks, element_type);

  // Set basic structure fields.
  map->size = 0;
  map->load_factor = load_factor;
  map->min_capacity = capacity;
  map->free_elements = true;

  // Allocate storage, if necessary.
  map->capacity = 0;
  map->elements = NULL;
  if (capacity > 0) {
    nuuk_map_status_t ensure_status = nuuk_ringmap_ensure_capacity(map, capacity);
    if (ensure_status != NUUK_MAP_OK) {
      return ensure_status;
    }
  } else {
    map->elements = NULL;
  }

  // Done.
  return NUUK_MAP_OK;
}

void nuuk_ringmap_destroy(nuuk_ringmap_t * map) {

  // By convention, return if `map` is `NULL`.
  if (!map) {
    return;
  }

  // Destroy base interface.
  nuuk_map_interface_destroy((nuuk_map_t *) map);

  // Free the current elements array.
  if (map->free_elements) {
    nuuk_free(map->elements);
  }
}


/******************************************************************************
 * INSTANCE FUNCTIONS
 */

nuuk_map_status_t nuuk_ringmap_ensure_capacity(nuuk_ringmap_t * map, uint32_t capacity) {

  // If the currently allocated capacity is enough, don't do anything.
  uint32_t needed_capacity = map->load_factor ? (capacity / map->load_factor) : capacity;
  if (needed_capacity <= map->capacity) {
    return NUUK_MAP_OK;
  }

  // If the map has a fixed size, return an error.
  if (map->elements && !map->load_factor) {
    return NUUK_MAP_ERR_MAX;
  }

  // Find how much space to allocate.
  uint32_t new_capacity = nuuk_next_pow2(needed_capacity);
  uint32_t record_size = NUUK_RINGMAP_RECORD_SIZE(ELEMENT_TYPE(map));
  uint32_t new_capacity_bytes = new_capacity * record_size;

  // Try to allocate the new space.
  char * new_elements = nuuk_alloc(new_capacity_bytes);
  if (!new_elements) {
    return NUUK_MAP_ERR_ALLOC;
  }

  // Zero-out the new space.
  memset(new_elements, 0, new_capacity_bytes);

  // Relocate every active element of the old map to the new one.
  if (map->elements) {
    for (uint32_t index = 0; index < map->capacity; ++index) {
      nuuk_ringmap_record_t * record = (nuuk_ringmap_record_t *) &map->elements[index * record_size];
      if (record->state == STATE_DEFINED) {
        uint32_t record_hash = nuuk_map_record_hash(MAP_RECORD(record));
        nuuk_ringmap_record_t * new_record = _nuuk_ringmap_locate(
            map, new_elements, new_capacity, RECORD_KEY(map, record), record_hash, false, false, NULL);
        if (!new_record) {
          nuuk_free(new_elements);
          return NUUK_MAP_ERR_ALLOC;
        }
        memcpy(new_record, record, record_size);
      }
    }
  }

  // Done. Replace the old elements with the new ones.
  nuuk_free(map->elements);
  map->elements = new_elements;
  map->capacity = new_capacity;

  return NUUK_MAP_OK;
}


/******************************************************************************
 * PUT FUNCTIONS
 */

nuuk_map_write_result_t nuuk_ringmap_put(nuuk_ringmap_t * map, const void * key, const void * value,
                                         void * prev_value) {

  // Let's ensure that we have the needed capacity.
  // If we seem to have reached max capacity, let it a chance to replace an existing value anyway.
  nuuk_map_status_t ensure_capacity_err = nuuk_ringmap_ensure_capacity(map, map->size + 1);
  if (ensure_capacity_err != NUUK_MAP_OK && ensure_capacity_err != NUUK_MAP_ERR_MAX) {
    return NUUK_MAP_WRITE_RESULT(ensure_capacity_err, false);
  }

  // First, let's hash that key.
  uint32_t key_hash = nuuk_map_element_key_hash(ELEMENT_TYPE(map), key);

  // Let's a welcoming space for that value.
  nuuk_ringmap_record_t * previous_record = NULL;
  nuuk_ringmap_record_t * record = _nuuk_ringmap_locate(
      map, map->elements, map->capacity, key, key_hash, false, false, &previous_record);
  if (!record) {
    return NUUK_MAP_WRITE_RESULT(NUUK_MAP_ERR_MAX, false);
  }

  // Looks like we found one... Put it in place. Key first, only if it is different as before, i.e. not defined.
  if (record->state != STATE_DEFINED) {
    memcpy(RECORD_KEY(map, record), key, KEY_SIZE(map));
    nuuk_map_record_hash_set(MAP_RECORD(record), key_hash);
    ++map->size;
  }

  // Copy the old value, if requested.
  char * value_pos = RECORD_VALUE(map, record);
  if (prev_value) {
    if (record->state == STATE_DEFINED) {
      memcpy(prev_value, value_pos, VALUE_SIZE(map));
    } else {
      memset(prev_value, 0, VALUE_SIZE(map));
    }
  }

  // Then the new value.
  memcpy(value_pos, value, VALUE_SIZE(map));

  // Set the state, and return.
  record->state = STATE_DEFINED;

  return NUUK_MAP_WRITE_RESULT(NUUK_MAP_OK, prev_value);
}


/******************************************************************************
 * GET FUNCTIONS
 */

bool nuuk_ringmap_has_key(nuuk_ringmap_t * map, const void * key) {
  uint32_t key_hash = nuuk_map_element_key_hash(ELEMENT_TYPE(map), key);
  return (bool) _nuuk_ringmap_locate(map, map->elements, map->capacity, key, key_hash, true, true, NULL);
}

bool nuuk_ringmap_get(nuuk_ringmap_t * map, const void * key, void * value) {
  uint32_t key_hash = nuuk_map_element_key_hash(ELEMENT_TYPE(map), key);
  nuuk_ringmap_record_t * record = _nuuk_ringmap_locate(
      map, map->elements, map->capacity, key, key_hash, true, true, NULL);
  if (!record) {
    return false;
  }
  memcpy(value, RECORD_VALUE(map, record), VALUE_SIZE(map));
  return true;
}


/******************************************************************************
 * DELETE FUNCTIONS
 */

nuuk_map_write_result_t nuuk_ringmap_remove(nuuk_ringmap_t * map, const void * key, void * prev_value) {
  uint32_t key_hash = nuuk_map_element_key_hash(ELEMENT_TYPE(map), key);
  nuuk_ringmap_record_t * record = _nuuk_ringmap_locate(
      map, map->elements, map->capacity, key, key_hash, true, false, NULL);
  if (!record) {
    return NUUK_MAP_WRITE_RESULT(NUUK_MAP_OK, false);
  }
  if (prev_value) {
    memcpy(prev_value, RECORD_VALUE(map, record), VALUE_SIZE(map));
  }
  record->state = STATE_DELETED;
  --map->size;
  return NUUK_MAP_WRITE_RESULT(NUUK_MAP_OK, true);
}
