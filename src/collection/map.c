/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include "nuuk/collection/map.h"


/******************************************************************************
 * EXTERN CONSTANTS
 */

const nuuk_map_write_result_t g_NUUK_MAP_WRITE_RESULT_HOOK_ERR = NUUK_MAP_WRITE_RESULT(NUUK_MAP_ERR_HOOK, false);

/******************************************************************************
 * INTERFACE FUNCTIONS
 */

void nuuk_map_interface_init(
    nuuk_map_t * map, const nuuk_map_hooks_t * hooks, const nuuk_map_element_type_t * element_type) {
  map->hooks = hooks;
  map->element_type = *element_type;
}
