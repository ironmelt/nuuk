/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include "nuuk/collection/arraylist.h"


/******************************************************************************
 * HOOKS
 */

static
void _nuuk_arraylist_hook_destroy(nuuk_list_t * list) {
  nuuk_arraylist_destroy((nuuk_arraylist_t *) list);
}

static
uint32_t _nuuk_arraylist_hook_size(nuuk_list_t * list) {
  return nuuk_arraylist_size((nuuk_arraylist_t *) list);
}

static
nuuk_list_status_t _nuuk_arraylist_hook_get_range(
    nuuk_list_t * list, uint32_t start_index, uint32_t end_index, void * dest) {
  return (nuuk_list_status_t) nuuk_arraylist_get_range((nuuk_arraylist_t *) list, start_index, end_index, dest);
}

static
nuuk_list_status_t _nuuk_arraylist_hook_get(nuuk_list_t * list, uint32_t index, void * dest) {
  return (nuuk_list_status_t) nuuk_arraylist_get((nuuk_arraylist_t *) list, index, dest);
}

static
nuuk_list_status_t _nuuk_arraylist_hook_put_range(nuuk_list_t * list, uint32_t index, uint32_t n_elements, void * src) {
  return (nuuk_list_status_t) nuuk_arraylist_put_range((nuuk_arraylist_t *) list, index, n_elements, src);
}

static
nuuk_list_status_t _nuuk_arraylist_hook_put(nuuk_list_t * list, uint32_t index, void * src) {
  return (nuuk_list_status_t) nuuk_arraylist_put((nuuk_arraylist_t *) list, index, src);
}

static
nuuk_list_status_t _nuuk_arraylist_hook_insert_range(
    nuuk_list_t * list, uint32_t index, uint32_t n_elements, void * src) {
  return (nuuk_list_status_t) nuuk_arraylist_insert_range((nuuk_arraylist_t *) list, index, n_elements, src);
}

static
nuuk_list_status_t _nuuk_arraylist_hook_insert(nuuk_list_t * list, uint32_t index, void * src) {
  return (nuuk_list_status_t) nuuk_arraylist_insert((nuuk_arraylist_t *) list, index, src);
}

static
nuuk_list_status_t _nuuk_arraylist_hook_append_range(nuuk_list_t * list, uint32_t n_elements, void * src) {
  return (nuuk_list_status_t) nuuk_arraylist_append_range((nuuk_arraylist_t *) list, n_elements, src);
}

static
nuuk_list_status_t _nuuk_arraylist_hook_append(nuuk_list_t * list, void * src) {
  return (nuuk_list_status_t) nuuk_arraylist_append((nuuk_arraylist_t *) list, src);
}

static
nuuk_list_status_t _nuuk_arraylist_hook_prepend_range(nuuk_list_t * list, uint32_t n_elements, void * src) {
  return (nuuk_list_status_t) nuuk_arraylist_prepend_range((nuuk_arraylist_t *) list, n_elements, src);
}

static
nuuk_list_status_t _nuuk_arraylist_hook_prepend(nuuk_list_t * list, void * src) {
  return (nuuk_list_status_t) nuuk_arraylist_prepend((nuuk_arraylist_t *) list, src);
}

static inline
nuuk_list_status_t _nuuk_arraylist_hook_remove_range(nuuk_list_t * list, uint32_t start_index, uint32_t end_index) {
  return (nuuk_list_status_t) nuuk_arraylist_remove_range((nuuk_arraylist_t *) list, start_index, end_index);
}

static inline
nuuk_list_status_t _nuuk_arraylist_hook_remove(nuuk_list_t * list, uint32_t index) {
  return (nuuk_list_status_t) nuuk_arraylist_remove((nuuk_arraylist_t *) list, index);
}

static inline
nuuk_list_status_t _nuuk_arraylist_hook_trim(nuuk_list_t * list, uint32_t index) {
  return (nuuk_list_status_t) nuuk_arraylist_trim((nuuk_arraylist_t *) list, index);
}

static inline
bool _nuuk_arraylist_hook_foreach(nuuk_list_t * list, nuuk_list_foreach_fn_t fn, void * udata) {
  return nuuk_arraylist_foreach((nuuk_arraylist_t *) list, fn, udata);
}



/******************************************************************************
 * HOOKS DEFINITION
 */

const nuuk_list_hooks_t nuuk_arraylist_hooks = {
  .destroy = _nuuk_arraylist_hook_destroy,
  .size = _nuuk_arraylist_hook_size,
  .get_range = _nuuk_arraylist_hook_get_range,
  .get = _nuuk_arraylist_hook_get,
  .put_range = _nuuk_arraylist_hook_put_range,
  .put = _nuuk_arraylist_hook_put,
  .insert = _nuuk_arraylist_hook_insert,
  .insert_range = _nuuk_arraylist_hook_insert_range,
  .append = _nuuk_arraylist_hook_append,
  .append_range = _nuuk_arraylist_hook_append_range,
  .prepend = _nuuk_arraylist_hook_prepend,
  .prepend_range = _nuuk_arraylist_hook_prepend_range,
  .remove = _nuuk_arraylist_hook_remove,
  .remove_range = _nuuk_arraylist_hook_remove_range,
  .trim = _nuuk_arraylist_hook_trim,
  .foreach = _nuuk_arraylist_hook_foreach
};
