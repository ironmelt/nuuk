/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include "nuuk/collection/concurrent/dsnmap.h"
#include "nuuk/util/math.h"

#include "dsnmap_fset_private.h"


/******************************************************************************
 * MACROS
 */

#define CURRENT_NODE(__map) ((void *) atomic_load(&(__map)->node))
#define ELEMENT_TYPE(__map) (nuuk_map_element_type((nuuk_map_t *) __map))


/******************************************************************************
 * TYPES
 */

/**
 * Dsnmap bucket.
 */
typedef struct nuuk_dsnmap_bucket_s {

  /**
   * Initialized flag.
   */
  atomic_char initialized;

  /**
   * Freezable set.
   */
  nuuk_dsnmap_fset_t fset;

} nuuk_dsnmap_bucket_t;

/**
 * Dsnmap node.
 */
typedef struct nuuk_dsnmap_node_s {

  /**
   * Prev node.
   */
  atomic_uintptr_t prev;

  /**
   * Capacity.
   */
  uint32_t n_buckets;

  /**
   * Buckets.
   */
  nuuk_dsnmap_bucket_t buckets[];

} nuuk_dsnmap_node_t;


/******************************************************************************
 * STATIC FUNCTIONS
 */

static
nuuk_dsnmap_node_t * _nuuk_dsnmap_node_new(uint32_t n_buckets, nuuk_dsnmap_node_t * prev) {
  nuuk_dsnmap_node_t * node = nuuk_alloc(sizeof(nuuk_dsnmap_node_t) + n_buckets * sizeof(nuuk_dsnmap_bucket_t));
  if (!node) {
    return node;
  }
  for (uint32_t index = 0; index < n_buckets; ++index) {
    atomic_init(&node->buckets[index].initialized, 0);
  }
  atomic_init(&node->prev, (intptr_t) prev);
  node->n_buckets = n_buckets;
  return node;
}

static
nuuk_dsnmap_node_t * _nuuk_dsnmap_acquire_current_node(nuuk_dsnmap_t * map) {
  nuuk_dsnmap_node_t * current_node = CURRENT_NODE(map);
  nuuk_dsnmap_node_t * expected = current_node;
  do {
    if (nuuk_hazard_static_set(&map->hazard, 0, current_node) != NUUK_HAZARD_OK) {
      return NULL;
    }
    current_node = CURRENT_NODE(map);
  } while (current_node != expected);
  return current_node;
}

static
void _nuuk_dsnmap_free_node(nuuk_dsnmap_t * map, nuuk_dsnmap_node_t * node) {
  if (!node) {
    return;
  }
  for (uint32_t index = 0; index < node->n_buckets; ++index) {
    if (atomic_load(&node->buckets[index].initialized)) {
      nuuk_dsnmap_fset_destroy(&node->buckets[index].fset, &map->fset_config);
    }
  }
  nuuk_free(node);
}

static
nuuk_map_status_t _nuuk_dsnmap_init_bucket(nuuk_dsnmap_t * map, nuuk_dsnmap_node_t * node, uint32_t bucket_no) {
  nuuk_dsnmap_bucket_t * bucket = &node->buckets[bucket_no];
  nuuk_dsnmap_fset_init(&bucket->fset, &map->fset_config);
  atomic_store(&bucket->initialized, 2);
  return NUUK_MAP_OK;
}

#if 0
static
nuuk_map_status_t _nuuk_dsnmap_resize(nuuk_dsnmap_t * map, nuuk_dsnmap_node_t * node, bool grow) {

  // Maybe we don't have to do anything.
  if (node->n_buckets <= 1 && !grow) {
    return NUUK_MAP_OK;
  }

  // Initialize all buckets.
  for (uint32_t bucket_no = 0; bucket_no < node->n_buckets; ++bucket_no) {
    nuuk_dsnmap_bucket_t * bucket = &node->buckets[bucket_no];
    char bucket_init_state = atomic_load(&bucket->initialized);
    if (bucket_init_state == 1) {
      while (atomic_load(&bucket->initialized) != 2) {
        // Just wait...
      }
    } else if (bucket_init_state == 0) {
      _nuuk_dsnmap_init_bucket(map, node, bucket_no);
    }
  }

  // Set new node.
  atomic_store(&node->prev, (intptr_t) NULL);
  uint32_t new_size = grow ? node->n_buckets * 2 : node->n_buckets / 2;
  nuuk_dsnmap_node_t * new_node = _nuuk_dsnmap_node_new(new_size, node);
  if (!new_node) {
    return NUUK_MAP_ERR_ALLOC;
  }
  atomic_compare_exchange_strong(&map->node, (intptr_t *) &node, (intptr_t) new_node);

  // Initialize every bucket from current node.
  return NUUK_MAP_OK;

}
#endif


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

nuuk_map_status_t nuuk_dsnmap_init(nuuk_dsnmap_t * map, const nuuk_map_element_type_t * element_type,
                                      uint32_t capacity, float load_factor) {

  // Initialize base interface.
  nuuk_map_interface_init((nuuk_map_t *) map, &nuuk_dsnmap_hooks, element_type);

  // Initialize hazard pointers.
  if (nuuk_hazard_init(&map->hazard, 2, true) != NUUK_HAZARD_OK) {
    return NUUK_MAP_ERR_UNKNOWN;
  }

  // Initialize fset config.
  nuuk_dsnmap_fset_config_init(&map->fset_config, &map->hazard, 1, element_type);

  // Allocate the required capacity.
  if (capacity) {
    nuuk_dsnmap_node_t * node = _nuuk_dsnmap_node_new(capacity, NULL);
    if (!node) {
      return NUUK_MAP_ERR_ALLOC;
    }
    atomic_init(&map->node, (intptr_t) node);
  } else {
    atomic_init(&map->node, (intptr_t) NULL);
  }

  // Initialize the other variables.
  atomic_init(&map->size, 0);

  return NUUK_MAP_OK;
}

void nuuk_dsnmap_destroy(nuuk_dsnmap_t * map) {
  if (!map) {
    return;
  }
  nuuk_dsnmap_node_t * node = CURRENT_NODE(map);
  if (node) {
    _nuuk_dsnmap_free_node(map, (nuuk_dsnmap_node_t *) atomic_load(&node->prev));
  }
  _nuuk_dsnmap_free_node(map, node);
  nuuk_hazard_destroy(&map->hazard);
  nuuk_dsnmap_fset_config_destroy(&map->fset_config);
  nuuk_map_interface_destroy((nuuk_map_t *) map);
}


/******************************************************************************
 * INFO FUNCTIONS
 */

uint32_t nuuk_dsnmap_size(nuuk_dsnmap_t * map) {
  return atomic_load(&map->size);
}


/******************************************************************************
 * PUT FUNCTIONS
 */

nuuk_map_write_result_t nuuk_dsnmap_put(nuuk_dsnmap_t * map, const void * key, const void * value, void * prev_value) {

  // Create fset operation.
  uint32_t key_hash = nuuk_map_element_key_hash(ELEMENT_TYPE(map), key);
  nuuk_dsnmap_fset_op_t fset_op = {
      .key = key,
      .value = value,
      .key_hash = key_hash,
  };

  // Apply the operation.
  nuuk_dsnmap_bucket_t * bucket;
  nuuk_dsnmap_fset_status_t fset_put_status;
  do {

    // Acquire current node.
    nuuk_dsnmap_node_t * current_node = _nuuk_dsnmap_acquire_current_node(map);

    // Get the relevant bucket for our data.
    uint32_t bucket_no = nuuk_mod_pow2(key_hash, current_node->n_buckets);
    bucket = &current_node->buckets[bucket_no];

    // Initialize if not done already.
    char bucket_state_check = 0;
    if (atomic_compare_exchange_strong(&bucket->initialized, &bucket_state_check, 1)) {
      nuuk_map_status_t init_status = _nuuk_dsnmap_init_bucket(map, current_node, bucket_no);
      if (init_status != NUUK_MAP_OK) {
        return NUUK_MAP_WRITE_RESULT(init_status, false);
      }
    }

    // Apply the operation.
    fset_put_status =
        (nuuk_dsnmap_fset_status_t) nuuk_dsnmap_fset_put(&bucket->fset, &map->fset_config, &fset_op, prev_value);

  } while(fset_put_status == NUUK_DSNMAP_FSET_ERR_FROZEN);

  // If alloc error, report it.
  if (fset_put_status == NUUK_DSNMAP_FSET_ERR_ALLOC) {
    return NUUK_MAP_WRITE_RESULT(NUUK_MAP_ERR_ALLOC, false);
  }

  // If size changed, increase map size.
  if (!fset_op.had_mapping) {
    atomic_fetch_add(&map->size, 1);
  }

  return NUUK_MAP_WRITE_RESULT(NUUK_MAP_OK, fset_op.had_mapping);
}


/******************************************************************************
 * GET FUNCTIONS
 */

bool nuuk_dsnmap_has_key(nuuk_dsnmap_t * map, const void * key) {
  return nuuk_dsnmap_get(map, key, NULL);
}

bool nuuk_dsnmap_get(nuuk_dsnmap_t * map, const void * key, void * value) {

  do {

    // Hash the key.
    uint32_t key_hash = nuuk_map_element_key_hash(ELEMENT_TYPE(map), key);

    // Acquire current node.
    nuuk_dsnmap_node_t * current_node = _nuuk_dsnmap_acquire_current_node(map);

    // Get the relevant bucket for our data.
    uint32_t bucket_no = nuuk_mod_pow2(key_hash, current_node->n_buckets);
    nuuk_dsnmap_bucket_t * bucket = &current_node->buckets[bucket_no];

    // If bucket is not initialized, try to get the prev bucket.
    if (atomic_load(&bucket->initialized) < 2) {
      nuuk_dsnmap_node_t * prev_node = (nuuk_dsnmap_node_t *) atomic_load(&current_node->prev);
      nuuk_dsnmap_node_t * prev_expected = current_node;
      if (!prev_node || !nuuk_hazard_static_set(&map->hazard, 0, current_node) != NUUK_HAZARD_OK) {
        return false;
      }
      prev_node = (nuuk_dsnmap_node_t *) atomic_load(&current_node->prev);
      if (current_node != prev_expected) {
        continue;
      }
      bucket_no = nuuk_mod_pow2(key_hash, prev_node->n_buckets);
      bucket = &prev_node->buckets[bucket_no];
    }

    return nuuk_dsnmap_fset_get(&bucket->fset, &map->fset_config, key, key_hash, value);

  } while (true);

}


/******************************************************************************
 * DELETE FUNCTIONS
 */

nuuk_map_write_result_t nuuk_dsnmap_remove(nuuk_dsnmap_t * map, const void * key, void * prev_value) {

    // Create fset operation.
    uint32_t key_hash = nuuk_map_element_key_hash(ELEMENT_TYPE(map), key);
    nuuk_dsnmap_fset_op_t fset_op = {
        .key = key,
        .value = NULL,
        .key_hash = key_hash,
    };

    // Apply the operation.
    nuuk_dsnmap_bucket_t * bucket;
    nuuk_dsnmap_fset_status_t fset_remove_status;
    do {

      // Acquire current node.
      nuuk_dsnmap_node_t * current_node = _nuuk_dsnmap_acquire_current_node(map);

      // Get the relevant bucket for our data.
      uint32_t bucket_no = nuuk_mod_pow2(key_hash, current_node->n_buckets);
      bucket = &current_node->buckets[bucket_no];

      // Initialize if not done already.
      char bucket_state_check = 0;
      if (atomic_compare_exchange_strong(&bucket->initialized, &bucket_state_check, 1)) {
        nuuk_map_status_t init_status = _nuuk_dsnmap_init_bucket(map, current_node, bucket_no);
        if (init_status != NUUK_MAP_OK) {
          return NUUK_MAP_WRITE_RESULT(init_status, false);
        }
      }

      // Apply the operation.
      fset_remove_status =
          (nuuk_dsnmap_fset_status_t) nuuk_dsnmap_fset_remove(&bucket->fset, &map->fset_config, &fset_op, prev_value);

    } while(fset_remove_status == NUUK_DSNMAP_FSET_ERR_FROZEN);

    // If alloc error, report it.
    if (fset_remove_status == NUUK_DSNMAP_FSET_ERR_ALLOC) {
      return NUUK_MAP_WRITE_RESULT(NUUK_MAP_ERR_ALLOC, false);
    }

    // If size changed, increase map size.
    if (fset_op.had_mapping) {
      atomic_fetch_sub(&map->size, 1);
    }

    return NUUK_MAP_WRITE_RESULT(NUUK_MAP_OK, fset_op.had_mapping);
}
