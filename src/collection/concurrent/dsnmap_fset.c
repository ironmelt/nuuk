/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include <string.h>

#include "nuuk/util/math.h"

#include "dsnmap_fset_private.h"


/******************************************************************************
 * TYPES
 */

typedef struct nuuk_dsnmap_fset_op_transform_args_s {

  /**
   * The fset.
   */
  nuuk_dsnmap_fset_t * fset;

  /**
   * The fset config.
   */
  const nuuk_dsnmap_fset_config_t * config;

  /**
   * The operation.
   */
  nuuk_dsnmap_fset_op_t * op;

  /**
   * The previous value.
   */
  void * prev_value;

} nuuk_dsnmap_fset_op_transform_args_t;


/******************************************************************************
 * STATIC CONSTANTS
 */

static const nuuk_dsnmap_fset_node_t fset_empty_node = {
    .frozen = false,
    .size = 0
};

static const nuuk_dsnmap_fset_node_t fset_empty_frozen_node = {
    .frozen = true,
    .size = 0
};


/******************************************************************************
 * STATIC FUNCTIONS
 */

static
bool _nuuk_dsnmap_fset_free_filter(void * ptr) {
  return ptr != &fset_empty_node && ptr != &fset_empty_frozen_node;
}

static inline
bool _nuuk_dsnmap_fset_record_matches(const nuuk_dsnmap_fset_config_t * config, nuuk_map_record_t * record,
                                      const void * key, uint32_t key_hash) {
  return record->hash == key_hash &&
         !nuuk_map_element_key_compare(config->element_type, nuuk_map_record_key(config->element_type, record), key);
}

static
nuuk_dsnmap_fset_node_t * _nuuk_dsnmap_fset_node_new(uint32_t record_size, uint32_t size, bool frozen) {
  uint32_t alloc_size = sizeof(nuuk_dsnmap_fset_node_t) + record_size * size;
  nuuk_dsnmap_fset_node_t * node = nuuk_alloc(alloc_size);
  if (!node) {
    return node;
  }
  node->frozen = frozen;
  node->size = size;
  return node;
}

static
int _nuuk_dsnmap_fset_insert_transform(void * cow_node, void ** transformed_node, void * udata) {

  // Get args.
  nuuk_dsnmap_fset_op_transform_args_t * args = (nuuk_dsnmap_fset_op_transform_args_t *) udata;
  nuuk_dsnmap_fset_node_t * node = (nuuk_dsnmap_fset_node_t *) cow_node;

  // Ensure the current node is not locked.
  if (node->frozen) {
    return NUUK_DSNMAP_FSET_ERR_FROZEN;
  }

  // Get appropriate index for insertion.
  uint32_t index = 0;
  uint32_t record_size = nuuk_map_record_size(args->config->element_type);
  for (; index < node->size; ++index) {
    nuuk_map_record_t * record = (nuuk_map_record_t *) &node->elements[index * record_size];
    if (_nuuk_dsnmap_fset_record_matches(args->config, record, args->op->key, args->op->key_hash)) {
      break;
    }
  }

  // Allocate a new elements array and copy old elements.
  uint32_t new_size = nuuk_max(node->size, index + 1);
  nuuk_dsnmap_fset_node_t * new_node = _nuuk_dsnmap_fset_node_new(record_size, new_size, false);
  if (!new_node) {
    return NUUK_DSNMAP_FSET_ERR_ALLOC;
  }
  memcpy(&new_node->elements[0], &node->elements[0], record_size * node->size);

  // Copy old element, if requested.
  nuuk_map_record_t * record = (nuuk_map_record_t *) &new_node->elements[index * record_size];
  if (args->prev_value && new_size == node->size) {
    memcpy(args->prev_value, nuuk_map_record_value(args->config->element_type, record),
           nuuk_map_element_value_size(args->config->element_type));
  }

  // Copy new element.
  nuuk_map_record_init(record, args->config->element_type, args->op->key, args->op->value, args->op->key_hash);

  // Has size changed?
  args->op->had_mapping = new_size == node->size;

  // Done!
  *transformed_node = new_node;
  return 0;
}

static
int _nuuk_dsnmap_fset_remove_transform(void * cow_node, void ** transformed_node, void * udata) {

  // Get args.
  nuuk_dsnmap_fset_op_transform_args_t * args = (nuuk_dsnmap_fset_op_transform_args_t *) udata;
  nuuk_dsnmap_fset_node_t * node = (nuuk_dsnmap_fset_node_t *) cow_node;

  // Ensure the current node is not locked.
  if (node->frozen) {
    return NUUK_DSNMAP_FSET_ERR_FROZEN;
  }

  // Get value index.
  uint32_t index = 0;
  uint32_t record_size = nuuk_map_record_size(args->config->element_type);
  for (; index < node->size; ++index) {
    nuuk_map_record_t * record = (nuuk_map_record_t *) &node->elements[index * record_size];
    if (_nuuk_dsnmap_fset_record_matches(args->config, record, args->op->key, args->op->key_hash)) {
      break;
    }
  }

  // Not found? Return.
  if (index == node->size) {
    *transformed_node = node;
    args->op->had_mapping = false;
    return 0;
  }

  // Copy old element, if requested.
  nuuk_map_record_t * record = (nuuk_map_record_t *) &node->elements[index * record_size];
  if (args->prev_value) {
    memcpy(args->prev_value, nuuk_map_record_value(args->config->element_type, record),
           nuuk_map_element_value_size(args->config->element_type));
  }

  // Allocate a new elements array and copy old elements.
  uint32_t new_size = node->size - 1;
  nuuk_dsnmap_fset_node_t * new_node = _nuuk_dsnmap_fset_node_new(record_size, new_size, false);
  if (!new_node) {
    return NUUK_DSNMAP_FSET_ERR_ALLOC;
  }
  memcpy(&new_node->elements[0], &node->elements[0], record_size * index);
  memcpy(&new_node->elements[index * record_size], &node->elements[(index + 1) * record_size],
         record_size * (node->size - index - 1));

  // Has size changed?
  args->op->had_mapping = true;

  // Done!
  *transformed_node = new_node;
  return 0;
}


/******************************************************************************
 * CONFIG FUNCTIONS
 */

void nuuk_dsnmap_fset_config_init(nuuk_dsnmap_fset_config_t * config, nuuk_hazard_t * hazard, uint32_t hazard_ptr_no,
                                  const nuuk_map_element_type_t * element_type) {
  config->cow_config.filter = _nuuk_dsnmap_fset_free_filter;
  config->cow_config.hazard = hazard;
  config->cow_config.hazard_ptr_no = hazard_ptr_no;
  config->element_type = element_type;
}


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

void nuuk_dsnmap_fset_init(nuuk_dsnmap_fset_t * fset, const nuuk_dsnmap_fset_config_t * config) {
  nuuk_cow_init(&fset->node, &config->cow_config, (void *) &fset_empty_node);
}

void nuuk_dsnmap_fset_destroy(nuuk_dsnmap_fset_t * fset, const nuuk_dsnmap_fset_config_t * config) {
  nuuk_cow_destroy(&fset->node, &config->cow_config);
}


/******************************************************************************
 * PUT FUNCTIONS
 */

nuuk_dsnmap_fset_status_t nuuk_dsnmap_fset_put(nuuk_dsnmap_fset_t * fset, const nuuk_dsnmap_fset_config_t * config,
                                               nuuk_dsnmap_fset_op_t * op, void * prev_value) {
  nuuk_dsnmap_fset_op_transform_args_t transform_args = {
      .fset = fset,
      .config = config,
      .op = op,
      .prev_value = prev_value
  };
  int transform_result = nuuk_cow_transform(&fset->node, &config->cow_config, _nuuk_dsnmap_fset_insert_transform,
                                            (void *) &transform_args);

  if (transform_result == NUUK_DSNMAP_FSET_ERR_FROZEN) {
    return transform_result;
  }

  return NUUK_DSNMAP_FSET_OK;
}


/******************************************************************************
 * GET FUNCTIONS
 */

bool nuuk_dsnmap_fset_get(nuuk_dsnmap_fset_t * fset, const nuuk_dsnmap_fset_config_t * config, const void * key,
                          uint32_t key_hash, void * value) {

  // Acquire node.
  nuuk_dsnmap_fset_node_t * node;
  nuuk_cow_acquire_node(&fset->node, &config->cow_config, (void **) &node);

  // Get appropriate index for insertion.
  bool result = false;
  uint32_t record_size = nuuk_map_record_size(config->element_type);
  for (uint32_t index = 0; index < node->size; ++index) {
    nuuk_map_record_t * record = (nuuk_map_record_t *) &node->elements[index * record_size];
    if (_nuuk_dsnmap_fset_record_matches(config, record, key, key_hash)) {
      uint32_t value_size = nuuk_map_element_value_size(config->element_type);
      char * record_value = nuuk_map_record_value(config->element_type, record);
      if (value) {
        memcpy(value, record_value, value_size);
      }
      result = true;
      break;
    }
  }

  nuuk_cow_release_node(&fset->node, &config->cow_config);
  return result;
}


/******************************************************************************
 * DELETE FUNCTIONS
 */

bool nuuk_dsnmap_fset_remove(nuuk_dsnmap_fset_t * fset, const nuuk_dsnmap_fset_config_t * config,
                             nuuk_dsnmap_fset_op_t * op, void * prev_value) {
  nuuk_dsnmap_fset_op_transform_args_t transform_args = {
      .fset = fset,
      .config = config,
      .op = op,
      .prev_value = prev_value
  };
  int transform_result = nuuk_cow_transform(&fset->node, &config->cow_config, _nuuk_dsnmap_fset_remove_transform,
                                            (void *) &transform_args);

  if (transform_result == NUUK_DSNMAP_FSET_ERR_FROZEN) {
    return transform_result;
  }

  return NUUK_DSNMAP_FSET_OK;
}
