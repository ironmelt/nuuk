/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include "nuuk/collection/concurrent/dsnmap.h"


/******************************************************************************
 * HOOKS
 */

static
void _nuuk_dsnmap_destroy_hook(nuuk_map_t * map) {
  nuuk_dsnmap_destroy((nuuk_dsnmap_t *) map);
}

static
uint32_t _nuuk_dsnmap_size_hook(nuuk_map_t * map) {
  return nuuk_dsnmap_size((nuuk_dsnmap_t *) map);
}

static
nuuk_map_write_result_t _nuuk_dsnmap_put_hook(nuuk_map_t * map, const void * key, const void * value,
                                              void * prev_value) {
  return nuuk_dsnmap_put((nuuk_dsnmap_t *) map, key, value, prev_value);
}

static
bool _nuuk_dsnmap_has_key_hook(nuuk_map_t * map, const void * key) {
  return nuuk_dsnmap_has_key((nuuk_dsnmap_t *) map, key);
}

static
bool _nuuk_dsnmap_get_hook(nuuk_map_t * map, const void * key, void * value) {
  return nuuk_dsnmap_get((nuuk_dsnmap_t *) map, key, value);
}

static
nuuk_map_write_result_t _nuuk_dsnmap_remove_hook(nuuk_map_t * map, const void * key, void * prev_value) {
  return nuuk_dsnmap_remove((nuuk_dsnmap_t *) map, key, prev_value);
}


/******************************************************************************
 * HOOKS DEFINITION
 */

/**
 * Hooks for various `nuuk_map_t` implementations.
 */
const nuuk_map_hooks_t nuuk_dsnmap_hooks = {
   .destroy = _nuuk_dsnmap_destroy_hook,
   .size = _nuuk_dsnmap_size_hook,
   .put = _nuuk_dsnmap_put_hook,
   .has_key = _nuuk_dsnmap_has_key_hook,
   .get = _nuuk_dsnmap_get_hook,
   .remove = _nuuk_dsnmap_remove_hook
};
