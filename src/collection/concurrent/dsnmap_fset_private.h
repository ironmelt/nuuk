/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include "nuuk/collection/concurrent/dsnmap_fset.h"
#include "nuuk/concurrent/cow.h"
#include "nuuk/concurrent/hazard.h"
#include "nuuk/util/alloc.h"


/******************************************************************************
 * TYPES
 */

/**
 * Fset node.
 */
typedef struct nuuk_dsnmap_fset_node_s {

  /*** `true` if the node is frozen.
   */
  bool frozen;

  /**
   * The number of elements in the set.
   */
  uint32_t size;

  /**
   * The elements array.
   */
  char elements[];

} nuuk_dsnmap_fset_node_t;

/**
 * Fset operation.
 */
typedef struct nuuk_dsnmap_fset_op_s {

  /**
   * The key.
   */
  const void * key;

  /**
   * The key hash.
   */
  uint32_t key_hash;

  /**
   * The value.
   */
  const void * value;

  /**
   * `true` if size has been modified.
   */
  bool had_mapping;

} nuuk_dsnmap_fset_op_t;

/**
 * Status codes for various `nuuk_dsnmap_fset_t` operations.
 */
typedef enum nuuk_dsnmap_fset_status_e {

  /**
   * Normal operation.
   */
  NUUK_DSNMAP_FSET_OK         = NUUK_OK,

  /**
   * Unable to allocate memory.
   */
  NUUK_DSNMAP_FSET_ERR_ALLOC  = NUUK_ERR_ALLOC,

  /**
   * The fset is frozen.
   */
  NUUK_DSNMAP_FSET_ERR_FROZEN = NUUK_ERR_CUSTOM

} nuuk_dsnmap_fset_status_t;


/******************************************************************************
 * CONFIG FUNCTIONS
 */

/**
 * Initialize a fset config.
 *
 * @param config        The config to initialize.
 * @param hazard        The hazard pointer to use.
 * @param hazard_ptr_no The static hazard pointer index to use.
 */
void nuuk_dsnmap_fset_config_init(nuuk_dsnmap_fset_config_t * config, nuuk_hazard_t * hazard, uint32_t hazard_ptr_no,
                                  const nuuk_map_element_type_t * element_type);

/**
 * Destroy a fset config and release resources used by it.
 *
 * @param  __config The fset config to destroy.
 */
#define nuuk_dsnmap_fset_config_destroy(__config)


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

/**
 * Initialize a fset with storage on heap.
 *
 * @param fset   The fset to initialize.
 * @param config The fset config.
 *
 * @return `NUUK_DSNMAP_FSET_OK`.
 */
void nuuk_dsnmap_fset_init(nuuk_dsnmap_fset_t * fset, const nuuk_dsnmap_fset_config_t * config);

/**
 * Destroy a fset and release resources used by it.
 *
 * @param map    The fset to destroy.
 * @param config The fset config.
 */
void nuuk_dsnmap_fset_destroy(nuuk_dsnmap_fset_t * fset, const nuuk_dsnmap_fset_config_t * config);


/******************************************************************************
 * PUT FUNCTIONS
 */

/**
 * Put a value in the fset.
 *
 * @param fset       The fset.
 * @param config     The fset config.
 * @param op         The fset operation.
 * @param prev_value Pointer to a memory location that will be set to the previous value, if any.
 *
 * @return
 *  - `NUUK_DSNMAP_FSET_OK`        if the operation is successful.
 *  - `NUUK_DSNMAP_FSET_ERR_ALLOC` if memory allocation failed.
 */
nuuk_dsnmap_fset_status_t nuuk_dsnmap_fset_put(nuuk_dsnmap_fset_t * fset, const nuuk_dsnmap_fset_config_t * config,
                                               nuuk_dsnmap_fset_op_t * op, void * prev_value);


/******************************************************************************
 * DELETE FUNCTIONS
 */

/**
 * Remove a value from the fset.
 *
 * @param fset       The fset.
 * @param config     The fset config.
 * @param op         The fset operation.
 * @param prev_value Pointer to a memory location that will be set to the previous value, if any.
 *
 * @return
 *  - `NUUK_DSNMAP_FSET_OK`        if the operation is successful.
 *  - `NUUK_DSNMAP_FSET_ERR_ALLOC` if memory allocation failed.
 */
bool nuuk_dsnmap_fset_remove(nuuk_dsnmap_fset_t * fset, const nuuk_dsnmap_fset_config_t * config,
                             nuuk_dsnmap_fset_op_t * op, void * prev_value);


/******************************************************************************
 * GET FUNCTIONS
 */

/**
 * Get a value from the fset.
 *
 * @param fset     The fset,
 * @param config   The fset config.
 * @param key      The key.
 * @param key_hash The key hash.
 * @param value    Pointer to a memory location that will be set to the value, if any.
 *
 * @return         `true` if the operation is successful.
 */
bool nuuk_dsnmap_fset_get(nuuk_dsnmap_fset_t * fset, const nuuk_dsnmap_fset_config_t * config, const void * key,
                          uint32_t key_hash, void * value);


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
