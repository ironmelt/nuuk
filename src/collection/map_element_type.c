/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include <string.h>

#include "nuuk/collection/map_element_type.h"
#include "nuuk/util/hash.h"


/******************************************************************************
 * INSTANCE FUNCTIONS
 */

void nuuk_map_element_type_init(
    nuuk_map_element_type_t * element_type,
    uint32_t key_size, uint32_t value_size, map_hash_fn_t hash, map_compare_fn_t compare) {
  element_type->key_size = key_size;
  element_type->value_size = value_size;
  element_type->hash = hash ? hash : XXH32;
  element_type->compare = compare ? compare : memcmp;
}
