/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include <string.h>

#include "nuuk/collection/map_record.h"


/*******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

void nuuk_map_record_init(nuuk_map_record_t * record, const nuuk_map_element_type_t * element_type, const void * key,
                          const void * value, uint32_t key_hash) {
  record->hash = key_hash;
  memcpy(nuuk_map_record_key(element_type, record), key, nuuk_map_element_key_size(element_type));
  memcpy(nuuk_map_record_value(element_type, record), value, nuuk_map_element_value_size(element_type));
}
