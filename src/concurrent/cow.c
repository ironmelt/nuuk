/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include "nuuk/concurrent/cow.h"


/******************************************************************************
 * MACROS
 */

#define CURRENT_NODE(__cow) ((void *) atomic_load(__cow))


/******************************************************************************
 * STATIC FUNCTIONS
 */

static
void _nuuk_cow_free(const nuuk_cow_config_t * config, void * node) {
  if (!config->filter || config->filter(node)) {
    nuuk_hazard_free(config->hazard, node);
  }
}


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

nuuk_cow_status_t nuuk_cow_init(nuuk_cow_t * cow, const nuuk_cow_config_t * config, void * node) {
  atomic_store(cow, (uintptr_t) node);
  return NUUK_COW_OK;
}

void nuuk_cow_destroy(nuuk_cow_t * cow, const nuuk_cow_config_t * config) {
  _nuuk_cow_free(config, CURRENT_NODE(cow));
}


/******************************************************************************
 * TRANSFORM FUNCTIONS
 */

int nuuk_cow_transform(nuuk_cow_t * cow, const nuuk_cow_config_t * config, nuuk_cow_transform_t transform,
    void * udata) {
  nuuk_cow_status_t result = NUUK_COW_OK;
  bool replaced = true;
  void * node;
  void * new_node;
  do {

    // Start by acquiring current node.
    nuuk_cow_status_t acquire_status = nuuk_cow_acquire_node(cow, config, &node);
    if (acquire_status != NUUK_COW_OK) {
      return acquire_status;
    }

    // Run the transform.
    nuuk_cow_status_t transform_err = transform(node, &new_node, udata);

    // Handle errors.
    if (transform_err != NUUK_COW_OK) {
      replaced = false;
      result = transform_err;
      break;
    }

    // Handle non-changed values.
    if (node == new_node) {
      replaced = false;
      break;
    }

    // Replace the value.
    if (atomic_compare_exchange_weak(cow, (intptr_t *) &node, (intptr_t) new_node)) {
      break;
    }

    // If failed, free the transformed node.
    _nuuk_cow_free(config, new_node);

  } while (true);

  // Remove hazard lease.
  nuuk_hazard_static_clear(config->hazard, config->hazard_ptr_no);

  // Free if replaced.
  if (replaced) {
    _nuuk_cow_free(config, node);
  }

  return result;
}


/******************************************************************************
 * ACCESSOR FUNCTIONS
 */

nuuk_cow_status_t nuuk_cow_acquire_node(nuuk_cow_t * cow, const nuuk_cow_config_t * config, void ** node) {
  void * current_node = CURRENT_NODE(cow);
  void * expected = current_node;
  do {
    if (nuuk_hazard_static_set(config->hazard, config->hazard_ptr_no, current_node) != NUUK_HAZARD_OK) {
      return NUUK_COW_ERR_HAZARD;
    }
    current_node = (void *) CURRENT_NODE(cow);
  } while (current_node != expected);
  *node = current_node;
  return NUUK_COW_OK;
}

void nuuk_cow_release_node(nuuk_cow_t * map, const nuuk_cow_config_t * config) {
  nuuk_hazard_static_clear(config->hazard, config->hazard_ptr_no);
}
