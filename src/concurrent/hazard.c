/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */


/*****************************************************************************/


#include <string.h>

#include "nuuk/collection/arraylist.h"
#include "nuuk/collection/arraylist_iterator.h"
#include "nuuk/collection/ringmap.h"
#include "nuuk/concurrent/atomic.h"
#include "nuuk/concurrent/hazard.h"
#include "nuuk/util/alloc.h"
#include "nuuk/util/hash.h"


/******************************************************************************
 * CONFIG DEFINES
 */

#define FREELIST_CAPACITY 16


/******************************************************************************
 * MACROS
 */

#define TS(hazard)                 (tss_get((hazard)->ts))
#define TS_HEAD(hazard)            ((nuuk_hazard_ts_t *) atomic_load(&(hazard)->ts_head))
#define DYN_HEAD(hazard)           ((nuuk_hazard_dyn_t *) atomic_load(&(hazard)->dyn_head))

#define AS_NUUK_HAZARD_LINKED(obj) ((nuuk_hazard_linked_t *) (obj))

#define LINKED_NEXT(__linked)      (((nuuk_hazard_linked_t *) (__linked))->next)


/******************************************************************************
 * TYPES
 */

/**
 * nuuk_hazard_linked_t is an interface for deactivable simply-linked types.
 */
typedef struct nuuk_hazard_linked_s {

  /**
   * Link to next element.
   */
  struct nuuk_hazard_linked_s * next;

  /**
   * Flag indicating whether the structure is currently active.
   */
  atomic_bool active;

} nuuk_hazard_linked_t;


/**
 * Linked list of thread-specific hazards.
 */
typedef struct nuuk_hazard_ts_s {

  /**
   * @private
   * nuuk_hazard_ts_t is a nuuk_hazard_linked_t, and can be casted to it.
   */
  nuuk_hazard_linked_t _;

  /**
   * Pointer to the parent hazard.
   */
  nuuk_hazard_t * hazard;

  /**
   * Array of memory blocks to be freed.
   */
  nuuk_arraylist_t free_list;

  /**
   * Array of static ptrs.
   */
  void * ptrs[];

} nuuk_hazard_ts_t;


/**
 * Linked list of dynamic pointers.
 */
struct nuuk_hazard_dyn_s {

  /**
   * @private
   * nuuk_hazard_dyn_t is a nuuk_hazard_linked_t, and can be casted to it.
   */
  nuuk_hazard_linked_t _;

  /**
   * Actual dynamic pointer.
   */
  void * ptr;

};


/******************************************************************************
 * STATIC VARIABLES
 */

static
nuuk_map_element_type_t _nuuk_hazard_free_map_element_type = {
  .key_size = sizeof(void *),
  .value_size = 0,
  .hash = XXH32,
  .compare = memcmp
};


/******************************************************************************
 * STATIC FUNCTIONS
 */

/**
 * Callback function to free pointers from the free list.
 */
static
bool _nuuk_hazard_ptr_free_fn(void * ptr, void * udata) {
  nuuk_free(*((void **) ptr));
  return true;
}


/**
 * Allocate a new TS struct.
 */
static
nuuk_hazard_ts_t * _nuuk_hazard_ts_alloc(nuuk_hazard_t * hazard) {
  size_t ptr_array_size = sizeof(void *) * hazard->n_ptrs;
  nuuk_hazard_ts_t * ts = nuuk_alloc(sizeof(nuuk_hazard_ts_t) + ptr_array_size);
  if (!ts) {
    return NULL;
  }
  if (nuuk_arraylist_init(&ts->free_list, sizeof(void *), FREELIST_CAPACITY, FREELIST_CAPACITY) != NUUK_ARRAYLIST_OK) {
    nuuk_free(ts);
    return NULL;
  }
  atomic_init(&AS_NUUK_HAZARD_LINKED(ts)->active, true);
  LINKED_NEXT(ts) = NULL;
  memset(&ts->ptrs[0], 0, ptr_array_size);
  atomic_fetch_add(&hazard->total_n_ptrs, hazard->n_ptrs);
  ts->hazard = hazard;
  return ts;
}

/**
 * Free a TS struct, forcing disposal of every memory block in the free list.
 */
static
void _nuuk_hazard_ts_free(nuuk_hazard_t * hazard, nuuk_hazard_ts_t * ts) {

  // Let's force free of every pointer.
  nuuk_arraylist_foreach(&ts->free_list, _nuuk_hazard_ptr_free_fn, (void *) hazard);

  // Destroy everything.
  nuuk_arraylist_destroy(&ts->free_list);
  nuuk_free(ts);
}

/**
 * Locate a TS struct suitable for the current thread, or create one if necessary.
 */
static
nuuk_hazard_ts_t * _nuuk_hazard_ts_locate(nuuk_hazard_t * hazard) {

  // First, see whether a ts struct has already been allocated for this thread.
  nuuk_hazard_ts_t * ts = TS(hazard);
  if (ts) {
    return ts;
  }

  // Second, let's see whether we can re-use an existing but inactive ts struct.
  nuuk_hazard_ts_t * ts_head = TS_HEAD(hazard);
  ts = ts_head;
  while (ts) {
    bool expected = false;
    if (atomic_compare_exchange_weak(&AS_NUUK_HAZARD_LINKED(ts)->active, &expected, true)) {
      break;
    }
    ts = (nuuk_hazard_ts_t *) LINKED_NEXT(ts);
  }

  // If we still didn't find any ts struct, let's be *crazy* and allocate a new one.
  if (!ts) {
    ts = _nuuk_hazard_ts_alloc(hazard);
    if (!ts) {
      return NULL;
    }
    LINKED_NEXT(ts) = AS_NUUK_HAZARD_LINKED(ts_head);
    while (!atomic_compare_exchange_weak(&hazard->ts_head, (intptr_t *) &ts_head, (intptr_t) ts)) {
      LINKED_NEXT(ts) = AS_NUUK_HAZARD_LINKED(ts_head);
    }
  }

  // Register the ts struct for the current thread.
  tss_set(hazard->ts, ts);

  return ts;
}

/**
 * Allocate a new DYN struct.
 */
static
nuuk_hazard_dyn_t * _nuuk_hazard_dyn_alloc(nuuk_hazard_t * hazard) {
  nuuk_hazard_dyn_t * dyn = nuuk_alloc(sizeof(nuuk_hazard_dyn_t));
  if (!dyn) {
    return NULL;
  }
  atomic_init(&AS_NUUK_HAZARD_LINKED(dyn)->active, true);
  LINKED_NEXT(dyn) = NULL;
  dyn->ptr = NULL;
  atomic_fetch_add(&hazard->total_n_ptrs, 1);
  return dyn;
}

/**
 * Locate a suitable DYN struct, or create one if necessary.
 */
static
nuuk_hazard_dyn_t * _nuuk_hazard_dyn_locate(nuuk_hazard_t * hazard) {

  // First, let's see whether we can re-use an existing but inactive dyn struct.
  nuuk_hazard_dyn_t * dyn_head = DYN_HEAD(hazard);
  nuuk_hazard_dyn_t * dyn = dyn_head;
  while (dyn) {
    bool expected = false;
    if (atomic_compare_exchange_weak(&AS_NUUK_HAZARD_LINKED(dyn)->active, &expected, true)) {
      break;
    }
    dyn = (nuuk_hazard_dyn_t *) LINKED_NEXT(dyn);
  }

  // If we still didn't find any dyn struct, let's be *crazy* and allocate a new one.
  if (!dyn) {
    dyn = _nuuk_hazard_dyn_alloc(hazard);
    if (!dyn) {
      return NULL;
    }
    LINKED_NEXT(dyn) = AS_NUUK_HAZARD_LINKED(dyn_head);
    while (!atomic_compare_exchange_weak(&hazard->dyn_head, (intptr_t *) &dyn_head, (intptr_t) dyn)) {
      LINKED_NEXT(dyn) = AS_NUUK_HAZARD_LINKED(dyn_head);
    }
  }

  return dyn;
}

/**
 * TS struct release callback, called on thread termination.
 */
static
void _nuuk_hazard_ts_release_cb(nuuk_hazard_ts_t * ts) {
  if (!ts) {
    return;
  }
  memset(&ts->ptrs[0], 0, sizeof(void *) * ts->hazard->n_ptrs);
  atomic_store(&AS_NUUK_HAZARD_LINKED(ts)->active, 0);
}

/**
 * Collect protected hazards.
 */
static
bool _nuuk_hazard_collect(nuuk_hazard_t * hazard, nuuk_ringmap_t * map) {

  // Get static pointers.
  nuuk_hazard_ts_t * ts = TS_HEAD(hazard);
  nuuk_hazard_ts_t * next_ts = NULL;
  while (ts) {
    next_ts = (nuuk_hazard_ts_t *) LINKED_NEXT(ts);
    for (uint32_t i = 0; i < hazard->n_ptrs; ++i) {
      if (ts->ptrs[i]) {
        if (nuuk_ringmap_put(map, &ts->ptrs[i], NULL, NULL).status != NUUK_MAP_OK) {
          return false;
        }
      }
    }
    ts = next_ts;
  }

  // Get dynamic pointers.
  nuuk_hazard_dyn_t * dyn = DYN_HEAD(hazard);
  nuuk_hazard_dyn_t * next_dyn = NULL;
  while (dyn) {
    next_dyn = (nuuk_hazard_dyn_t *) LINKED_NEXT(dyn);
    if (dyn->ptr && nuuk_ringmap_put(map, &dyn->ptr, NULL, NULL).status != NUUK_MAP_OK) {
      return false;
    }
    dyn = next_dyn;
  }

  return true;
}

/**
 * Try to safely clean pointers for `ts`.
 */
static
void _nuuk_hazard_clean(nuuk_hazard_t * hazard, nuuk_hazard_ts_t * ts) {

  // Collect protected pointers.
  nuuk_ringmap_t map;
  while (true) {
    nuuk_ringmap_inita(&map, &_nuuk_hazard_free_map_element_type, 2 * atomic_load(&hazard->total_n_ptrs));
    if (_nuuk_hazard_collect(hazard, &map)) {
      break;
    }
    nuuk_ringmap_destroy(&map);
  }

  // Clean our pointers.
  uint32_t new_free_list_cur_index = 0;
  void * new_free_list[nuuk_arraylist_size(&ts->free_list)];
  nuuk_arraylist_iterator_t iter;
  nuuk_arraylist_iterator_init(&iter, &ts->free_list);
  while (nuuk_arraylist_iterator_has_next(&iter)) {

    // If protected, keep it in the free list, or free it otherwise.
    void * ptr = *((void **) nuuk_arraylist_iterator_next(&iter));
    if (nuuk_ringmap_has_key(&map, &ptr)) {
      new_free_list[new_free_list_cur_index++] = ptr;
    } else {
      nuuk_free(ptr);
    }
  }

  // Replace the list elements.
  nuuk_arraylist_put_range(&ts->free_list, 0, new_free_list_cur_index, &new_free_list);
  nuuk_arraylist_trim(&ts->free_list, new_free_list_cur_index);

  // Destroy our map.
  nuuk_ringmap_destroy(&map);
}


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

nuuk_hazard_status_t nuuk_hazard_init(nuuk_hazard_t * hazard, uint32_t n_ptrs, bool lazy) {
  hazard->n_ptrs = n_ptrs;
  int tss_err = tss_create(&hazard->ts, (tss_dtor_t) _nuuk_hazard_ts_release_cb);
  atomic_init(&hazard->total_n_ptrs, 0);
  if (tss_err == thrd_error) {
    return NUUK_HAZARD_ERR_TSS;
  }
  if (lazy) {
    atomic_init(&hazard->ts_head, 0);
  } else {
    nuuk_hazard_ts_t * ts = _nuuk_hazard_ts_alloc(hazard);
    if (!ts) {
      tss_delete(hazard->ts);
      return NUUK_HAZARD_ERR_ALLOC;
    }
    atomic_init(&hazard->ts_head, (intptr_t) ts);
    tss_set(hazard->ts, ts);
  }
  atomic_init(&hazard->dyn_head, 0);

  return NUUK_HAZARD_OK;
}

void nuuk_hazard_destroy(nuuk_hazard_t * hazard) {

  // By convention, return if `hazard` is `NULL`.
  if (!hazard) {
    return;
  }

  // Destroy ts structs.
  nuuk_hazard_ts_t * ts = TS_HEAD(hazard);
  nuuk_hazard_ts_t * next_ts = NULL;
  while (ts) {
    next_ts = (nuuk_hazard_ts_t *) LINKED_NEXT(ts);
    _nuuk_hazard_ts_free(hazard, ts);
    ts = next_ts;
  }

  // Destroy dyn structs.
  nuuk_hazard_dyn_t * dyn = DYN_HEAD(hazard);
  nuuk_hazard_dyn_t * next_dyn = NULL;
  while (dyn) {
    next_dyn = (nuuk_hazard_dyn_t *) LINKED_NEXT(dyn);
    nuuk_free(dyn);
    dyn = next_dyn;
  }

  tss_delete(hazard->ts);
}


/******************************************************************************
 * DISPOSE FUNCTIONS
 */

nuuk_hazard_status_t nuuk_hazard_free(nuuk_hazard_t * hazard, void * ptr) {

  // Register for cleaning.
  if (!ptr) {
    return NUUK_HAZARD_OK;
  }
  nuuk_hazard_ts_t * ts = _nuuk_hazard_ts_locate(hazard);
  nuuk_arraylist_append(&ts->free_list, &ptr);

  // Maybe we should clean a little...
  if (nuuk_arraylist_size(&ts->free_list) >= FREELIST_CAPACITY) {
    _nuuk_hazard_clean(hazard, ts);
  }

  return NUUK_HAZARD_OK;
}


/******************************************************************************
 * STATIC HAZARD FUNCTIONS
 */

nuuk_hazard_status_t nuuk_hazard_static_set(nuuk_hazard_t * hazard, uint32_t static_ptr_no, void * ptr) {
  nuuk_hazard_ts_t * ts = _nuuk_hazard_ts_locate(hazard);
  if (!ts) {
    return NUUK_HAZARD_ERR_ALLOC;
  }
  ts->ptrs[static_ptr_no] = ptr;
  return NUUK_HAZARD_OK;
}

void nuuk_hazard_static_clear(nuuk_hazard_t * hazard, uint32_t static_ptr_no) {
  nuuk_hazard_ts_t * ts = TS(hazard);
  if (ts) {
    ts->ptrs[static_ptr_no] = NULL;
  }
}


/******************************************************************************
 * DYNAMIC HAZARD FUNCTIONS
 */

nuuk_hazard_status_t nuuk_hazard_dynamic_reserve(nuuk_hazard_t * hazard, nuuk_hazard_dyn_handle_t * dyn_handle) {
  nuuk_hazard_dyn_t * dyn = _nuuk_hazard_dyn_locate(hazard);
  if (!dyn) {
    return NUUK_HAZARD_ERR_ALLOC;
  }
  dyn_handle->dyn = dyn;
  return NUUK_HAZARD_OK;
}

void nuuk_hazard_dynamic_release(nuuk_hazard_dyn_handle_t * dyn_handle) {
  dyn_handle->dyn->ptr = NULL;
  atomic_store(&AS_NUUK_HAZARD_LINKED(dyn_handle->dyn)->active, 0);
}

void nuuk_hazard_dynamic_set(nuuk_hazard_dyn_handle_t * dyn_handle, void * ptr) {
  dyn_handle->dyn->ptr = ptr;
}

nuuk_hazard_status_t nuuk_hazard_dynamic_reserve_set(nuuk_hazard_t * hazard, void * ptr,
    nuuk_hazard_dyn_handle_t * dyn_handle) {
  nuuk_hazard_status_t hazard_err = nuuk_hazard_dynamic_reserve(hazard, dyn_handle);
  if (hazard_err != NUUK_HAZARD_OK) {
    return hazard_err;
  }
  nuuk_hazard_dynamic_set(dyn_handle, ptr);
  return NUUK_HAZARD_OK;
}

void nuuk_hazard_dynamic_clear(nuuk_hazard_dyn_handle_t * dyn_handle) {
  dyn_handle->dyn->ptr = NULL;
}
