/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief List interface.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include "nuuk/collection/arraylist.h"
#include "nuuk/collection/iterator.h"


/******************************************************************************
 * EXTERN CONSTANTS
 */

extern const nuuk_iterator_hooks_t nuuk_arraylist_iterator_hooks;


/******************************************************************************
 * TYPES
 */

/**
 * An arraylist implementation.
 */
typedef struct nuuk_arraylist_iterator_s {

  /**
   * @Private
   * `nuuk_arraylist_iterator_t` is a `nuuk_iterator_t`, and can be casted to it.
   */
  nuuk_iterator_t _;

  /**
   * Current index.
   */
  nuuk_arraylist_t * list;

  /**
   * Current index.
   */
  uint32_t index;

} nuuk_arraylist_iterator_t;


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

/**
 * Initialize an arraylist iterator.
 *
 * @param iter The `nuuk_arraylist_iterator_t` to initialize.
 * @param list The list to iterate on.
 */
void nuuk_arraylist_iterator_init(nuuk_arraylist_iterator_t * iter, nuuk_arraylist_t * list);

/**
 * Destroy an iterator, and release resources used by it.
 *
 * @param list The iterator to destroy.
 */
void nuuk_arraylist_iterator_destroy(nuuk_arraylist_iterator_t * iter);


/******************************************************************************
 * ITERATOR FUNCTIONS
 */

/**
 * Tests whether there is another element in the iterator.
 *
 * @return `true` if there is another element in the iterator, `false` otherwise.
 */
static inline
bool nuuk_arraylist_iterator_has_next(nuuk_arraylist_iterator_t * iter) {
  return iter->index < nuuk_arraylist_size(iter->list);
}

/**
 * Read the next value.
 *
 * @return the next value in the iterator.
 */
void * nuuk_arraylist_iterator_next(nuuk_arraylist_iterator_t * iter);


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
