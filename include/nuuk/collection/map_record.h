/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief Common map functions.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include "nuuk/collection/map_element_type.h"
#include "nuuk/util/types.h"


/******************************************************************************
 * TYPES
 */

/**
 * A basic map record.
 */
typedef struct nuuk_map_record_s {

  /**
   * The hashed key. 
   */
  uint32_t hash;

  /**
   * Packed key + value.
   */
  char element[];

} nuuk_map_record_t;


/*******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

void nuuk_map_record_init(nuuk_map_record_t * record, const nuuk_map_element_type_t * element_type, const void * key,
                          const void * value, uint32_t key_hash);


/******************************************************************************
 * INFO FUNCTIONS
 */

/**
 * Get the size of a `nuuk_map_record_s` for a specific element type.
 *
 * @param element_type The element type to get record size for.
 *
 * @return The record size for `element_type` in bytes.
 */
static inline
uint32_t nuuk_map_record_size(const nuuk_map_element_type_t * element_type) {
  return sizeof(nuuk_map_record_t) + nuuk_map_element_size(element_type);
}


/******************************************************************************
 * ACCESSOR FUNCTIONS
 */

/**
 * Get the saved hash of a record.
 *
 * @param record The record to get hash for.
 *
 * @return the saved hash of the record key.
 */
static inline
uint32_t nuuk_map_record_hash(const nuuk_map_record_t * record) {
  return record->hash;
}

/**
 * Get the key of a record.
 *
 * @param element_type The element type.
 * @param record       The record to get key for.
 *
 * @return a pointer to the key for `record`.
 */
static inline
char * nuuk_map_record_key(const nuuk_map_element_type_t * element_type, nuuk_map_record_t * record) {
  return &record->element[0];
}

/**
 * Get the value of a record.
 *
 * @param element_type The element type.
 * @param record       The record to get value for.
 *
 * @return a pointer to the value for `record`.
 */
static inline
char * nuuk_map_record_value(const nuuk_map_element_type_t * element_type, nuuk_map_record_t * record) {
  return &record->element[nuuk_map_element_key_size(element_type)];
}


/******************************************************************************
 * SETTER FUNCTIONS
 */

/**
 * Set the hash of a record.
 *
 * @param record The record to set hash for.
 * @param hash   The hash.
 */
static inline
void nuuk_map_record_hash_set(nuuk_map_record_t * record, uint32_t hash) {
  record->hash = hash;
}


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
