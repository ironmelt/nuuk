/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief Common map functions.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include "nuuk/util/types.h"


/******************************************************************************
 * TYPES
 */

/**
 * Hash function for map types.
 *
 * @param ptr  Pointer to the buffer to hash.
 * @param nun  Number of bytes to hash.
 * @param seed Hash seed.
 *
 * @return the hash value of data at `ptr`.
 */
typedef uint32_t (* map_hash_fn_t) (const void* ptr, size_t num, uint32_t seed);

/**
 * Compare function for map types.
 *
 * @param ptr1 Pointer to the first buffer.
 * @param ptr2 Pointer to the second buffer.
 * @param num  Number of bytes to compare.
 *
 * @return 0 if `ptr1 == ptr2`, strictly negative value if `ptr1 < ptr2`, or strictly positive value if `ptr1 > ptr2`.
 */
typedef int32_t (* map_compare_fn_t) (const void * ptr1, const void * ptr2, size_t num);

/**
 * A map element type.
 */
typedef struct nuuk_map_element_type_s {

  /**
   * Key hash function.
   */
  map_hash_fn_t hash;

  /**
   * Key comparison function.
   */
  map_compare_fn_t compare;

  /**
   * The key size in bytes.
   */
  uint32_t key_size;

  /**
   * The value size in bytes.
   */
  uint32_t value_size;

} nuuk_map_element_type_t;


/******************************************************************************
 * INSTANCE FUNCTIONS
 */

/**
 * Initialize a map element type.
 *
 * @param key_size   Key size in bytes.
 * @param value_size Value size in bytes.
 * @param hash       Hash function to use. If `NULL`, a reasonable default will be used.
 * @param compare    Compare function to use. If `NULL`, a reasonable default will be used.
 */
void nuuk_map_element_type_init(
    nuuk_map_element_type_t * et, uint32_t key_size, uint32_t value_size, map_hash_fn_t hash, map_compare_fn_t compare);


/******************************************************************************
 * INFO FUNCTIONS
 */

/**
 * Get the key size for a specific element type.
 *
 * @param element_type The element type to get key size for.
 *
 * @return The key size for `element_type` in bytes.
 */
static inline
uint32_t nuuk_map_element_key_size(const nuuk_map_element_type_t * element_type) {
  return element_type->key_size;
}

/**
 * Get the value size for a specific element type.
 *
 * @param element_type The element type to get value size for.
 *
 * @return The value size for `element_type` in bytes.
 */
static inline
uint32_t nuuk_map_element_value_size(const nuuk_map_element_type_t * element_type) {
  return element_type->value_size;
}

/**
 * Get the size of an element buffer for a specific element type.
 *
 * @param element_type The element type to get buffer size for.
 *
 * @return The buffer size for `element_type` in bytes.
 */
static inline
uint32_t nuuk_map_element_size(const nuuk_map_element_type_t * element_type) {
  return nuuk_map_element_key_size(element_type) + nuuk_map_element_value_size(element_type);
}


/******************************************************************************
 * UTILITY FUNCTIONS
 */

/**
 * Get the size of an element buffer for a specific element type.
 *
 * @param element_type The element type.
 *
 * @return The buffer size for `element_type` in bytes.
 */
static inline
uint32_t nuuk_map_element_key_hash(const nuuk_map_element_type_t * element_type, const void * key) {
  return element_type->hash(key, element_type->key_size, 0);
}

/**
 * Compare two key buffers for a specific element type.
 *
 * @param element_type The element type.
 * @param key1         Pointer to the first key buffer.
 * @param key2         Pointer to the second key buffer.
 *
 * @return 0 if `key1 == key2`, strictly negative value if `key1 < key2`, or strictly positive value if `key1 > key2`.
 */
static inline
int32_t nuuk_map_element_key_compare(
    const nuuk_map_element_type_t * element_type, const void * key1, const void * key2) {
  return element_type->compare(key1, key2, element_type->key_size);
}


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
