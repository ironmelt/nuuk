/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief Common map functions.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include "nuuk/collection/map_record.h"
#include "nuuk/util/hooks.h"
#include "nuuk/util/status.h"
#include "nuuk/util/types.h"


/******************************************************************************
 * TYPES
 */

/*
 * Forward declaration of `nuuk_map_hooks_t`.
 */
typedef struct nuuk_map_hooks_s nuuk_map_hooks_t;

/**
 * Interface for a basic map.
 */
typedef struct nuuk_map_s {

  /**
   * Hooks for various `nuuk_map_t` implementations.
   */
  const nuuk_map_hooks_t * hooks;

  /**
   * Element type.
   */
  nuuk_map_element_type_t element_type;

} nuuk_map_t;

/**
 * Map status codes.
 */
typedef enum nuuk_map_status_e {

  /**
   * Normal operation.
   */
  NUUK_MAP_OK        = NUUK_OK,

  /**
   * Unable to allocate memory.
   */
  NUUK_MAP_ERR_ALLOC = NUUK_ERR_ALLOC,

  /**
   * Maximum capacity has been reached.
   */
  NUUK_MAP_ERR_MAX   = NUUK_ERR_MAX,

  /**
   * Hook error.
   */
  NUUK_MAP_ERR_HOOK  = NUUK_ERR_HOOK,

  /**
   * Unknown error.
   */
  NUUK_MAP_ERR_UNKNOWN = NUUK_ERR_UNKNOWN

} nuuk_map_status_t;

/**
 * Response for write operations.
 */
typedef struct nuuk_map_write_result_s {

  /**
   * Status code
   */
  nuuk_map_status_t status;

  /**
   * if the operation if successful, true` if a previous mapping existed. Undefined otherwise.
   */
  bool had_mapping;

} nuuk_map_write_result_t;

/**
 * Hooks used by `nuuk_map_t` implementations.
 */
struct nuuk_map_hooks_s {

  /*---------------------------------------------------------
   * Instance hooks.
   */

  /**
   * Destroy a shashmap and release resources used by it.
   *
   * @param map The `nuuk_shashmap_t` to destroy.
   */
  void (* destroy)(nuuk_map_t * map);

  /*---------------------------------------------------------
   * Info hooks.
   */

  /**
   * Get the number of elements currently in the map.
   *
   * @param map The map.
   *
   * @return The number of elements currently in `map`.
   */
  uint32_t (* size)(nuuk_map_t * map);

  /*---------------------------------------------------------
   * Put hooks.
   */

  /**
   * Puts a key / value to the map.
   *
   * If `prev_value` is not `NULL`, the previous value of the key will be copied to `prev_value` if the key was already
   * present in the map. Otherwise, it will be set to `NULL`.
   *
   * @param map        The map.
   * @param key        Pointer to the key.
   * @param value      Pointer to the value.
   * @param prev_value Memory location to set the previous value to. Can be `NULL`.
   *
   * @return The write result. The status can be one of:
   *   - `NUUK_MAP_OK`        if operation was successful.
   *   - `NUUK_MAP_ERR_ALLOC` if memory allocation failed.
   *   - `NUUK_MAP_ERR_MAX`   if the maximum capacity has been reached.
   */
  nuuk_map_write_result_t (* put)(nuuk_map_t * map, const void * key, const void * value, void * prev_value);

  /*---------------------------------------------------------
   * Get hooks.
   */

  /**
   * Check whether the map currently contains a mapping for a key.
   *
   * @param map The map.
   * @param key The key.
   *
   * @return `true` if `map` has a mapping for `key`.
   */
  bool (* has_key)(nuuk_map_t * map, const void * key);

  /**
   * Get the value currently associated with a key.
   *
   * @param map   The map.
   * @param key   The key.
   * @param value Memory location to which to copy the value, if any. If not found, it will be left untouched.
   *
   * @return `true` if `map` has a mapping for `key`.
   */
  bool (* get)(nuuk_map_t * map, const void * key, void * value);

  /*---------------------------------------------------------
   * Remove hooks.
   */

  /**
   * Delete the mapping for a key.
   *
   * @param map        The map.
   * @param key        The key to delete mapping for.
   * @param prev_value Memory location to set the previous value to. Can be `NULL`.
   *
   * @return The write result.
   */
  nuuk_map_write_result_t (* remove)(nuuk_map_t * map, const void * key, void * prev_value);

};


/******************************************************************************
 * EXTERN CONSTANTS
 */

extern const nuuk_map_write_result_t g_NUUK_MAP_WRITE_RESULT_HOOK_ERR;


/******************************************************************************
 * MACROS
 */

#define NUUK_MAP_WRITE_RESULT(__status, __had_mapping) \
    ((nuuk_map_write_result_t) { \
        .status = (__status), \
        .had_mapping = (__had_mapping) \
    })


/******************************************************************************
 * INTERFACE FUNCTIONS
 */

/**
 * Initialize a `nuuk_list_t`. This should be called only by subtypes.
 *
 * @param list         The list to initialize.
 * @param hooks        The hooks to use for this list.
 *
 * @return `NUUK_LIST_OK`.
 */
void nuuk_map_interface_init(
    nuuk_map_t * map, const nuuk_map_hooks_t * hooks, const nuuk_map_element_type_t * element_type);

/**
 * Destroys a `nuuk_list_t`, and release resources used by it.
 *
 * @param list The list to destroy.
 */
#define nuuk_map_interface_destroy(list)


/******************************************************************************
 * ACCESSOR FUNCTIONS
 */

static inline
nuuk_map_element_type_t * nuuk_map_element_type(nuuk_map_t * map) {
  return &map->element_type;
}


/******************************************************************************
 * INSTANCE FUNCTIONS
 */

/**
 * Destroy a map and release resources used by it.
 *
 * @param map The map to destroy.
 */
static inline
void nuuk_map_destroy(nuuk_map_t * map) {
  nuuk_hook(destroy, 0, map);
}


/******************************************************************************
 * INFO FUNCTIONS
 */

/**
 * Get the number of elements currently in the map.
 *
 * @param map The map.
 *
 * @return The number of elements currently in `map`.
 */
static inline
uint32_t nuuk_map_size(nuuk_map_t * map) {
  return nuuk_hook(size, 0, map);
}


/******************************************************************************
 * PUT FUNCTIONS
 */

/**
 * Puts a key / value to the map.
 *
 * If `prev_value` is not `NULL`, the previous value of the key will be copied to `prev_value` if the key was already
 * present in the map. Otherwise, it will be set to `NULL`.
 *
 * @param map        The map.
 * @param key        Pointer to the key.
 * @param value      Pointer to the value.
 * @param prev_value Memory location to set the previous value to. Can be `NULL`.
 *
 * @return The write result. The status can be one of:
 *   - `NUUK_MAP_OK`        if operation was successful.
 *   - `NUUK_MAP_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_MAP_ERR_MAX`   if the maximum capacity has been reached.
 *   - `NUUK_MAP_ERR_HOOK`  if the hook can't be called.
 */
static inline
nuuk_map_write_result_t nuuk_map_put(nuuk_map_t * map, const void * key, const void * value, void * prev_value) {
  return nuuk_hook(put, g_NUUK_MAP_WRITE_RESULT_HOOK_ERR, map, key, value, prev_value);
}


/******************************************************************************
 * GET FUNCTIONS
 */

/**
 * Check whether the map currently contains a mapping for a key.
 *
 * @param map The map.
 * @param key The key.
 *
 * @return `true` if `map` has a mapping for `key`.
 */
static inline
bool nuuk_map_has_key(nuuk_map_t * map, const void * key) {
  return nuuk_hook(has_key, false, map, key);
}

/**
 * Get the value currently associated with a key.
 *
 * @param map   The map.
 * @param key   The key.
 * @param value Memory location to which to copy the value, if any. If not found, it will be left untouched.
 *
 * @return `true` if `map` has a mapping for `key`.
 */
static inline
bool nuuk_map_get(nuuk_map_t * map, const void * key, void * value) {
  return nuuk_hook(get, false, map, key, value);
}


/******************************************************************************
 * DELETE FUNCTIONS
 */

/**
 * Delete the mapping for a key.
 *
 * @param map        The map.
 * @param key        The key to delete mapping for.
 * @param prev_value Memory location to set the previous value to. Can be `NULL`.
 *
 * @return The write result.
 */
static inline
nuuk_map_write_result_t nuuk_map_remove(nuuk_map_t * map, const void * key, void * prev_value) {
  return nuuk_hook(remove, g_NUUK_MAP_WRITE_RESULT_HOOK_ERR, map, key, prev_value);
}


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
