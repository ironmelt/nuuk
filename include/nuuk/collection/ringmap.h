/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief Simple hashmap implementation.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include <string.h>

#include "nuuk/collection/map.h"
#include "nuuk/util/alloc.h"


/******************************************************************************
 * EXTERN CONSTANTS
 */

extern const nuuk_map_hooks_t nuuk_ringmap_hooks;


/******************************************************************************
 * TYPES
 */

/**
 * Simple hashmap implementation, that can either be allocated on the stack or on the heap.In the case of a
 * stack-allocated map, the capacity of the map is fixed, and any element insertion beyond the initially allocated
 * capacity will result in an error.
 *
 * This implementation is not thread-safe.
 */
typedef struct nuuk_ringmap_s {

  /**
   * @private
   * `nuuk_ringmap_t` is a `nuuk_map_t`, and can be casted to it.
   */
  nuuk_map_t _;

  /**
   * Element array.
   */
  char * elements;

  /**
   * Number of elements in the map.
   */
  uint32_t size;

  /**
   * Load factor used to trigger map resize.
   * By convention, if it equals zero, the map has a fixed size.
   */
  float load_factor;

  /**
   * The current capacity of the map. It MUST be a power of two.
   */
  uint32_t capacity;

  /**
   * The minimum capacity of the map. It MUST be a power of two.
   */
  uint32_t min_capacity;

  /**
   * If `true`, the element array will be freed when the ringmap is destroyed.
   */
  bool free_elements;

} nuuk_ringmap_t;

/**
 * Ringmap record.
 */
typedef struct nuuk_ringmap_record_s {

  /**
   * Holds the current state of the element, which may be:
   *   - `0` if the element is undefined.
   *   - `1` if the element has been marked as deleted.
   *   - `2` if the element is currently active.
   */
  unsigned char state;

  /**
   * The actual map record.
   */
  nuuk_map_record_t map_record;

} nuuk_ringmap_record_t;


/******************************************************************************
 * MACROS
 */

/**
 * @private
 */
#define NUUK_RINGMAP_RECORD_SIZE(element_type) (sizeof(nuuk_ringmap_record_t) + nuuk_map_element_size(element_type))


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

/**
 * Initialize a ringmap with storage on heap, and allocates the element storage.
 *
 * @param map          The `nuuk_ringmap_t` to initialize.
 * @param element_type The element type.
 * @param capacity     The capacity to allocate. If `0`, the storage will be allocated lazily.
 * @param load_factor  The load factor of the map. If `0`, the map will have a fixed maximum size of `capacity`.
 *
 * @return
 *   - `NUUK_MAP_OK`    if initialization was successful.
 *   - `NUUK_RINGMAP_ALLOC` if memory allocation failed.
 */
nuuk_map_status_t nuuk_ringmap_init(
    nuuk_ringmap_t * map, const nuuk_map_element_type_t * element_type, uint32_t capacity, float load_factor);

/**
 * Initialize a fixed-size ringmap with storage on stack, and allocates the element storage.
 *
 * @param map          The `nuuk_ringmap_t` to initialize.
 * @param element_type The element type.
 * @param capacity     The capacity to allocate.
 */
#define nuuk_ringmap_inita(__map, __element_type, __capacity) \
    nuuk_ringmap_init(__map, __element_type, 0, 0); \
    (__map)->free_elements = false; \
    (__map)->capacity = __capacity; \
    (__map)->elements = nuuk_alloca((__capacity) * NUUK_RINGMAP_RECORD_SIZE(__element_type)); \
    memset((__map)->elements, 0, (__capacity) * NUUK_RINGMAP_RECORD_SIZE(__element_type)); \

/**
 * Destroy a ringmap and release resources used by it.
 *
 * @param map The `nuuk_ringmap_t` to destroy.
 */
void nuuk_ringmap_destroy(nuuk_ringmap_t * map);


/******************************************************************************
 * INSTANCE FUNCTIONS
 */

/**
 * Ensure that the map has enough allocated capacity to hold at least `capacity` elements.
 *
 * @param map      The map.
 * @param capacity The desired minimum element capacity.
 *
 * @return
 *   - `NUUK_MAP_OK`        if operation was successful.
 *   - `NUUK_MAP_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_MAP_ERR_MAX`   if `capacity` is higher than the currently allocated capacity, and \
 *                              `nuuk_ringmap_t.load_factor` is `0`
 */
nuuk_map_status_t nuuk_ringmap_ensure_capacity(nuuk_ringmap_t * map, uint32_t capacity);


/******************************************************************************
 * INFO FUNCTIONS
 */

/**
 * Get the number of elements currently in the map.
 *
 * @param map The map.
 *
 * @return The number of elements currently in `map`.
 */
static inline
uint32_t nuuk_ringmap_size(nuuk_ringmap_t * map) {
  return map->size;
}

/**
 * Get the current allocated element capacity of the map.
 *
 * @param map The map.
 *
 * @return The current capacity of `map`.
 */
static inline
uint32_t nuuk_ringmap_capacity(nuuk_ringmap_t * map) {
  return map->capacity;
}


/******************************************************************************
 * PUT FUNCTIONS
 */

/**
 * Puts a key / value to the map.
 *
 * If `prev_value` is not `NULL`, the previous value of the key will be copied to `prev_value` if the key was already
 * present in the map. Otherwise, it will be set to `NULL`.
 *
 * @param map        The map.
 * @param key        Pointer to the key.
 * @param value      Pointer to the value.
 * @param prev_value Memory location to set the previous value to. Can be `NULL`.
 *
 * @return The write result. The status can be one of:
 *   - `NUUK_MAP_OK`        if operation was successful.
 *   - `NUUK_MAP_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_MAP_ERR_MAX`   if `capacity` is higher than the currently allocated capacity, and \
 *                              `nuuk_ringmap_t.load_factor` is `0`
 */
nuuk_map_write_result_t nuuk_ringmap_put(nuuk_ringmap_t * map, const void * key, const void * value, void * prev_value);


/******************************************************************************
 * GET FUNCTIONS
 */

/**
 * Check whether the map currently contains a mapping for a key.
 *
 * @param map The map.
 * @param key The key.
 *
 * @return `true` if `map` has a mapping for `key`.
 */
bool nuuk_ringmap_has_key(nuuk_ringmap_t * map, const void * key);

/**
 * Get the value currently associated with a key.
 *
 * @param map   The map.
 * @param key   The key.
 * @param value Memory location to which to copy the value, if any. If not found, it will be left untouched.
 *
 * @return `true` if `map` has a mapping for `key`.
 */
bool nuuk_ringmap_get(nuuk_ringmap_t * map, const void * key, void * value);


/******************************************************************************
 * DELETE FUNCTIONS
 */

/**
 * Delete the mapping for a key.
 *
 * @param map        The map.
 * @param key        The key to delete mapping for.
 * @param prev_value Memory location to set the previous value to. Can be `NULL`.
 *
 * @return The write result.
 */
nuuk_map_write_result_t nuuk_ringmap_remove(nuuk_ringmap_t * map, const void * key, void * prev_value);


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
