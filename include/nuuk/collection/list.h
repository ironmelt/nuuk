/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief List interface.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include "nuuk/util/hooks.h"
#include "nuuk/util/status.h"
#include "nuuk/util/types.h"


/******************************************************************************
 * TYPES
 */

/**
 * Callback function for `foreach` loops.
 *
 * @param element Pointer to the current element.
 * @param udata   User-provided data.
 *
 * @return `false` to stop iterating, `true` to continue.
 */
typedef bool (* nuuk_list_foreach_fn_t)(void * element, void * udata);

/* Forward declaration of `nuuk_list_hooks_t`. */
typedef struct nuuk_list_hooks_s nuuk_list_hooks_t;

/**
 * The `nuuk_list_t` interface.
 * All nuuk list implementations inherits from this structure.
 */
typedef struct nuuk_list_t {

  /**
   * Hooks used by `as_list_t` functions.
   */
  const nuuk_list_hooks_t * hooks;

} nuuk_list_t;

/**
 * Status codes for `nuuk_list_t` operations.
 */
typedef enum nuuk_list_status_e {

  /**
   * Successful operation.
   */
  NUUK_LIST_OK        = NUUK_OK,

  /**
   * Memory allocation failure.
   */
  NUUK_LIST_ERR_ALLOC = NUUK_ERR_ALLOC,

  /**
   * Maximum capacity reached.
   */
  NUUK_LIST_ERR_MAX   = NUUK_ERR_MAX,

  /**
   * Index out of range.
   */
  NUUK_LIST_ERR_INDEX = NUUK_ERR_INDEX,

  /**
   * Unsupported operation.
   */
  NUUK_LIST_ERR_HOOK  = NUUK_ERR_HOOK

} nuuk_list_status_t;

/**
 * Hooks used by `nuuk_list_t` implementations.
 */
struct nuuk_list_hooks_s {

  /**
   * Destroy a list, and release resources used by it.
   *
   * @param list The list to destroy.
   */
  void (* destroy)(nuuk_list_t * list);

  /**
   * Get the size of the list.
   *
   * @return The number of elements currently in the list.
   */
  uint32_t (* size)(nuuk_list_t * list);

  /**
   * Get a range of elements from the list, and copy them to `dest`.
   *
   * The range will start a `start_index` (inclusive) and end at `end_index` (exclusive).
   *
   * @param list        The list.
   * @param start_index The start index (inclusive).
   * @param end_index   The end index (exclusive).
   * @param dest        The destination buffer, which must have a size of at least
   *                    `element_size * (end_index - start_index)`.
   *
   * @return
   *   - `NUUK_LIST_OK`        if the elements were copied successfully.
   *   - `NUUK_LIST_ERR_INDEX` if the requested index range is invalid.
   */
  nuuk_list_status_t (* get_range)(nuuk_list_t * list, uint32_t start_index, uint32_t end_index, void * dest);

  /**
   * Get a single element from the list, and copy it to `dest`.
   *
   * @param list  The list.
   * @param index The element index.
   * @param dest  The destination buffer, which must have a size of at least `element_size`.
   *
   * @return
   *   - `NUUK_LIST_OK`        if the element was copied successfully.
   *   - `NUUK_LIST_ERR_INDEX` if the requested index is invalid.
   */
  nuuk_list_status_t (* get)(nuuk_list_t * list, uint32_t index, void * dest);

  /**
   * Put `n_elements` elements to the list, starting at `index`, replacing elements on these positions, if any.
   *
   * @param list       The list.
   * @param index      The index at which to put elements.
   * @param n_elements The number of elements to copy.
   * @param src        The source buffer, which must have a size of at least `element_size * n_elements`.
   *
   * @return
   *   - `NUUK_LIST_OK`        if the operation is successful.
   *   - `NUUK_LIST_ERR_ALLOC` if memory allocation failed.
   *   - `NUUK_LIST_ERR_MAX`   if the list has a fixed capacity, and `index + n_elements` is greater than the fixed
   *                           capacity.
   */
  nuuk_list_status_t (* put_range)(nuuk_list_t * list, uint32_t index, uint32_t n_elements, void * src);

  /**
   * Put a single element to the list at `index`, replacing the element at `index`, if any.
   *
   * @param list       The list.
   * @param index      The index at which to put the element.
   * @param src        The source buffer, which must have a size of at least `element_size`.
   *
   * @return
   *   - `NUUK_LIST_OK`        if the operation is successful.
   *   - `NUUK_LIST_ERR_ALLOC` if memory allocation failed.
   *   - `NUUK_LIST_ERR_MAX`   if the list has a fixed capacity, and `index` is greater than the fixed capacity.
   */
  nuuk_list_status_t (* put)(nuuk_list_t * list, uint32_t index, void * src);

  /**
   * Insert `n_elements` elements to the list, starting at `index`, shifting previous elements, if any.
   *
   * @param list       The list.
   * @param index      The index at which to insert elements.
   * @param n_elements The number of elements to copy.
   * @param src        The source buffer, which must have a size of at least `element_size * n_elements`.
   *
   * @return
   *   - `NUUK_LIST_OK`        if the operation is successful.
   *   - `NUUK_LIST_ERR_ALLOC` if memory allocation failed.
   *   - `NUUK_LIST_ERR_MAX`   if the list has a fixed capacity, and `index + n_elements` is greater than the fixed
   *                           capacity.
   */
  nuuk_list_status_t (* insert_range)(nuuk_list_t * list, uint32_t index, uint32_t n_elements, void * src);

  /**
   * Insert a single element to the list at `index`, shifting previous elements, if any.
   *
   * @param list       The list.
   * @param index      The index at which to insert the element.
   * @param src        The source buffer, which must have a size of at least `element_size`.
   *
   * @return
   *   - `NUUK_LIST_OK`        if the operation is successful.
   *   - `NUUK_LIST_ERR_ALLOC` if memory allocation failed.
   *   - `NUUK_LIST_ERR_MAX`   if the list has a fixed capacity, and `index` is greater than the fixed capacity.
   */
  nuuk_list_status_t (* insert)(nuuk_list_t * list, uint32_t index, void * src);

  /**
   * Insert `n_elements` elements to the end of the list.
   *
   * @param list       The list.
   * @param n_elements The number of elements to append.
   * @param src        The source buffer, which must have a size of at least `element_size * n_elements`.
   *
   * @return
   *   - `NUUK_LIST_OK`        if the operation is successful.
   *   - `NUUK_LIST_ERR_ALLOC` if memory allocation failed.
   *   - `NUUK_LIST_ERR_MAX`   if the list has a fixed capacity, and list size + `n_elements` is greater than the
   *                           fixed capacity.
   */
  nuuk_list_status_t (* append_range)(nuuk_list_t * list, uint32_t n_elements, void * src);

  /**
   * Insert a single element to the end of the list.
   *
   * @param list The list.
   * @param src  The source buffer, which must have a size of at least `element_size`.
   *
   * @return
   *   - `NUUK_LIST_OK`        if the operation is successful.
   *   - `NUUK_LIST_ERR_ALLOC` if memory allocation failed.
   *   - `NUUK_LIST_ERR_MAX`   if the list has a fixed capacity, and list size + 1 is greater than the fixed
   *                           capacity.
   */
  nuuk_list_status_t (* append)(nuuk_list_t * list, void * src);

  /**
   * Insert `n_elements` elements to the beginning of the list, shifting the elements previously held.
   *
   * @param list       The list.
   * @param n_elements The number of elements to append.
   * @param src        The source buffer, which must have a size of at least `element_size * n_elements`.
   *
   * @return
   *   - `NUUK_LIST_OK`        if the operation is successful.
   *   - `NUUK_LIST_ERR_ALLOC` if memory allocation failed.
   *   - `NUUK_LIST_ERR_MAX`   if the list has a fixed capacity, and list size + `n_elements` is greater than the
   *                           fixed capacity.
   */
  nuuk_list_status_t (* prepend_range)(nuuk_list_t * list, uint32_t n_elements, void * src);

  /**
   * Insert a single element to the beginning on the list, shifting the elements previously held.
   *
   * @param list The list.
   * @param src  The source buffer, which must have a size of at least `element_size`.
   *
   * @return
   *   - `NUUK_LIST_OK`        if the operation is successful.
   *   - `NUUK_LIST_ERR_ALLOC` if memory allocation failed.
   *   - `NUUK_LIST_ERR_MAX`   if the list has a fixed capacity, and list size + 1 is greater than the fixed
   *                           capacity.
   */
  nuuk_list_status_t (* prepend)(nuuk_list_t * list, void * src);

  /**
   * Remove a range of elements from the list, shifting the position of elements held after.
   *
   * The range will start a `start_index` (inclusive) and end at `end_index` (exclusive).
   *
   * @param list        The list.
   * @param start_index The start index (inclusive).
   * @param end_index   The end index (exclusive).
   *
   * @return
   *   - `NUUK_LIST_OK`        if the range was removed successfully.
   *   - `NUUK_LIST_ERR_INDEX` if the range is invalid.
   */
  nuuk_list_status_t (* remove_range)(nuuk_list_t * list, uint32_t start_index, uint32_t end_index);

  /**
   * Remove a single element from the list, shifting the position of elements held after.
   *
   * @param list        The list.
   * @param start_index The index of the element to remove.
   *
   * @return
   *   - `NUUK_LIST_OK`        if the range was removed successfully.
   *   - `NUUK_LIST_ERR_INDEX` if the index is invalid.
   */
  nuuk_list_status_t (* remove)(nuuk_list_t * list, uint32_t index);

  /**
   * Iterate through the elements of the list, calling `fn` for each.
   *
   * The iteration will stop when the end of the list is reached, or immediately if a call of `fn` returns `true`.
   *
   * @param list  The list.
   * @param fn    The function.
   * @param udata Data to pass to the `fn` function.
   *
   * @return `false` if iteration has been interrupted, or `true` otherwise.
   */
  bool (* foreach)(nuuk_list_t * list, nuuk_list_foreach_fn_t fn, void * udata);

  /**
   * Delete all elements at and beyond specified index. Capacity is left untouched.
   *
   * @param list  The list.
   * @param index The number of elements to keep in the list.
   *
   * @return
   *   - `NUUK_LIST_OK`        if the operation is successful.
   *   - `NUUK_LIST_ERR_INDEX` if `index` is greater than the list size.
   */
  nuuk_list_status_t (* trim)(nuuk_list_t * list, uint32_t index);

};


/******************************************************************************
 * INTERFACE FUNCTIONS
 */

/**
 * Initialize a `nuuk_list_t`. This should be called only by subtypes.
 *
 * @param list         The list to initialize.
 * @param hooks        The hooks to use for this list.
 *
 * @return `NUUK_LIST_OK`.
 */
nuuk_list_status_t nuuk_list_interface_init(nuuk_list_t * list, const nuuk_list_hooks_t * hooks);


/**
 * Destroys a `nuuk_list_t`, and release resources used by it.
 *
 * @param list The list to destroy.
 */
#define nuuk_list_interface_destroy(list)


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

/**
 * Destroy a list, and release resources used by it.
 *
 * @param list The list to destroy.
 */
static inline
void nuuk_list_destroy(nuuk_list_t * list) {
  nuuk_hook(destroy, 0, list);
}


/******************************************************************************
 * INFO FUNCTIONS
 */

/**
 * Get the size of the list.
 *
 * @return The number of elements currently in the list.
 */
static inline
uint32_t nuuk_list_size(nuuk_list_t * list) {
  return nuuk_hook(size, 0, list);
}


/******************************************************************************
 * READ FUNCTIONS
 */

/**
 * Get a range of elements from the list, and copy them to `dest`.
 *
 * The range will start a `start_index` (inclusive) and end at `end_index` (exclusive).
 *
 * @param list        The list.
 * @param start_index The start index (inclusive).
 * @param end_index   The end index (exclusive).
 * @param dest        The destination buffer, which must have a size of at least
 *                    `element_size * (end_index - start_index)`.
 *
 * @return
 *   - `NUUK_LIST_OK`              if the elements were copied successfully.
 *   - `NUUK_LIST_ERR_INDEX`       if the requested index range is invalid.
 *   - `NUUK_LIST_ERR_HOOK` if the operation is not supported.
 */
static inline
nuuk_list_status_t nuuk_list_get_range(nuuk_list_t * list, uint32_t start_index, uint32_t end_index, void * dest) {
  return nuuk_hook(get_range, NUUK_LIST_ERR_HOOK, list, start_index, end_index, dest);
}

/**
 * Get a single element from the list, and copy it to `dest`.
 *
 * @param list  The list.
 * @param index The element index.
 * @param dest  The destination buffer, which must have a size of at least `element_size`.
 *
 * @return
 *   - `NUUK_LIST_OK`              if the element was copied successfully.
 *   - `NUUK_LIST_ERR_INDEX`       if the requested index is invalid.
 *   - `NUUK_LIST_ERR_HOOK` if the operation is not supported.
 */
static inline
nuuk_list_status_t nuuk_list_get(nuuk_list_t * list, uint32_t index, void * dest) {
  return nuuk_hook(get, NUUK_LIST_ERR_HOOK, list, index, dest);
}


/******************************************************************************
 * WRITE FUNCTIONS
 */

/**
 * Put `n_elements` elements to the list, starting at `index`, replacing elements on these positions, if any.
 *
 * @param list       The list.
 * @param index      The index at which to put elements.
 * @param n_elements The number of elements to copy.
 * @param src        The source buffer, which must have a size of at least `element_size * n_elements`.
 *
 * @return
 *   - `NUUK_LIST_OK`              if the operation is successful.
 *   - `NUUK_LIST_ERR_ALLOC`       if memory allocation failed.
 *   - `NUUK_LIST_ERR_MAX`         if the list has a fixed capacity, and `index + n_elements` is greater than the fixed
 *                                 capacity.
 *   - `NUUK_LIST_ERR_HOOK` if the operation is not supported.
 */
static inline
nuuk_list_status_t nuuk_list_put_range(nuuk_list_t * list, uint32_t index, uint32_t n_elements, void * src) {
  return nuuk_hook(put_range, NUUK_LIST_ERR_HOOK, list, index, n_elements, src);
}

/**
 * Put a single element to the list at `index`, replacing the element at `index`, if any.
 *
 * @param list       The list.
 * @param index      The index at which to put the element.
 * @param src        The source buffer, which must have a size of at least `element_size`.
 *
 * @return
 *   - `NUUK_LIST_OK`              if the operation is successful.
 *   - `NUUK_LIST_ERR_ALLOC`       if memory allocation failed.
 *   - `NUUK_LIST_ERR_MAX`         if the list has a fixed capacity, and `index` is greater than the fixed capacity.
 *   - `NUUK_LIST_ERR_HOOK` if the operation is not supported.
 */
static inline
nuuk_list_status_t nuuk_list_put(nuuk_list_t * list, uint32_t index, void * src) {
  return nuuk_hook(put, NUUK_LIST_ERR_HOOK, list, index, src);
}

/**
 * Insert `n_elements` elements to the list, starting at `index`, shifting previous elements, if any.
 *
 * @param list       The list.
 * @param index      The index at which to insert elements.
 * @param n_elements The number of elements to copy.
 * @param src        The source buffer, which must have a size of at least `element_size * n_elements`.
 *
 * @return
 *   - `NUUK_LIST_OK`              if the operation is successful.
 *   - `NUUK_LIST_ERR_ALLOC`       if memory allocation failed.
 *   - `NUUK_LIST_ERR_MAX`         if the list has a fixed capacity, and `index + n_elements` is greater than the fixed
 *                                 capacity.
 *   - `NUUK_LIST_ERR_HOOK` if the operation is not supported.
 */
static inline
nuuk_list_status_t nuuk_list_insert_range(nuuk_list_t * list, uint32_t index, uint32_t n_elements, void * src) {
  return nuuk_hook(insert_range, NUUK_LIST_ERR_HOOK, list, index, n_elements, src);
}

/**
 * Insert a single element to the list at `index`, shifting previous elements, if any.
 *
 * @param list       The list.
 * @param index      The index at which to insert the element.
 * @param src        The source buffer, which must have a size of at least `element_size`.
 *
 * @return
 *   - `NUUK_LIST_OK`              if the operation is successful.
 *   - `NUUK_LIST_ERR_ALLOC`       if memory allocation failed.
 *   - `NUUK_LIST_ERR_MAX`         if the list has a fixed capacity, and `index` is greater than the fixed capacity.
 *   - `NUUK_LIST_ERR_HOOK` if the operation is not supported.
 */
static inline
nuuk_list_status_t nuuk_list_insert(nuuk_list_t * list, uint32_t index, void * src) {
  return nuuk_hook(insert, NUUK_LIST_ERR_HOOK, list, index, src);
}

/**
 * Insert `n_elements` elements to the end of the list.
 *
 * @param list       The list.
 * @param n_elements The number of elements to append.
 * @param src        The source buffer, which must have a size of at least `element_size * n_elements`.
 *
 * @return
 *   - `NUUK_LIST_OK`        if the operation is successful.
 *   - `NUUK_LIST_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_LIST_ERR_MAX`   if the list has a fixed capacity, and list size + `n_elements` is greater than the
 *                           fixed capacity.
 */
static inline
nuuk_list_status_t nuuk_list_append_range(nuuk_list_t * list, uint32_t n_elements, void * src) {
  return nuuk_hook(append_range, NUUK_LIST_ERR_HOOK, list, n_elements, src);
}

/**
 * Insert a single element to the end of the list.
 *
 * @param list The list.
 * @param src  The source buffer, which must have a size of at least `element_size`.
 *
 * @return
 *   - `NUUK_LIST_OK`        if the operation is successful.
 *   - `NUUK_LIST_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_LIST_ERR_MAX`   if the list has a fixed capacity, and list size + 1 is greater than the fixed
 *                           capacity.
 */
static inline
nuuk_list_status_t nuuk_list_append(nuuk_list_t * list, void * src) {
  return nuuk_hook(append, NUUK_LIST_ERR_HOOK, list, src);
}

/**
 * Insert `n_elements` elements to the beginning of the list, shifting the elements previously held.
 *
 * @param list       The list.
 * @param n_elements The number of elements to append.
 * @param src        The source buffer, which must have a size of at least `element_size * n_elements`.
 *
 * @return
 *   - `NUUK_LIST_OK`        if the operation is successful.
 *   - `NUUK_LIST_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_LIST_ERR_MAX`   if the list has a fixed capacity, and list size + `n_elements` is greater than the
 *                           fixed capacity.
 */
static inline
nuuk_list_status_t nuuk_list_prepend_range(nuuk_list_t * list, uint32_t n_elements, void * src) {
  return nuuk_hook(prepend_range, NUUK_LIST_ERR_HOOK, list, n_elements, src);
}

/**
 * Insert a single element to the beginning on the list, shifting the elements previously held.
 *
 * @param list The list.
 * @param src  The source buffer, which must have a size of at least `element_size`.
 *
 * @return
 *   - `NUUK_LIST_OK`        if the operation is successful.
 *   - `NUUK_LIST_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_LIST_ERR_MAX`   if the list has a fixed capacity, and list size + 1 is greater than the fixed
 *                           capacity.
 */
static inline
nuuk_list_status_t nuuk_list_prepend(nuuk_list_t * list, void * src) {
  return nuuk_hook(prepend, NUUK_LIST_ERR_HOOK, list, src);
}


/******************************************************************************
 * DELETE FUNCTIONS
 */

/**
 * Remove a range of elements from the list, shifting the position of elements held after.
 *
 * The range will start a `start_index` (inclusive) and end at `end_index` (exclusive).
 *
 * @param list        The list.
 * @param start_index The start index (inclusive).
 * @param end_index   The end index (exclusive).
 *
 * @return
 *   - `NUUK_LIST_OK`        if the range was removed successfully.
 *   - `NUUK_LIST_ERR_INDEX` if the range is invalid.
 */
static inline
nuuk_list_status_t nuuk_list_remove_range(nuuk_list_t * list, uint32_t start_index, uint32_t end_index) {
  return nuuk_hook(remove_range, NUUK_LIST_ERR_HOOK, list, start_index, end_index);
}

/**
 * Remove a single element from the list, shifting the position of elements held after.
 *
 * @param list        The list.
 * @param start_index The index of the element to remove.
 *
 * @return
 *   - `NUUK_LIST_OK`        if the range was removed successfully.
 *   - `NUUK_LIST_ERR_INDEX` if the index is invalid.
 */
static inline
nuuk_list_status_t nuuk_list_remove(nuuk_list_t * list, uint32_t index) {
  return nuuk_hook(remove, NUUK_LIST_ERR_HOOK, list, index);
}


/******************************************************************************
 * MODIFICATION FUNCTIONS
 */

/**
 * Delete all elements at and beyond specified index. Capacity is left untouched.
 *
 * @param list  The list.
 * @param index The number of elements to keep in the list.
 *
 * @return
 *   - `NUUK_LIST_OK`        if the operation is successful.
 *   - `NUUK_LIST_ERR_INDEX` if `index` is greater than the list size.
 */
static inline
nuuk_list_status_t nuuk_list_trim(nuuk_list_t * list, uint32_t index) {
  return nuuk_hook(trim, false, list, index);
}


/******************************************************************************
 * ITERATION FUNCTIONS
 */

/**
 * Iterate through the elements of the list, calling `fn` for each.
 *
 * The iteration will stop when the end of the list is reached, or immediately if a call of `fn` returns `true`.
 *
 * @param list  The list.
 * @param fn    The function.
 * @param udata Data to pass to the `fn` function.
 *
 * @return `false` if iteration has been interrupted, or `true` otherwise.
 */
static inline
bool nuuk_list_foreach(nuuk_list_t * list, nuuk_list_foreach_fn_t fn, void * udata) {
  return nuuk_hook(foreach, false, list, fn, udata);
}


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
