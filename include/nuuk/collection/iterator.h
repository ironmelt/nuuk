/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief List interface.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include "nuuk/util/hooks.h"
#include "nuuk/util/types.h"


/******************************************************************************
 * TYPES
 */

/* Forward declaration of `nuuk_iterator_hooks_t`. */
typedef struct nuuk_iterator_hooks_s nuuk_iterator_hooks_t;

/**
 * The `nuuk_iterator_t` interface.
 * All nuuk list implementations inherits from this structure.
 */
typedef struct nuuk_iterator_t {

  /**
   * Hooks used by `as_iterator_t` functions.
   */
  const nuuk_iterator_hooks_t * hooks;

} nuuk_iterator_t;

/**
 * Hooks used by `as_iterator_t` implementations.
 */
struct nuuk_iterator_hooks_s {

  /**
   * Destroy an iterator, and release resources used by it.
   *
   * @param list The iterator to destroy.
   */
  void (* destroy)(nuuk_iterator_t * iter);

  /**
   * Tests whether there is another element in the iterator.
   *
   * @return `true` if there is another element in the iterator, `false` otherwise.
   */
  bool (* has_next)(nuuk_iterator_t * iter);

  /**
   * Read the next value.
   *
   * @return the next value in the iterator.
   */
  void * (* next)(nuuk_iterator_t * iter);

};


/******************************************************************************
 * INTERFACE FUNCTIONS
 */

/**
 * Initialize a `nuuk_iterator_t`. This should be called only by subtypes.
 *
 * @param list         The iterator to initialize.
 * @param hooks        The hooks to use for this list.
 */
void nuuk_iterator_interface_init(nuuk_iterator_t * iter, const nuuk_iterator_hooks_t * hooks);


/**
 * Destroys a `nuuk_iterator_t`, and release resources used by it.
 *
 * @param list The iterator to destroy.
 */
#define nuuk_iterator_interface_destroy(iter)


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

/**
 * Destroy an iterator, and release resources used by it.
 *
 * @param list The iterator to destroy.
 */
static inline
void nuuk_iterator_destroy(nuuk_iterator_t * iter) {
  nuuk_hook(destroy, 0, iter);
}


/******************************************************************************
 * ITERATOR FUNCTIONS
 */

/**
 * Tests whether there is another element in the iterator.
 *
 * @return `true` if there is another element in the iterator, `false` otherwise.
 */
static inline
bool nuuk_iterator_has_next(nuuk_iterator_t * iter) {
  return nuuk_hook(has_next, 0, iter);
}

/**
 * Read the next value.
 *
 * @return the next value in the iterator.
 */
static inline
void * nuuk_iterator_next(nuuk_iterator_t * iter) {
  return nuuk_hook(next, 0, iter);
}


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
