/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief Arraylist implementation.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include "nuuk/collection/list.h"
#include "nuuk/util/alloc.h"


/******************************************************************************
 * EXTERN CONSTANTS
 */

extern const nuuk_list_hooks_t nuuk_arraylist_hooks;


/******************************************************************************
 * TYPES
 */

/**
 * An arraylist implementation.
 */
typedef struct nuuk_arraylist_s {

  /**
   * @Private
   * `nuuk_arraylist_t` is a `nuuk_list_t`, and can be casted to it.
   */
  nuuk_list_t _;

  /**
   * Current size of the list.
   */
  uint32_t size;

  /**
   * Current allocated capacity of the list.
   */
  uint32_t capacity;

  /**
   * Block size of the list.
   */
  uint32_t block_size;


  /**
   * The list element size.
   */
  uint32_t element_size;

  /**
   * If `true`, the element array will be freed when the list is destroyed.
   */
  bool free_elements;

  /**
   * Current element array.
   */
  char * elements;

} nuuk_arraylist_t;

/**
 * Status codes for `nuuk_arraylist_t` operations.
 */
typedef enum nuuk_arraylist_status_e {

  /**
   * Successful operation.
   */
  NUUK_ARRAYLIST_OK        = NUUK_LIST_OK,

  /**
   * Memory allocation failure.
   */
  NUUK_ARRAYLIST_ERR_ALLOC = NUUK_LIST_ERR_ALLOC,

  /**
   * Maximum capacity reached.
   */
  NUUK_ARRAYLIST_ERR_MAX   = NUUK_LIST_ERR_MAX,

  /**
   * Index out of range.
   */
  NUUK_ARRAYLIST_ERR_INDEX = NUUK_LIST_ERR_INDEX,

} nuuk_arraylist_status_t;


/******************************************************************************
 * INSTANCE FUNCTIONS
 */

/**
 * Initialize a heap-allocated list.
 *
 * @param list         The list to initialize.
 * @param element_size The size of a single element of the list in bytes.
 * @param capacity     The initial capacity to allocate. If `0`, the storage will be allocated lazily.
 * @param block_size   The block size of the list, i.e. by how much elements the list will be extended when additional
 *                     capacity is needed. If `0`, the list will have a fixed-size of `capacity`.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the list has been initialized successfully.
 *   - `NUUK_ARRAYLIST_ERR_ALLOC` if memory couldn't be allocated.
 */
nuuk_arraylist_status_t nuuk_arraylist_init(
    nuuk_arraylist_t * list, uint32_t element_size, uint32_t capacity, uint32_t block_size);

/**
 * Initialize a stack-allocated list.
 *
 * Due to the stack allocation, the resulting list has a fixed size of `capacity`, and cannot be extended.
 *
 * @param list         The list to initialize.
 * @param element_size The size of a single element of the list in bytes.
 * @param __capacity   The initial capacity to allocate. If `0`, the storage will be allocated lazily.
 */
#define nuuk_arraylist_inita(list, element_size, __capacity) \
  nuuk_arraylist_init((list), (element_size), 0, 0); \
  (list)->free_elements = false; \
  (list)->capacity = __capacity; \
  (list)->elements = nuuk_alloca((__capacity) * (element_size));

/**
 * Destroy a list, and release resources used by it.
 *
 * @param list The list to destroy.
 */
void nuuk_arraylist_destroy(nuuk_arraylist_t * list);

/**
 * Ensure the list has enough allocated capacity to store at least `capacity` elements without any further reallocation.
 *
 * @param list     The list.
 * @param capacity The desired minimum capacity.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the operation is successful.
 *   - `NUUK_ARRAYLIST_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_ARRAYLIST_ERR_MAX`   if the list has a fixed capacity, and `capacity` is greater than the fixed capacity.
 */
nuuk_arraylist_status_t nuuk_arraylist_ensure_capacity(nuuk_arraylist_t * list, uint32_t capacity);


/******************************************************************************
 * INFO FUNCTIONS
 */

/**
 * Get the size of the list.
 *
 * @return The number of elements currently in the list.
 */
static inline
uint32_t nuuk_arraylist_size(nuuk_arraylist_t * list) {
  return list->size;
}

/**
 * Get the allocated capacity of the list.
 *
 * @return The allocated capacity of the list.
 */
static inline
uint32_t nuuk_arraylist_capacity(nuuk_arraylist_t * list) {
  return list->capacity;
}


/******************************************************************************
 * READ FUNCTIONS
 */

/**
 * Get a range of elements from the list, and copy them to `dest`.
 *
 * The range will start a `start_index` (inclusive) and end at `end_index` (exclusive).
 *
 * @param list        The list.
 * @param start_index The start index (inclusive).
 * @param end_index   The end index (exclusive).
 * @param dest        The destination buffer, which must have a size of at least
 *                    `element_size * (end_index - start_index)`.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the elements were copied successfully.
 *   - `NUUK_ARRAYLIST_ERR_INDEX` if the requested index range is invalid.
 */
nuuk_arraylist_status_t nuuk_arraylist_get_range(
    nuuk_arraylist_t * list, uint32_t start_index, uint32_t end_index, void * dest);

/**
 * Get a single element from the list, and copy it to `dest`.
 *
 * @param list  The list.
 * @param index The element index.
 * @param dest  The destination buffer, which must have a size of at least `element_size`.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the element was copied successfully.
 *   - `NUUK_ARRAYLIST_ERR_INDEX` if the requested index is invalid.
 */
static inline
nuuk_arraylist_status_t nuuk_arraylist_get(nuuk_arraylist_t * list, uint32_t index, void * dest) {
  return nuuk_arraylist_get_range(list, index, index + 1, dest);
}


/******************************************************************************
 * WRITE FUNCTIONS
 */

/**
 * Put `n_elements` elements to the list, starting at `index`, replacing elements on these positions, if any.
 *
 * @param list       The list.
 * @param index      The index at which to put elements.
 * @param n_elements The number of elements to copy.
 * @param src        The source buffer, which must have a size of at least `element_size * n_elements`.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the operation is successful.
 *   - `NUUK_ARRAYLIST_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_ARRAYLIST_ERR_MAX`   if the list has a fixed capacity, and `index + n_elements` is greater than the fixed
 *                                capacity.
 */
nuuk_arraylist_status_t nuuk_arraylist_put_range(
    nuuk_arraylist_t * list, uint32_t index, uint32_t n_elements, void * src);

/**
 * Put a single element to the list at `index`, replacing the element at `index`, if any.
 *
 * @param list       The list.
 * @param index      The index at which to put the element.
 * @param src        The source buffer, which must have a size of at least `element_size`.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the operation is successful.
 *   - `NUUK_ARRAYLIST_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_ARRAYLIST_ERR_MAX`   if the list has a fixed capacity, and `index` is greater than the fixed capacity.
 */
static inline
nuuk_arraylist_status_t nuuk_arraylist_put(nuuk_arraylist_t * list, uint32_t index, void * src) {
  return nuuk_arraylist_put_range(list, index, 1, src);
}

/**
 * Insert `n_elements` elements to the list, starting at `index`, shifting previous elements, if any.
 *
 * @param list       The list.
 * @param index      The index at which to insert elements.
 * @param n_elements The number of elements to copy.
 * @param src        The source buffer, which must have a size of at least `element_size * n_elements`.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the operation is successful.
 *   - `NUUK_ARRAYLIST_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_ARRAYLIST_ERR_MAX`   if the list has a fixed capacity, and `index + n_elements` is greater than the fixed
 *                                capacity.
 */
nuuk_arraylist_status_t nuuk_arraylist_insert_range(
    nuuk_arraylist_t * list, uint32_t index, uint32_t n_elements, void * src);

/**
 * Insert a single element to the list at `index`, shifting previous elements, if any.
 *
 * @param list  The list.
 * @param index The index at which to insert the element.
 * @param src   The source buffer, which must have a size of at least `element_size`.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the operation is successful.
 *   - `NUUK_ARRAYLIST_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_ARRAYLIST_ERR_MAX`   if the list has a fixed capacity, and `index` is greater than the fixed capacity.
 */
static inline
nuuk_arraylist_status_t nuuk_arraylist_insert(nuuk_arraylist_t * list, uint32_t index, void * src) {
  return nuuk_arraylist_insert_range(list, index, 1, src);
}

/**
 * Insert `n_elements` elements to the end of the list.
 *
 * @param list       The list.
 * @param n_elements The number of elements to append.
 * @param src        The source buffer, which must have a size of at least `element_size * n_elements`.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the operation is successful.
 *   - `NUUK_ARRAYLIST_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_ARRAYLIST_ERR_MAX`   if the list has a fixed capacity, and list size + `n_elements` is greater than the
 *                                fixed capacity.
 */
static inline
nuuk_arraylist_status_t nuuk_arraylist_append_range(nuuk_arraylist_t * list, uint32_t n_elements, void * src) {
  return nuuk_arraylist_put_range(list, nuuk_arraylist_size(list), n_elements, src);
}

/**
 * Insert a single element to the end of the list.
 *
 * @param list The list.
 * @param src  The source buffer, which must have a size of at least `element_size`.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the operation is successful.
 *   - `NUUK_ARRAYLIST_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_ARRAYLIST_ERR_MAX`   if the list has a fixed capacity, and list size + 1 is greater than the fixed
 *                                capacity.
 */
static inline
nuuk_arraylist_status_t nuuk_arraylist_append(nuuk_arraylist_t * list, void * src) {
  return nuuk_arraylist_append_range(list, 1, src);
}

/**
 * Insert `n_elements` elements to the beginning of the list, shifting the elements previously held.
 *
 * @param list       The list.
 * @param n_elements The number of elements to append.
 * @param src        The source buffer, which must have a size of at least `element_size * n_elements`.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the operation is successful.
 *   - `NUUK_ARRAYLIST_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_ARRAYLIST_ERR_MAX`   if the list has a fixed capacity, and list size + `n_elements` is greater than the
 *                                fixed capacity.
 */
static inline
nuuk_arraylist_status_t nuuk_arraylist_prepend_range(nuuk_arraylist_t * list, uint32_t n_elements, void * src) {
  return nuuk_arraylist_insert_range(list, 0, n_elements, src);
}

/**
 * Insert a single element to the beginning on the list, shifting the elements previously held.
 *
 * @param list The list.
 * @param src  The source buffer, which must have a size of at least `element_size`.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the operation is successful.
 *   - `NUUK_ARRAYLIST_ERR_ALLOC` if memory allocation failed.
 *   - `NUUK_ARRAYLIST_ERR_MAX`   if the list has a fixed capacity, and list size + 1 is greater than the fixed
 *                                capacity.
 */
static inline
nuuk_arraylist_status_t nuuk_arraylist_prepend(nuuk_arraylist_t * list, void * src) {
  return nuuk_arraylist_prepend_range(list, 1, src);
}


/******************************************************************************
 * DELETE FUNCTIONS
 */

/**
 * Remove a range of elements from the list, shifting the position of elements held after.
 *
 * The range will start a `start_index` (inclusive) and end at `end_index` (exclusive).
 *
 * @param list        The list.
 * @param start_index The start index (inclusive).
 * @param end_index   The end index (exclusive).
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the range was removed successfully.
 *   - `NUUK_ARRAYLIST_ERR_INDEX` if the range is invalid.
 */
nuuk_arraylist_status_t nuuk_arraylist_remove_range(nuuk_arraylist_t * list, uint32_t start_index, uint32_t end_index);

/**
 * Remove a single element from the list, shifting the position of elements held after.
 *
 * @param list        The list.
 * @param start_index The index of the element to remove.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the range was removed successfully.
 *   - `NUUK_ARRAYLIST_ERR_INDEX` if the index is invalid.
 */
static inline
nuuk_arraylist_status_t nuuk_arraylist_remove(nuuk_arraylist_t * list, uint32_t index) {
  return nuuk_arraylist_remove_range(list, index, index + 1);
}


/******************************************************************************
 * MODIFICATION FUNCTIONS
 */

/**
 * Delete all elements at and beyond specified index. Capacity is left untouched.
 *
 * @param list  The list.
 * @param index The number of elements to keep in the list.
 *
 * @return
 *   - `NUUK_ARRAYLIST_OK`        if the operation is successful.
 *   - `NUUK_ARRAYLIST_ERR_INDEX` if `index` is greater than the list size.
 */
nuuk_arraylist_status_t nuuk_arraylist_trim(nuuk_arraylist_t * list, uint32_t index);


/******************************************************************************
 * ITERATION FUNCTIONS
 */

/**
 * Iterate through the elements of the list, calling `fn` for each.
 *
 * The iteration will stop when the end of the list is reached, or immediately if a call of `fn` returns `true`.
 *
 * @param list  The list.
 * @param fn    The function.
 * @param udata Data to pass to the `fn` function.
 *
 * @return `false` if iteration has been interrupted, or `true` otherwise.
 */
bool nuuk_arraylist_foreach(nuuk_arraylist_t * list, nuuk_list_foreach_fn_t fn, void * udata);


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
