/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief Dynamic-size, nonblocking hash map.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include <string.h>

#include "nuuk/collection/concurrent/dsnmap_fset.h"
#include "nuuk/collection/map.h"
#include "nuuk/concurrent/atomic.h"
#include "nuuk/concurrent/hazard.h"
#include "nuuk/util/alloc.h"


/******************************************************************************
 * EXTERN CONSTANTS
 */

extern const nuuk_map_hooks_t nuuk_dsnmap_hooks;


/******************************************************************************
 * TYPES
 */

/**
 * Concurrent hashmap implementation, based on the work from Yujie Liu et al. (Dynamically-sized nonblocking hash
 * ables).
 *
 * This implementation is thread-safe.
 */
typedef struct nuuk_dsnmap_s {

  /**
   * @private
   * `nuuk_dsnmap_t` is a `nuuk_map_t`, and can be casted to it.
   */
  nuuk_map_t _;

  /**
   * Current head.
   */
  atomic_intptr_t node;

  /**
   * Size of the map.
   */
  atomic_uint32_t size;

  /**
   * Hazard pointers.
   */
  nuuk_hazard_t hazard;

  /**
   * Fset config.
   */
  nuuk_dsnmap_fset_config_t fset_config;

} nuuk_dsnmap_t;


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

/**
 * Initialize a dsnmap with storage on heap, and allocates the element storage.
 *
 * @param map          The `nuuk_dsnmap_t` to initialize.
 * @param element_type The element type.
 * @param capacity     The capacity to allocate. If `0`, the storage will be allocated lazily.
 * @param load_factor  The load factor of the map. If `0`, the map will have a fixed maximum size of `capacity`.
 *
 * @return
 *  - `NUUK_MAP_OK`        if initialization was successful.
 *  - `NUUK_MAP_ERR_ALLOC` if memory allocation failed.
 */
nuuk_map_status_t nuuk_dsnmap_init(nuuk_dsnmap_t * map, const nuuk_map_element_type_t * element_type,
                                   uint32_t capacity, float load_factor);

/**
 * Destroy a dsnmap and release resources used by it.
 *
 * @param map The `nuuk_dsnmap_t` to destroy.
 */
void nuuk_dsnmap_destroy(nuuk_dsnmap_t * map);


/******************************************************************************
 * INFO FUNCTIONS
 */

/**
 * Get the number of elements currently in the map.
 *
 * @param map The map.
 *
 * @return The number of elements currently in `map`.
 */
uint32_t nuuk_dsnmap_size(nuuk_dsnmap_t * map);


/******************************************************************************
 * PUT FUNCTIONS
 */

/**
 * Pus a key / value to the map.
 *
 * If `prev_value` is not `NULL`, the previous value of the key will be copied to `prev_value` if the key was already
 * present in the map. Otherwise, it will be set to `NULL`.
 *
 * @param map        The map.
 * @param key        Pointer to the key.
 * @param value      Pointer to the value.
 * @param prev_value Memory location to set the previous value to. Can be `NULL`.
 *
 * @return The write result. The status can be one of:
 *
 *  - `NUUK_MAP_OK`        if operation was successful.
 *  - `NUUK_MAP_ERR_ALLOC` if memory allocation failed.
 *  - `NUUK_MAP_ERR_MAX`   if `capacity` is higher than the currently allocated capacity, and \
 *                         `nuuk_dsnmap_t.load_factor` is `0`
 */
nuuk_map_write_result_t nuuk_dsnmap_put(nuuk_dsnmap_t * map, const void * key, const void * value, void * prev_value);


/******************************************************************************
 * GET FUNCTIONS
 */

/**
 * Check whether the map currently contains a mapping for a key.
 *
 * @param map The map.
 * @param key The key.
 *
 * @return `true` if `map` has a mapping for `key`.
 */
bool nuuk_dsnmap_has_key(nuuk_dsnmap_t * map, const void * key);

/**
 * Get the value currently associated with a key.
 *
 * @param map   The map.
 * @param key   The key.
 * @param value Memory location to which to copy the value, if any. If not found, it will be left untouched.
 *
 * @return `true` if `map` has a mapping for `key`.
 */
bool nuuk_dsnmap_get(nuuk_dsnmap_t * map, const void * key, void * value);


/******************************************************************************
 * DELETE FUNCTIONS
 */

/**
 * Delete the mapping for a key.
 *
 * @param map        The map.
 * @param key        The key to delete mapping for.
 * @param prev_value Memory location to set the previous value to. Can be `NULL`.
 *
 * @return The write result.
 */
nuuk_map_write_result_t nuuk_dsnmap_remove(nuuk_dsnmap_t * map, const void * key, void * prev_value);


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
