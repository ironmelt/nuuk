/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include "nuuk/collection/map_record.h"
#include "nuuk/concurrent/cow.h"
#include "nuuk/util/status.h"


/******************************************************************************
 * TYPES
 */

/**
 * @private
 * Freezable, copy-on-write set implementation.
 */
typedef struct nuuk_dsnmap_fset_s {

  /**
   * Current head.
   */
  nuuk_cow_t node;

} nuuk_dsnmap_fset_t;

/**
 * @private
 * Fset config.
 */
typedef struct nuuk_dsnmap_fset_config_s {

  /**
   * The cow config.
   */
  nuuk_cow_config_t cow_config;

  /**
   * The element type.
   */
  const nuuk_map_element_type_t * element_type;

} nuuk_dsnmap_fset_config_t;


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
