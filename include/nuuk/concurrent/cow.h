/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief Low-level utilies to work with COW data structures.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include "nuuk/concurrent/atomic.h"
#include "nuuk/concurrent/hazard.h"
#include "nuuk/util/status.h"


/******************************************************************************
 * TYPES
 */

/**
 * Low-level COW implementation.
 */
typedef atomic_intptr_t nuuk_cow_t;

/**
 * Transform function, called by `nuuk_cow_run_transform()`.
 *
 * @param cow              The copy on write structure to transform.
 * @param node             The node to transform.
 * @param transformed_node A pointer to the transformed node location pointer.
 * @param udata            The user data provided to the `nuuk_cow_run_transform()`.
 *
 * @return `NUUK_COW_OK` if the transform has been applied successfully.
 */
typedef int (* nuuk_cow_transform_t) (void * node, void ** transformed_node, void * udata);

/**
 * Filtering function for selective free. It is useful for example when the COW can be set to a heap-allocated variable
 * that *MUST NOT* be passed to the `free` function.
 *
 * @param ptr The pointer to test.
 *
 * @return `true` if the `ptr` may be freed.
 */
typedef bool (* nuuk_cow_filter_t)(void * ptr);

/**
 * COW configuration.
 */
typedef struct nuuk_cow_config_s {

  /**
   * Optional free filter.
   */
  nuuk_cow_filter_t filter;

  /**
   * Hazard to use.
   */
  nuuk_hazard_t * hazard;

  /**
   * Static hazard pointer to use.
   */
  uint32_t hazard_ptr_no;

} nuuk_cow_config_t;

/**
 * Status codes for various `nuuk_hazard_t` operations.
 */

typedef enum nuuk_cow_status_e {

  /**
   * Normal operation.
   */
  NUUK_COW_OK         = NUUK_OK,

  /**
   * Hazard pointer reported an error.
   */
  NUUK_COW_ERR_HAZARD = NUUK_ERR_EXTERNAL

} nuuk_cow_status_t;


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

/**
 * Initialize a `nuuk_cow_t`.
 *
 * @param cow    The `nuuk_cow_t` to initialize.
 * @param config The COW configuration.
 * @param node   The initial node to set the COW to.
 *
 * @return `NUUK_COW_OK`.
 */
nuuk_cow_status_t nuuk_cow_init(nuuk_cow_t * cow, const nuuk_cow_config_t * config, void * node);

/**
 * Destroy a `nuuk_cow_t` and release resources used by it.
 *
 * @param cow The `nuuk_cow_t` to destroy.
 * @param config The COW configuration.
 */
void nuuk_cow_destroy(nuuk_cow_t * cow, const nuuk_cow_config_t * config);


/******************************************************************************
 * TRANSFORM FUNCTIONS
 */

/**
 * Run a transform on a COW, and set the results as the current node. The previous node will be considered for
 * deferred disposal.
 *
 * @param cow       The COW.
 * @param config    The COW configuration.
 * @param transform The transform function to run.
 * @param udata     Optional data to pass to the transform function.
 *
 * @return `NUUK_COW_OK` if operation was successful, or the non-zero return code of the `transform` function, if any.
 */
int nuuk_cow_transform(nuuk_cow_t * cow, const nuuk_cow_config_t * config, nuuk_cow_transform_t transform,
    void * udata);


/******************************************************************************
 * ACCESSOR FUNCTIONS
 */

/**
 * Get and acquire the current node.
 *
 * @param cow    The COW.
 * @param config The COW configuration.
 * @param node   Will be set to the address of the acquired node.
 *
 * @return
 *   - `NUUK_COW_OK`         if the operation was successful.
 *   - `NUUK_COW_ERR_HAZARD` if the hazard pointer returned an error.
 */
nuuk_cow_status_t nuuk_cow_acquire_node(nuuk_cow_t * map, const nuuk_cow_config_t * config, void ** node);

/**
 * Release the previously acquired node.
 *
 * @param cow    The COW.
 * @param config The COW configuration.
 */
void nuuk_cow_release_node(nuuk_cow_t * map, const nuuk_cow_config_t * config);


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
