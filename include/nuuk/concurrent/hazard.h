/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief A hazard pointer implementation, based on the work from Andrei Alexandrescu and Maged Michael : "Lock-Free
 * Data Structures with Hazard Pointers".
 *
 * It allows at the same time to use a defined number of static thread-specific hazard pointers, as well as dynamically
 * created pointers. These structures are allocated from the heap.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include "nuuk/concurrent/atomic.h"
#include "nuuk/concurrent/thread.h"
#include "nuuk/util/status.h"
#include "nuuk/util/types.h"


/******************************************************************************
 * TYPES
 */

/**
 * Hazard structure.
 */
typedef struct nuuk_hazard_s {

  /**
   * Number of static pointers that will be allocated to each thread.
   */
  uint32_t n_ptrs;

  /**
   * Head of the linked list of thread-specific static structure.
   */
  atomic_intptr_t ts_head;

  /**
   * Head of the linked list of dynamic pointer structures.
   */
  atomic_intptr_t dyn_head;

  /**
   * Thread-specific storage, pointing to the static structure assigned to each thread, if any.
   */
  tss_t ts;

  /**
   * Current total number of hazard pointers.
   */
  atomic_uint32_t total_n_ptrs;

} nuuk_hazard_t;


/**
 * @internal
 * Forward declaration of `nuuk_hazard_dyn_t`.
 */

typedef struct nuuk_hazard_dyn_s nuuk_hazard_dyn_t;


/**
 * Handle pointing to a dynamic hazard pointer.
 * A single dynamic hazard pointer, represented by this handle, is not thread-safe, and should only be used by a single
 * thread at a time.
 */
typedef struct nuuk_hazard_dyn_handle_s {

  /**
   * Pointer to the allocated dynamic hazard.
   */
  nuuk_hazard_dyn_t * dyn;

} nuuk_hazard_dyn_handle_t;


/**
 * Status codes for various `nuuk_hazard_t` operations.
 */

typedef enum nuuk_hazard_status_e {

  /**
   * Normal operation.
   */
  NUUK_HAZARD_OK        = NUUK_OK,

  /**
   * Unable to alloc memory, because `nuuk_alloc()` failed.
   */
  NUUK_HAZARD_ERR_ALLOC = NUUK_ERR_ALLOC,

  /**
   * Unable to init thread-specific storage.
   */
  NUUK_HAZARD_ERR_TSS   = NUUK_ERR_EXTERNAL

} nuuk_hazard_status_t;


/******************************************************************************
 * LIFECYCLE FUNCTIONS
 */

/**
 * Initialize a hazard.
 *
 * @param hazard  The hazard to initialize.
 * @param n_ptrs  The number of static pointers to allocate to each thread.
 * @param lazy    If `false`, the thread-local structure will be allocated immediately.
 *
 * @return
 *   - `NUUK_HAZARD_OK`        if the initialization was successful.
 *   - `NUUK_HAZARD_ERR_ALLOC` if memory allocation failed.
 */
nuuk_hazard_status_t nuuk_hazard_init(nuuk_hazard_t * hazard, uint32_t n_ptrs, bool lazy);

/**
 * Destroy the hazard and release resources used by it.
 *
 * It will also free right away any memory block previously registered for deferred disposal.
 *
 * @param hazard The `nuuk_hazard_t` to destroy. If `NULL`, this function will return immediately.
 */
void nuuk_hazard_destroy(nuuk_hazard_t * hazard);


/******************************************************************************
 * DISPOSE FUNCTIONS
 */

/**
 * Register a pointer for deferred free.
 *
 * @param hazard The hazard reponsible for freeing `ptr`.
 * @param ptr    Pointer to the memory block to register for deferred free.
 *
 * @return
 *   - `NUUK_HAZARD_OK`        if the pointer has been registered for deferred free.
 *   - `NUUK_HAZARD_ERR_ALLOC` if memory allocation failed.
 */
nuuk_hazard_status_t nuuk_hazard_free(nuuk_hazard_t * hazard, void * ptr);


/******************************************************************************
 * STATIC HAZARD FUNCTIONS
 */

/**
 * Set a static pointer of a hazard.
 *
 * @param hazard        The hazard to set pointer from.
 * @param static_ptr_no The zero-based pointer index to set.
 * @param ptr           The value to which to set the static hazard pointer.
 *
 * @return
 *   - `NUUK_HAZARD_OK`        if the pointer has been set.
 *   - `NUUK_HAZARD_ERR_ALLOC` if memory allocation failed.
 */
nuuk_hazard_status_t nuuk_hazard_static_set(nuuk_hazard_t * hazard, uint32_t static_ptr_no, void * ptr);

/**
 * Clear a static pointer of a hazard.
 *
 * @param hazard        The hazard to clear pointer from.
 * @param static_ptr_no The zero-based pointer index to clear.
 */
void nuuk_hazard_static_clear(nuuk_hazard_t * hazard, uint32_t static_ptr_no);


/******************************************************************************
 * DYNAMIC HAZARD FUNCTIONS
 */

/**
 * Reserve a dynamic hazard pointer for future use.
 *
 * @param hazard     The hazard to set pointer from.
 * @param dyn_handle A pointer to the `nuuk_hazard_dyn_handle_t` to use as handle.
 *
 * @return
 *   - `NUUK_HAZARD_OK`        if a dynamic hazard pointer could be reserved.
 *   - `NUUK_HAZARD_ERR_ALLOC` if memory allocation failed.
 */
nuuk_hazard_status_t nuuk_hazard_dynamic_reserve(nuuk_hazard_t * hazard, nuuk_hazard_dyn_handle_t * dyn_handle);

/**
 * Release and clear a previously reserved dynamic hazard pointer.
 *
 * @param dyn_handle The dynamic hazard pointer handle.
 */
void nuuk_hazard_dynamic_release(nuuk_hazard_dyn_handle_t * dyn_handle);

/**
 * Set a dynamic hazard pointer.
 *
 * @param dyn_handle The dynamic hazard pointer handle.
 * @param ptr        The value to which to set the dynamic hazard pointer.
 */
void nuuk_hazard_dynamic_set(nuuk_hazard_dyn_handle_t * dyn_handle, void * ptr);

/**
 * Set a static pointer of a hazard.
 *
 * @param hazard     The hazard to set pointer from.
 * @param ptr        The value to which to set the dynamic hazard pointer.
 * @param dyn_handle A pointer to the `nuuk_hazard_dyn_handle_t` to use as handle.
 *
 * @return
 *   - `NUUK_HAZARD_OK`        if a dynamic hazard pointer could be reserved and set.
 *   - `NUUK_HAZARD_ERR_ALLOC` if memory allocation failed.
 */
nuuk_hazard_status_t nuuk_hazard_dynamic_reserve_set(nuuk_hazard_t * hazard, void * ptr,
    nuuk_hazard_dyn_handle_t * dyn_handle);

/**
 * Clear a dynamic hazard pointer.
 *
 * @param dyn_handle Handle of the dynamic hazard pointer to clear.
 */
void nuuk_hazard_dynamic_clear(nuuk_hazard_dyn_handle_t * dyn_handle);


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
