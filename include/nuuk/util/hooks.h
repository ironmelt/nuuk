/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief Hook helpers.
 */

#pragma once


/******************************************************************************
 * MACROS
 */

/**
 * Call a hook on an object. If hook not found, then return the default value.
 *
 * @param hook    The name of the hook to call.
 * @param default The default value in case `hook` is not defined for `object`.
 * @param object  The object to call `hook` for.
 * @param args    The additional arguments to pass to the hook.
 *
 * @return the return value of calling `hook` if it is defined, `default` otherwise.
 */
#define nuuk_hook(hook, default, object, args...) \
  ((object) && (object)->hooks && (object)->hooks->hook ? (object)->hooks->hook((object), ## args) : (default))
