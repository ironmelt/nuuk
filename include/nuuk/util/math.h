/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief Miscanellous utility helpers, including heavily optimized mathematical functions.
 */

#pragma once


/*****************************************************************************/


#include <limits.h>


/******************************************************************************
 * MACROS
 */

/**
 * Compute the minimum of two values.
 *
 * This implementation uses bit twiddling hacks to avoid branching.
 *
 * @param a The first value.
 * @param b The second value.
 *
 * @return `a` if `a < b`, `b` otherwise.
 */
#define nuuk_min(a, b) ((b) ^ (((a) ^ (b)) & -((a) < (b))))

/**
 * Compute the maximum of two values.
 *
 * This implementation uses bit twiddling hacks to avoid branching.
 *
 * @param a The first value.
 * @param b The second value.
 *
 * @return `a` if `a > b`, `b` otherwise.
 */
#define nuuk_max(a, b) ((a) ^ (((a) ^ (b)) & -((a) < (b))))

/**
 * Compute the absolute value.
 *
 * This implementation uses bit twiddling hacks to avoid branching.
 *
 * @param v The value.
 *
 * @return `v` if `v >= 0`, `v * -1` otherwise.
 */
#define nuuk_abs(v) \
  ({ \
    __typeof__((v)) mask = (v) >> (sizeof(__typeof__((v))) * CHAR_BIT - 1); \
    (((v) + mask) ^ mask); \
  })

/**
 * Checks whether a value is a power of two.
 *
 * This implementation uses bit twiddling hacks to avoid branching.
 *
 * @param v The value.
 *
 * @return `true` if `v` is a power of two.
 */
#define nuuk_is_pow2(v) ((v) && !((v) & ((v) - 1)))

/**
 * Round a value up to the next highest power of two.
 *
 * @param v The value.
 *
 * @return The next highest power of two. If `v` is already a power of two, the function will return `v`.
 */
#define nuuk_next_pow2(v) \
  ({ \
    __typeof__((v)) v_copy = v; \
    --v_copy; \
    __typeof__((v)) bit_limit = sizeof(__typeof__((v))) * CHAR_BIT; \
    for (__typeof__((v)) i = 1; i < bit_limit; i *= 2) { \
      v_copy |= v_copy >> i; \
    } \
    ++v_copy; \
  })

/**
 * Calculates the modulus of a value with a divisor which is a power of two.
 *
 * This implementation uses bit twiddling hacks to avoid branching.
 *
 * @param v   The value.
 * @param div The divisor. It MUST be a power of two.
 *
 * @return The result of `v % div`.
 */
#define nuuk_mod_pow2(v, div) ((v) & ((div) - 1))
