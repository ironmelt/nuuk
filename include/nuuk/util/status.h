/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief Status codes for Nuuk operations.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************
 * TYPES
 */

/**
 * Nuuk status codes.
 */
typedef enum nuuk_status_e {

  /**
   * Normal operation.
   */
  NUUK_OK            = 0,

  /**
   * Unable to allocate memory.
   */
  NUUK_ERR_ALLOC     = 1,

  /**
   * Maximum capacity reached.
   */
  NUUK_ERR_MAX       = 2,

  /**
   * Illegal index.
   */
  NUUK_ERR_INDEX     = 3,

  /**
   * Hook error.
   */
  NUUK_ERR_HOOK      = 50,

  /**
   * External library error.
   */
  NUUK_ERR_EXTERNAL  = 100,

  /**
   * Custom error.
   */
  NUUK_ERR_CUSTOM    = 150,

  /**
   * Custom error.
   */
  NUUK_ERR_UNKNOWN   = 200,

} nuuk_status_t;


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
