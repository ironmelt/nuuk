/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief Collection of macros aliasing default alloc functions.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include <alloca.h>
#include <stdlib.h>


/******************************************************************************
 * MACROS
 */

#define nuuk_alloc(__size) \
  (malloc(__size))

#define nuuk_realloc(__ptr, __size) \
  (realloc(__ptr, __size))

#define nuuk_calloc(__n_items, __item_size) \
  (calloc(__n_items, __item_size))

#define nuuk_free(__ptr) \
  (free(__ptr))

#define nuuk_alloca(__size) \
  (alloca(__size))


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
