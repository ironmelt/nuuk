/*
 * This file is part of Nuuk.
 *
 * Copyright 2015 Ironmelt Limited.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

/**
 * @file
 * @brief ANSI escape codes.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif


/*****************************************************************************/


#include <unistd.h>

#include "nuuk/util/types.h"


/******************************************************************************
 * DEFINES
 */


/*
 * GENERAL CODES
 */

#define ANSI_RESET               "\x1b[0m"
#define ANSI_BOLD                "\x1b[1m"
#define ANSI_DIM                 "\x1b[2m"
#define ANSI_UNDERLINED          "\x1b[4m"


/*
 * TEXT COLOR
 */

#define ANSI_COLOR_BLACK         "\x1b[30m"
#define ANSI_COLOR_RED           "\x1b[31m"
#define ANSI_COLOR_GREEN         "\x1b[32m"
#define ANSI_COLOR_YELLOW        "\x1b[33m"
#define ANSI_COLOR_BLUE          "\x1b[34m"
#define ANSI_COLOR_MAGENTA       "\x1b[35m"
#define ANSI_COLOR_CYAN          "\x1b[36m"
#define ANSI_COLOR_LIGHT_GRAY    "\x1b[37m"
#define ANSI_COLOR_DARK_GRAY     "\x1b[90m"
#define ANSI_COLOR_LIGHT_RED     "\x1b[91m"
#define ANSI_COLOR_LIGHT_GREEN   "\x1b[92m"
#define ANSI_COLOR_LIGHT_YELLOW  "\x1b[93m"
#define ANSI_COLOR_LIGHT_BLUE    "\x1b[94m"
#define ANSI_COLOR_LIGHT_MAGENTA "\x1b[95m"
#define ANSI_COLOR_LIGHT_CYAN    "\x1b[96m"
#define ANSI_COLOR_WHITE         "\x1b[97m"


/*
 * BACKGROUND COLOR
 */

#define ANSI_BG_BLACK            "\x1b[40m"
#define ANSI_BG_RED              "\x1b[41m"
#define ANSI_BG_GREEN            "\x1b[42m"
#define ANSI_BG_YELLOW           "\x1b[43m"
#define ANSI_BG_BLUE             "\x1b[44m"
#define ANSI_BG_MAGENTA          "\x1b[45m"
#define ANSI_BG_CYAN             "\x1b[46m"
#define ANSI_BG_LIGHT_GRAY       "\x1b[47m"
#define ANSI_BG_DARK_GRAY        "\x1b[100m"
#define ANSI_BG_LIGHT_RED        "\x1b[101m"
#define ANSI_BG_LIGHT_GREEN      "\x1b[102m"
#define ANSI_BG_LIGHT_YELLOW     "\x1b[103m"
#define ANSI_BG_LIGHT_BLUE       "\x1b[104m"
#define ANSI_BG_LIGHT_MAGENTA    "\x1b[105m"
#define ANSI_BG_LIGHT_CYAN       "\x1b[106m"
#define ANSI_BG_WHITE            "\x1b[107m"


/******************************************************************************
 * INLINE FUNCTIONS
 */

/**
 * Tests whether `fd` is an open file descriptor referring to a terminal where
 * ANSI escape codes are supported.
 *
 * @param fd The file descriptor to test.
 *
 * @return `true` if `fd` is an open file descriptor referring to a terminal.
 */
static inline
bool nuuk_ansi_codes_are_supported(int fd) {
  return isatty(fd);
}


/*****************************************************************************/


#ifdef __cplusplus
} // extern "C"
#endif
